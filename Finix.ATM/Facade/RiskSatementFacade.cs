﻿using AutoMapper;
using Finix.ATM.DTO;
using Finix.ATM.Infrastructure;
using Finix.ATM.Infrastructure.Models;
using Finix.Auth.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Facade
{
   public class RiskSatementFacade :BaseFacade
    {
        #region Risk Statement

        public List<RiskStatementDto> GetReport()
        {
            var list = new List<RiskStatementDto>();
            return list;

        }
        public ResponseDto SaveRiskStatement(RiskStatementDto dto, long? userId)
        {
            var entity = new RiskStatement();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<RiskStatement>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Statement is Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<RiskStatement>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Statement is Saved Successfully";
                }
            }
            catch (Exception e)
            {
                responce.Message = e.ToString();
                responce.Message = "Failed!";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<RiskStatementDto> GetStatements(long? upid)
        {
            List<RiskStatement> data = new List<RiskStatement>();
            if (upid != null)
                data = GenService.GetAll<RiskStatement>().Where(c => c.Status == EntityStatus.Active && c.SpecificArea.ParentID == upid).ToList();
            else
                data = GenService.GetAll<RiskStatement>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<RiskStatementDto>>(data);
        }

        
        public List<ElementAtRiskDto> getElementsAtRisk(long? upid)
        {
            var data = GenService.GetAll<ElementAtRisk>().Where(c => c.Status == EntityStatus.Active && (c.UPId == upid));
            return Mapper.Map<List<ElementAtRiskDto>>(data);
        }
        #endregion

        public ResponseDto SaveRiskPriority(RiskPriorityDto dto, long? userId)
        {
            var entity = new RiskPriority();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<RiskPriority>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Priority is Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<RiskPriority>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Priority is Saved Successfully";
                }
            }
            catch (Exception e)
            {
                responce.Message = e.ToString();
                responce.Message = "Failed!";
            }
            GenService.SaveChanges();
            UpdateRating((long)dto.UPId);
            return responce;
        }

        public ResponseDto UpdateRating(long upid)
        {
            var updatedstatements = new List<RiskPriority>();
            var statements =GenService.GetAll<RiskPriority>().Where(x=>x.UPId==upid).OrderByDescending(x=>x.RiskPercentage);
            ResponseDto responce = new ResponseDto();
            int i = 1;
            foreach (var st in statements)
            {
                st.Rating = i;
                updatedstatements.Add(st);
                i++;
            }
            try
            {
                GenService.Save(updatedstatements);
                responce.Success = true;
                responce.Message = "New Risk Priority Has been Set";
            }
            catch
            {
                
                responce.Message = "Failed!";
            }
            return responce;
        }

        public List<RiskPriorityDto> GetRatings(long? upid)
        {
            List<RiskPriority> data = new List<RiskPriority>();
            if (upid != null)
                data = GenService.GetAll<RiskPriority>().Where(c => c.Status == EntityStatus.Active && c.UPId == upid).ToList();
            else
                data = GenService.GetAll<RiskPriority>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<RiskPriorityDto>>(data);
        }

        public List<RiskPriorityDto> GetRatingReport()
        {
            var list = new List<RiskPriorityDto>();
            return list;

        }

        public List<ElementAtRiskDto> GetElementAtRiskReport()
        {
            var list = new List<ElementAtRiskDto>();
            return list;

        }

        #region Risk Reducton Options
        public ResponseDto SaveRiskReductionOption(RiskReductionOptionDto dto, long? userId)
        {
            var entity = new RiskReductionOption();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<RiskReductionOption>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Reduction Options are Updated Successfully";
                }
                else
                {
                    entity = Mapper.Map<RiskReductionOption>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Reduction Options are Saved Successfully";
                }
            }
            catch (Exception e)
            {
                responce.Message = e.ToString();
                responce.Message = "Failed!";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<RiskReductionOptionDto> GetReductionOptions(long upid)
        {
            var data = GenService.GetAll<RiskReductionOption>().Where(c => c.Status == EntityStatus.Active && c.RiskStatement.SpecificArea.ParentID == upid).ToList();
            return Mapper.Map<List<RiskReductionOptionDto>>(data);
        }
        #endregion

        #region Risk Reduction Option Action Plan
        public List<RiskStatementDto> GetTopStatements(long? upid)
        {
            List<RiskStatement> data = new List<RiskStatement>();
            var riskpriority = GenService.GetAll<RiskPriority>().OrderByDescending(x => x.PriorityPoint).ToList();
            if (upid != null)
                data = GenService.GetAll<RiskStatement>().Where(c => c.Status == EntityStatus.Active && c.SpecificArea.ParentID == upid).ToList();
            else
                data = GenService.GetAll<RiskStatement>().Where(c => c.Status == EntityStatus.Active).ToList();

            var final = (from item in data
                         join pr in riskpriority
                         on item.Id equals
                         pr.RiskStatementId
                         select new RiskStatementDto
                         {
                             Id = item.Id,
                             Statement = item.Statement,
                         }).ToList();
            return final;
        }

        public List<RiskReductionOptionDto> GetReductionOptionsByStatement(long stid)
        {
            var data = GenService.GetAll<RiskReductionOption>().Where(c => c.Status == EntityStatus.Active && c.RiskStatementId == stid).ToList();
            return Mapper.Map<List<RiskReductionOptionDto>>(data);
        }
        public List<OptionActionPlanDto> GetActionPlansByStatement(long stid)
        {
            var data = GenService.GetAll<OptionActionPlan>().Where(c => c.Status == EntityStatus.Active && c.RiskStatementId == stid).ToList();
            return Mapper.Map<List<OptionActionPlanDto>>(data);
        }
        public ResponseDto SaveOptionActionPlan(List<OptionActionPlanDto> dto, long? userId)
        {
            var entity = new OptionActionPlan();
            ResponseDto responce = new ResponseDto();
            if (dto.Count > 0)
            {
                try
                {
                    foreach (var item in dto)
                    {
                        if (item.Id != null && item.Id > 0)
                        {
                            entity = GenService.GetById<OptionActionPlan>((long)item.Id);
                            item.CreateDate = entity.CreateDate;
                            item.CreatedBy = entity.CreatedBy;
                            Mapper.Map(item, entity);
                            entity.EditDate = DateTime.Now;
                            entity.EditedBy = userId;
                            entity.Status = EntityStatus.Active;
                            GenService.Save(entity);
                            //responce.Success = true;
                            //responce.Message = "Action plans are Edited Successfully";
                        }
                        else
                        {
                            entity = Mapper.Map<OptionActionPlan>(item);
                            entity.Status = EntityStatus.Active;
                            entity.CreateDate = DateTime.Now;
                            entity.CreatedBy = userId;
                            GenService.Save(entity);

                        }
                    }
                    responce.Success = true;
                    responce.Message = "Action plans are Saved Successfully";
                }
                catch (Exception)
                {
                    responce.Message = "Action plan Save Failed";
                }
                GenService.SaveChanges();
            }
            return responce;
        }

        public List<RiskReductionOptionDto> GetOptions(long stid)
        {
            var actions=GenService.GetAll<OptionActionPlan>().Where(c => c.Status == EntityStatus.Active && c.RiskStatementId == stid).Select(x=>x.OptionId).ToList();
            var data = GenService.GetAll<RiskReductionOption>().Where(c => c.Status == EntityStatus.Active && c.RiskStatementId == stid && !actions.Contains(c.Id)).ToList();
            return Mapper.Map<List<RiskReductionOptionDto>>(data);
        }
        #endregion
    }
}
