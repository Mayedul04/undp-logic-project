﻿using Finix.ATM.DTO;
using Finix.Auth.Facade;
using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Facade
{
    public class EnumFacade : BaseFacade
    {
        public List<EnumDto> GetLocationLevels()
        {
            var typeList = Enum.GetValues(typeof(LocationLevel))
               .Cast<LocationLevel>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetPotentialtoSolutions()
        {
            var typeList = Enum.GetValues(typeof(PotentialToProperSolution))
               .Cast<PotentialToProperSolution>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetFoodProductionTypes()
        {
            var typeList = Enum.GetValues(typeof(FoodProductionType))
               .Cast<FoodProductionType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetPovertySituations()
        {
            var typeList = Enum.GetValues(typeof(PovertySituation))
               .Cast<PovertySituation>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetGenders()
        {
            var typeList = Enum.GetValues(typeof(Gender))
               .Cast<Gender>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetIndicatorPrimaryGroups()
        {
            var typeList = Enum.GetValues(typeof(IndicatorPrimaryGroup))
               .Cast<IndicatorPrimaryGroup>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetRelations()
        {
            var typeList = Enum.GetValues(typeof(Relations))
               .Cast<Relations>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetMaritalStatus()
        {
            var typeList = Enum.GetValues(typeof(MaritalStatus))
               .Cast<MaritalStatus>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetReligions()
        {
            var typeList = Enum.GetValues(typeof(Religion))
               .Cast<Religion>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetRentalTypes()
        {
            var typeList = Enum.GetValues(typeof(HouseRenatalType))
               .Cast<HouseRenatalType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetHouseTypes()
        {
            var typeList = Enum.GetValues(typeof(HouseType))
               .Cast<HouseType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetLatrineTypes()
        {
            var typeList = Enum.GetValues(typeof(LatrineType))
               .Cast<LatrineType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = Util.UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }
    }
}
