﻿using AutoMapper;
using Finix.ATM.DTO;
using Finix.ATM.Infrastructure;
using Finix.ATM.Infrastructure.Models;
using Finix.Auth.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Facade
{
    public class HouseHoldFacade : BaseFacade
    {
        #region Energy Sources
        public ResponseDto SaveEnergySource(EnergySourceDto dto, long? userId)
        {
            var entity = new EnergySource();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<EnergySource>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Energy Source Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<EnergySource>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Energy Source Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Energy Source Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<EnergySourceDto> GetEnergySources()
        {
            var data = GenService.GetAll<EnergySource>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<EnergySourceDto>>(data);
        }
        #endregion

        #region Materials
        public ResponseDto SaveMaterial(MaterialDto dto, long? userId)
        {
            var entity = new Material();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Material>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Material Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Material>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Material Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Material Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<MaterialDto> GetMaterials()
        {
            var data = GenService.GetAll<Material>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<MaterialDto>>(data);
        }

        public List<BuildingPartDto> GetBuildingParts()
        {
            var data = GenService.GetAll<BuildingPart>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<BuildingPartDto>>(data);
        }
        #endregion

        #region HouseHold Survey
        public ResponseDto SaveHHBasicInfo(HouseHoldInformationDto dto, long? userId)
        {
            var entity = new HouseHoldInformation();
            ResponseDto responce = new ResponseDto();

            var mem = new HouseHoldMembers();


            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldInformation>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    //entity.Members = members;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Basic Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldInformation>(dto);
                    //entity.Members = members;
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Basic Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Basic Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHmember(HouseHoldMembersDto dto, long? userId)
        {
            var entity = new HouseHoldMembers();
            ResponseDto responce = new ResponseDto();

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldMembers>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Food Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldMembers>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Food Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Food Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHDisasters(HouseHoldDisastersDto dto, long? userId)
        {
            var entity = new HouseHoldDisasters();
            ResponseDto responce = new ResponseDto();

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldDisasters>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Disaster Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldDisasters>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Disaster Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Disaster Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHSurveyInfo(HouseHoldSurveyAdministrationDto dto, long? userId)
        {
            var entity = new HouseHoldSurveyAdministration();
            ResponseDto responce = new ResponseDto();

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldSurveyAdministration>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Survey Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldSurveyAdministration>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Survey Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Survey Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHBuildingInfo(HouseHoldBuildingDto dto, long? userId)
        {
            var entity = new HouseHoldBuilding();
            ResponseDto responce = new ResponseDto();

            var otherbuildings = new List<HouseHoldOtherBuildings>();
            var building = new HouseHoldOtherBuildings();
            var hazards = new List<HouseHoldHazards>();
            var hazard = new HouseHoldHazards();

            if (dto.OtherBuildings != null && dto.OtherBuildings.Count > 0)
            {
                foreach (var item in dto.OtherBuildings)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        building = new HouseHoldOtherBuildings();
                        building = GenService.GetById<HouseHoldOtherBuildings>((long)item.Id);
                        item.CreatedBy = building.CreatedBy;
                        item.CreateDate = building.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, building);
                        GenService.Save(building, false);
                        otherbuildings.Add(building);
                    }
                    else
                    {
                        building = Mapper.Map<HouseHoldOtherBuildings>(item);
                        building.CreateDate = DateTime.Now;
                        building.CreatedBy = userId;
                        building.Status = EntityStatus.Active;
                        otherbuildings.Add(building);
                    }
                }
            }

            if (dto.PronetoHazards != null && dto.PronetoHazards.Count > 0)
            {
                foreach (var item in dto.PronetoHazards)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        hazard = new HouseHoldHazards();
                        hazard = GenService.GetById<HouseHoldHazards>((long)item.Id);
                        item.CreatedBy = hazard.CreatedBy;
                        item.CreateDate = hazard.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, hazard);
                        GenService.Save(hazard, false);
                        hazards.Add(hazard);
                    }
                    else
                    {
                        hazard = Mapper.Map<HouseHoldHazards>(item);
                        hazard.CreateDate = DateTime.Now;
                        hazard.CreatedBy = userId;
                        hazard.Status = EntityStatus.Active;
                        hazards.Add(hazard);
                    }
                }
            }



            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldBuilding>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.OtherBuildings = otherbuildings;
                    entity.PronetoHazards = hazards;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Building Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldBuilding>(dto);
                    entity.OtherBuildings = otherbuildings;
                    entity.PronetoHazards = hazards;
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Building Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Building Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHFoodInfo(HouseHoldFoodDto dto, long? userId)
        {
            var entity = new HouseHoldFood();
            ResponseDto responce = new ResponseDto();

            var mainfoods = new List<HouseHoldMainFoods>();
            var mem = new HouseHoldMainFoods();

            var foodshortages = new List<HouseHoldFoodShortages>();
            var shor = new HouseHoldFoodShortages();

            if (dto.MainFoods != null && dto.MainFoods.Count > 0)
            {
                foreach (var item in dto.MainFoods)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        mem = new HouseHoldMainFoods();
                        mem = GenService.GetById<HouseHoldMainFoods>((long)item.Id);
                        item.CreatedBy = mem.CreatedBy;
                        item.CreateDate = mem.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = EntityStatus.Active;
                        Mapper.Map(item, mem);
                        GenService.Save(mem, false);
                        mainfoods.Add(mem);
                    }
                    else
                    {
                        mem = Mapper.Map<HouseHoldMainFoods>(item);
                        mem.CreateDate = DateTime.Now;
                        mem.CreatedBy = userId;
                        mem.Status = EntityStatus.Active;
                        mainfoods.Add(mem);
                    }
                }
            }

            if (dto.FoodShortages != null && dto.FoodShortages.Count > 0)
            {
                foreach (var item in dto.FoodShortages)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        shor = new HouseHoldFoodShortages();
                        shor = GenService.GetById<HouseHoldFoodShortages>((long)item.Id);
                        item.CreatedBy = shor.CreatedBy;
                        item.CreateDate = shor.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = EntityStatus.Active;
                        Mapper.Map(item, shor);
                        GenService.Save(shor, false);
                        foodshortages.Add(shor);
                    }
                    else
                    {
                        shor = Mapper.Map<HouseHoldFoodShortages>(item);
                        shor.CreateDate = DateTime.Now;
                        shor.CreatedBy = userId;
                        shor.Status = EntityStatus.Active;
                        foodshortages.Add(shor);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldFood>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.MainFoods = mainfoods;
                    entity.FoodShortages = foodshortages;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Food Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldFood>(dto);
                    entity.MainFoods = mainfoods;
                    entity.FoodShortages = foodshortages;
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Food Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Food Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHHealthInfo(HouseHoldHealthDto dto, long? userId)
        {
            var entity = new HouseHoldHealth();
            ResponseDto responce = new ResponseDto();

            var watersources = new List<HouseHoldWaterSource>();
            var source = new HouseHoldWaterSource();

            if (dto.MainWaterSources != null && dto.MainWaterSources.Count > 0)
            {
                foreach (var item in dto.MainWaterSources)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        source = new HouseHoldWaterSource();
                        source = GenService.GetById<HouseHoldWaterSource>((long)item.Id);
                        item.CreatedBy = source.CreatedBy;
                        item.CreateDate = source.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, source);
                        GenService.Save(source, false);
                        watersources.Add(source);
                    }
                    else
                    {
                        source = Mapper.Map<HouseHoldWaterSource>(item);
                        source.CreateDate = DateTime.Now;
                        source.CreatedBy = userId;
                        source.Status = EntityStatus.Active;
                        watersources.Add(source);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldHealth>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.MainWaterSources = watersources;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Health Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldHealth>(dto);
                    entity.MainWaterSources = watersources;
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Health Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Health Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHLandAsset(HouseHoldLandAssetDto dto, long? userId)
        {
            var entity = new HouseHoldLandAsset();
            ResponseDto responce = new ResponseDto();

            var agritools = new List<HouseHoldAgriTools>();
            var tool = new HouseHoldAgriTools();


            if (dto.AgriculturalTools != null && dto.AgriculturalTools.Count > 0)
            {
                foreach (var item in dto.AgriculturalTools)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        tool = new HouseHoldAgriTools();
                        tool = GenService.GetById<HouseHoldAgriTools>((long)item.Id);
                        item.CreatedBy = tool.CreatedBy;
                        item.CreateDate = tool.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, tool);
                        GenService.Save(tool, false);
                        agritools.Add(tool);
                    }
                    else
                    {
                        tool = Mapper.Map<HouseHoldAgriTools>(item);
                        tool.CreateDate = DateTime.Now;
                        tool.CreatedBy = userId;
                        tool.Status = EntityStatus.Active;
                        agritools.Add(tool);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldLandAsset>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.AgriculturalTools = agritools;

                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Land & Asset has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldLandAsset>(dto);
                    entity.AgriculturalTools = agritools;

                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Land & Asset has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Land & Asset Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveHHEnergyInfo(HouseHoldEnergyDto dto, long? userId)
        {
            var entity = new HouseHoldEnergy();
            ResponseDto responce = new ResponseDto();

            var energysources = new List<HouseHoldEnergySources>();
            var source = new HouseHoldEnergySources();
            var hhcrops = new List<HouseHoldCrops>();
            var crop = new HouseHoldCrops();

            if (dto.HouseHoldEnergySources != null && dto.HouseHoldEnergySources.Count > 0)
            {
                foreach (var item in dto.HouseHoldEnergySources)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        source = new HouseHoldEnergySources();
                        source = GenService.GetById<HouseHoldEnergySources>((long)item.Id);
                        item.CreatedBy = source.CreatedBy;
                        item.CreateDate = source.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, source);
                        GenService.Save(source, false);
                        energysources.Add(source);
                    }
                    else
                    {
                        source = Mapper.Map<HouseHoldEnergySources>(item);
                        source.CreateDate = DateTime.Now;
                        source.CreatedBy = userId;
                        source.Status = EntityStatus.Active;
                        energysources.Add(source);
                    }
                }
            }



            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldEnergy>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.HouseHoldEnergySources = energysources;

                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Energy Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldEnergy>(dto);
                    entity.HouseHoldEnergySources = energysources;

                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "House Hold Energy Information has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "House Hold Energy Information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<HouseHoldListDto> GetHouseHolds(long? upid)
        {
            var data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active && c.Village.Parent.ParentID == upid).ToList();
            var result = (from item in data
                          select new HouseHoldListDto
                          {
                              HHId = item.Id,
                              HouseHoldNo = item.HouseHoldNo,
                              UPId = item.Village.Parent.ParentID,
                              Address = item.Village.Parent.LocationName + ", " + item.Village.LocationName,
                              IsSelected = item.IsEligibleforScheme
                          }).ToList();
            return result;
        }

        public List<HouseHoldListDto> GetWaitingHouseHolds(long? upid)
        {
            var data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active && c.Village.Parent.ParentID == upid && c.IsEligibleforScheme == false);
            var result = (from item in data
                          select new HouseHoldListDto
                          {
                              HHId = item.Id,
                              HouseHoldNo = item.HouseHoldNo,
                              UPId = item.Village.Parent.ParentID,
                              Address = item.Village.Parent.LocationName + ", " + item.Village.LocationName,
                              IsSelected = item.IsEligibleforScheme
                          }).ToList();
            return result;
        }

        public List<HouseHoldListDto> GetSelectedHouseHolds(long? upid)
        {
            var data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active && c.Village.Parent.ParentID == upid && c.IsEligibleforScheme == true);
            var interventionlist = GenService.GetAll<Interventions>().Where(c => c.Status == EntityStatus.Active && c.HouseHold.Village.Parent.ParentID == upid);
            var disbursmenthistory = GenService.GetAll<DisbursementHistory>().Where(c => c.Status == EntityStatus.Active).GroupBy(x => x.InterventionId).Select(a => new { DisbursedAmount = a.Sum(b => b.DisbursedAmount), InterventionId = a.Key.Value });
            var result = (from item in data
                          join inter in interventionlist
                          on item.Id equals inter.HHId
                          into ps
                          from p in ps.DefaultIfEmpty()
                          join budget in disbursmenthistory
                          on p.Id equals budget.InterventionId
                          into final
                          from f in final.DefaultIfEmpty()
                          select new HouseHoldListDto
                          {
                              HHId = item.Id,
                              InterventionId = p.Id,
                              HouseHoldNo = item.HouseHoldNo,
                              Address = item.Village.Parent.LocationName + ", " + item.Village.LocationName,
                              GrantedAmount = (p.Id > 0 ? p.CRFGrant : 0),
                              DisbursedAmount = (f.InterventionId > 0 ? f.DisbursedAmount : 0),
                              IsSelected = item.IsEligibleforScheme,
                              HaveIntervertion = (p.Id > 0 ? true : false)
                          }).ToList();
            //group new { item.Id, item.HouseHoldNo, item.Address, item.IsEligibleforScheme, p.Id, p.CRFGrant ,f.InterventionId} by
            return result;
        }

        public ResponseDto SaveHHforProposal(List<long> selectedhh, long userId)
        {
            ResponseDto responce = new ResponseDto();
            var pendingHHList = new List<HouseHoldInformation>();
            try
            {
                if (selectedhh.Count > 0)
                {
                    foreach (var item in selectedhh)
                    {
                        var household = GenService.GetById<HouseHoldInformation>(item);
                        household.IsEligibleforScheme = true;
                        household.EditDate = DateTime.Now;
                        household.EditedBy = userId;
                        pendingHHList.Add(household);

                    }
                    GenService.Save(pendingHHList);
                    responce.Success = true;
                    responce.Message = "HouseHold Have been Edited Successfully";
                }
                else
                {
                    responce.Success = false;
                    responce.Message = "There is no selected Members.";
                }
            }
            catch (Exception ex)
            {
                responce.Success = false;
                responce.Message = "Member Pending Fee Save Failed";
            }
            return responce;
        }

        public HouseHoldInformationDto LoadHouseHoldBasicInfo(long hhid)
        {
            HouseHoldInformationDto basicinfo = new HouseHoldInformationDto();
            var data = GenService.GetById<HouseHoldInformation>(hhid);
            if (data != null)
            {
                basicinfo = Mapper.Map<HouseHoldInformationDto>(data);
                //var members = GenService.GetAll<HouseHoldMembers>().Where(x => x.HHId == hhid);
                //basicinfo.Members = Mapper.Map<List<HouseHoldMembersDto>>(members.ToList());

                return basicinfo;
            }
            else
                return null;
        }

        public List<HouseHoldMembersDto> LoadHouseHoldMemberInfo(long hhid)
        {
            var members = new List<HouseHoldMembersDto>();
            var data = GenService.GetAll<HouseHoldMembers>().Where(x => x.HHId == hhid).ToList();
            if (data != null)
            {
                members = Mapper.Map<List<HouseHoldMembersDto>>(data);
                return members;
            }
            else
                return null;
        }

        public HouseHoldDisastersDto LoadHouseHoldDisasterInfo(long hhid)
        {
            var disasters = new HouseHoldDisastersDto();
            var data = GenService.GetAll<HouseHoldDisasters>().Where(x => x.HHId == hhid).FirstOrDefault();
            if (data != null)
            {
                disasters = Mapper.Map<HouseHoldDisastersDto>(data);
                return disasters;
            }
            else
                return null;
        }

        public HouseHoldMembersDto getMemberInfo(long memid)
        {
            var member = new HouseHoldMembersDto();
            var data = GenService.GetById<HouseHoldMembers>(memid);
            if (data != null)
            {
                member = Mapper.Map<HouseHoldMembersDto>(data);
                return member;
            }
            else
                return null;
        }

        public HouseHoldSurveyAdministrationDto LoadHouseHoldSurveyInfo(long hhid)
        {
            HouseHoldSurveyAdministrationDto survey = new HouseHoldSurveyAdministrationDto();
            var data = GenService.GetAll<HouseHoldSurveyAdministration>().Where(x => x.HHId == hhid).FirstOrDefault();
            if (data != null)
            {
                survey = Mapper.Map<HouseHoldSurveyAdministrationDto>(data);
                return survey;
            }
            else
                return null;
        }

        public HouseHoldBuildingDto LoadHouseHoldBuildingInfo(long hhid)
        {
            HouseHoldBuildingDto buildinginfo = new HouseHoldBuildingDto();
            var data = GenService.GetAll<HouseHoldBuilding>().Where(x => x.HHId == hhid).FirstOrDefault();
            if (data != null)
            {
                buildinginfo = Mapper.Map<HouseHoldBuildingDto>(data);
                var otherbuildings = GenService.GetAll<HouseHoldOtherBuildings>().Where(x => x.HouseHoldBuildingId == buildinginfo.Id);
                buildinginfo.OtherBuildings = Mapper.Map<List<HouseHoldOtherBuildingsDto>>(otherbuildings.ToList());
                var hazards = GenService.GetAll<HouseHoldHazards>().Where(x => x.HouseHoldBuildingId == buildinginfo.Id && x.Status == EntityStatus.Active);
                buildinginfo.PronetoHazards = Mapper.Map<List<HouseHoldHazardsDto>>(hazards.ToList());

                return buildinginfo;
            }
            else
                return null;
        }

        public List<HouseHoldHazardsDto> LoadHouseHoldHazardList(long hhid)
        {
            var data = GenService.GetAll<HouseHoldBuilding>().Where(x => x.HHId == hhid).FirstOrDefault();
            var hazards = GenService.GetAll<HouseHoldHazards>().Where(x => x.HouseHoldBuildingId == data.Id && x.Status == EntityStatus.Active);
            return Mapper.Map<List<HouseHoldHazardsDto>>(hazards.ToList());
        }

        public HouseHoldFoodDto LoadHouseHoldFoodInfo(long hhid)
        {
            HouseHoldFoodDto foodinfo = new HouseHoldFoodDto();
            var data = GenService.GetAll<HouseHoldFood>().Where(x => x.HHId == hhid).FirstOrDefault();
            if (data != null)
            {
                foodinfo = Mapper.Map<HouseHoldFoodDto>(data);
                var mainfoods = GenService.GetAll<HouseHoldMainFoods>().Where(x => x.HouseHoldFoodId == foodinfo.Id);
                foodinfo.MainFoods = Mapper.Map<List<HouseHoldMainFoodsDto>>(mainfoods.ToList());
                var foodshartages = GenService.GetAll<HouseHoldFoodShortages>().Where(x => x.HouseHoldFoodId == foodinfo.Id);
                foodinfo.FoodShortages = Mapper.Map<List<HouseHoldFoodShortagesDto>>(foodshartages.ToList());
                return foodinfo;
            }
            else
                return null;
        }

        public HouseHoldHealthDto LoadHouseHoldHealthInfo(long hhid)
        {
            HouseHoldHealthDto healthinfo = new HouseHoldHealthDto();
            var data = GenService.GetAll<HouseHoldHealth>().Where(x => x.HHId == hhid).FirstOrDefault();
            if (data != null)
            {
                healthinfo = Mapper.Map<HouseHoldHealthDto>(data);
                var sources = GenService.GetAll<HouseHoldWaterSource>().Where(x => x.HouseHoldHealthId == healthinfo.Id && x.Status == EntityStatus.Active);
                healthinfo.MainWaterSources = Mapper.Map<List<HouseHoldWaterSourceDto>>(sources.ToList());

                return healthinfo;
            }
            else
                return null;
        }
        public HouseHoldLandAssetDto LoadHouseHoldLandAsset(long hhid)
        {
            HouseHoldLandAssetDto assetinfo = new HouseHoldLandAssetDto();
            var data = GenService.GetAll<HouseHoldLandAsset>().Where(x => x.HHId == hhid).FirstOrDefault();
            if (data != null)
            {
                assetinfo = Mapper.Map<HouseHoldLandAssetDto>(data);
                var tools = GenService.GetAll<HouseHoldAgriTools>().Where(x => x.AssetId == assetinfo.Id && x.Status == EntityStatus.Active);
                assetinfo.AgriculturalTools = Mapper.Map<List<HouseHoldAgriToolsDto>>(tools.ToList());
                return assetinfo;
            }
            else
                return null;
        }

        public HouseHoldEnergyDto LoadHouseHoldEnergyInfo(long hhid)
        {
            HouseHoldEnergyDto energyinfo = new HouseHoldEnergyDto();
            var data = GenService.GetAll<HouseHoldEnergy>().Where(x => x.HHId == hhid).FirstOrDefault();
            if (data != null)
            {
                energyinfo = Mapper.Map<HouseHoldEnergyDto>(data);
                var sources = GenService.GetAll<HouseHoldEnergySources>().Where(x => x.HouseHoldEnergyId == energyinfo.Id && x.Status == EntityStatus.Active);
                energyinfo.HouseHoldEnergySources = Mapper.Map<List<HouseHoldEnergySourcesDto>>(sources.ToList());
                //var crops = GenService.GetAll<HouseHoldCrops>().Where(x => x.HouseHoldEnergyId == energyinfo.Id && x.Status == EntityStatus.Active);
                //energyinfo.HouseHoldCrops = Mapper.Map<List<HouseHoldCropsDto>>(crops.ToList());

                return energyinfo;
            }
            else
                return null;
        }
        #endregion

        #region Interventions
        public ResponseDto SaveInterventions(InterventionsDto dto, long? userId)
        {
            var entity = new Interventions();
            ResponseDto responce = new ResponseDto();

            var supporttypes = new List<InterventionSupport>();
            var support = new InterventionSupport();

            var hazards = new List<InterventionHazard>();
            var hazard = new InterventionHazard();

            var budgets = new List<InterventionBudget>();
            var budget = new InterventionBudget();

            var benefits = new List<InterventionExpectedbenfits>();
            var benefit = new InterventionExpectedbenfits();


            if (dto.SupportTypes != null && dto.SupportTypes.Count > 0)
            {
                foreach (var item in dto.SupportTypes)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        support = new InterventionSupport();
                        support = GenService.GetById<InterventionSupport>((long)item.Id);
                        item.CreatedBy = support.CreatedBy;
                        item.CreateDate = support.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, support);
                        GenService.Save(support, false);
                        supporttypes.Add(support);
                    }
                    else
                    {
                        support = Mapper.Map<InterventionSupport>(item);
                        support.CreateDate = DateTime.Now;
                        support.CreatedBy = userId;
                        support.Status = EntityStatus.Active;
                        supporttypes.Add(support);
                    }
                }
            }

            if (dto.InterventionHazards != null && dto.InterventionHazards.Count > 0)
            {
                foreach (var item in dto.InterventionHazards)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        hazard = new InterventionHazard();
                        hazard = GenService.GetById<InterventionHazard>((long)item.Id);
                        item.CreatedBy = hazard.CreatedBy;
                        item.CreateDate = hazard.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, hazard);
                        GenService.Save(hazard, false);
                        hazards.Add(hazard);
                    }
                    else
                    {
                        hazard = Mapper.Map<InterventionHazard>(item);
                        hazard.CreateDate = DateTime.Now;
                        hazard.CreatedBy = userId;
                        hazard.Status = EntityStatus.Active;
                        hazards.Add(hazard);
                    }
                }
            }

            if (dto.InterventionBudgets != null && dto.InterventionBudgets.Count > 0)
            {
                foreach (var item in dto.InterventionBudgets)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        budget = new InterventionBudget();
                        budget = GenService.GetById<InterventionBudget>((long)item.Id);
                        item.CreatedBy = budget.CreatedBy;
                        item.CreateDate = budget.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = item.Status;
                        Mapper.Map(item, budget);
                        GenService.Save(budget, false);
                        budgets.Add(budget);
                    }
                    else
                    {
                        budget = Mapper.Map<InterventionBudget>(item);
                        budget.CreateDate = DateTime.Now;
                        budget.CreatedBy = userId;
                        budget.Status = EntityStatus.Active;
                        budgets.Add(budget);
                    }
                }
            }

            if (dto.InterventionExpectedbenfits != null && dto.InterventionExpectedbenfits.Count > 0)
            {
                foreach (var item in dto.InterventionExpectedbenfits)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        benefit = new InterventionExpectedbenfits();
                        benefit = GenService.GetById<InterventionExpectedbenfits>((long)item.Id);
                        item.CreatedBy = benefit.CreatedBy;
                        item.CreateDate = benefit.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = EntityStatus.Active;
                        Mapper.Map(item, benefit);
                        GenService.Save(benefit, false);
                        benefits.Add(benefit);
                    }
                    else
                    {
                        benefit = Mapper.Map<InterventionExpectedbenfits>(item);
                        benefit.CreateDate = DateTime.Now;
                        benefit.CreatedBy = userId;
                        benefit.Status = EntityStatus.Active;
                        benefits.Add(benefit);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Interventions>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.SupportTypes = supporttypes;
                    entity.InterventionBudgets = budgets;
                    entity.InterventionHazards = hazards;
                    entity.InterventionExpectedbenfits = benefits;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Intervention has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Interventions>(dto);
                    entity.SupportTypes = supporttypes;
                    entity.InterventionBudgets = budgets;
                    entity.InterventionHazards = hazards;
                    entity.InterventionExpectedbenfits = benefits;
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Intervention has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Intervention Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public InterventionsDto LoadIntervention(long id)
        {
            InterventionsDto interventioninfo = new InterventionsDto();
            var data = GenService.GetById<Interventions>(id);
            if (data != null)
            {
                interventioninfo = Mapper.Map<InterventionsDto>(data);
                var supports = GenService.GetAll<InterventionSupport>().Where(x => x.InterventionId == interventioninfo.Id && x.Status == EntityStatus.Active);
                interventioninfo.SupportTypes = Mapper.Map<List<InterventionSupportDto>>(supports.ToList());
                var hazards = GenService.GetAll<InterventionHazard>().Where(x => x.InterventionId == interventioninfo.Id && x.Status == EntityStatus.Active);
                interventioninfo.InterventionHazards = Mapper.Map<List<InterventionHazardDto>>(hazards.ToList());
                var budgets = GenService.GetAll<InterventionBudget>().Where(x => x.InterventionId == interventioninfo.Id && x.Status == EntityStatus.Active);
                interventioninfo.InterventionBudgets = Mapper.Map<List<InterventionBudgetDto>>(budgets.ToList());
                var benefits = GenService.GetAll<InterventionExpectedbenfits>().Where(x => x.InterventionId == interventioninfo.Id && x.Status == EntityStatus.Active);
                interventioninfo.InterventionExpectedbenfits = Mapper.Map<List<InterventionExpectedbenfitsDto>>(benefits.ToList());
                return interventioninfo;
            }
            else
                return null;
        }
        public ResponseDto SaveImplementation(InterventionImplementationDto dto, long? userId)
        {
            var entity = new InterventionImplementation();
            ResponseDto responce = new ResponseDto();

            var expenses = new List<InterventionExpenditures>();
            var expense = new InterventionExpenditures();
            double owncontribution = 0;
            double totalexpense = 0;

            if (dto.Expenses != null && dto.Expenses.Count > 0)
            {
                foreach (var item in dto.Expenses)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        expense = new InterventionExpenditures();
                        expense = GenService.GetById<InterventionExpenditures>((long)item.Id);
                        item.CreatedBy = expense.CreatedBy;
                        item.CreateDate = expense.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = EntityStatus.Active;
                        totalexpense += expense.CostAmount;
                        if (item.SourceId == 2)
                            owncontribution += expense.CostAmount;
                        Mapper.Map(item, expense);
                        GenService.Save(expense, false);
                        expenses.Add(expense);
                    }
                    else
                    {
                        expense = Mapper.Map<InterventionExpenditures>(item);
                        expense.CreateDate = DateTime.Now;
                        expense.CreatedBy = userId;
                        expense.Status = EntityStatus.Active;
                        if (item.SourceId == 2)
                            owncontribution += expense.CostAmount;
                        totalexpense += expense.CostAmount;
                        expenses.Add(expense);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<InterventionImplementation>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.Expenses = expenses;
                    entity.OwnContribution = owncontribution;
                    entity.ActualCost = totalexpense;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Intervention Implementation has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<InterventionImplementation>(dto);
                    entity.Expenses = expenses;
                    entity.OwnContribution = owncontribution;
                    entity.ActualCost = totalexpense;
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Intervention Implementation has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Intervention Implementation Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public InterventionImplementationDto LoadInterventionImplementation(long iid)
        {
            InterventionImplementationDto Implementationinfo = new InterventionImplementationDto();
            var data = GenService.GetAll<InterventionImplementation>().Where(x => x.InterventionId == iid).FirstOrDefault();
            if (data != null)
            {
                Implementationinfo = Mapper.Map<InterventionImplementationDto>(data);
                var expenses = GenService.GetAll<InterventionExpenditures>().Where(x => x.ImplementationId == Implementationinfo.Id && x.Status == EntityStatus.Active);
                Implementationinfo.Expenses = Mapper.Map<List<InterventionExpendituresDto>>(expenses.ToList());

                return Implementationinfo;
            }
            else
                return null;
        }
        public ResponseDto SaveAdaptionCapacityDevelopment(AdaptionCapacityDevelopmentDto dto, long? userId)
        {
            var entity = new AdaptionCapacityDevelopment();
            ResponseDto responce = new ResponseDto();

            var trainings = new List<Trainings>();
            var training = new Trainings();



            if (dto.Trainings != null && dto.Trainings.Count > 0)
            {
                foreach (var item in dto.Trainings)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        training = new Trainings();
                        training = GenService.GetById<Trainings>((long)item.Id);
                        item.CreatedBy = training.CreatedBy;
                        item.CreateDate = training.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = EntityStatus.Active;
                        Mapper.Map(item, training);
                        GenService.Save(training, false);
                        trainings.Add(training);
                    }
                    else
                    {
                        training = Mapper.Map<Trainings>(item);
                        training.CreateDate = DateTime.Now;
                        training.CreatedBy = userId;
                        training.Status = EntityStatus.Active;
                        trainings.Add(training);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<AdaptionCapacityDevelopment>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.Trainings = trainings;

                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Adaption Capacity Development has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<AdaptionCapacityDevelopment>(dto);
                    entity.Trainings = trainings;

                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Adaption Capacity Development has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Adaption Capacity Development Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public AdaptionCapacityDevelopmentDto LoadADCData(long iid)
        {
            AdaptionCapacityDevelopmentDto ADCinfo = new AdaptionCapacityDevelopmentDto();
            var data = GenService.GetAll<AdaptionCapacityDevelopment>().Where(x => x.InterventionId == iid).FirstOrDefault();
            if (data != null)
            {
                ADCinfo = Mapper.Map<AdaptionCapacityDevelopmentDto>(data);
                var tarinings = GenService.GetAll<Trainings>().Where(x => x.AdaptionCapacityId == ADCinfo.Id && x.Status == EntityStatus.Active);
                ADCinfo.Trainings = Mapper.Map<List<TrainingsDto>>(tarinings.ToList());

                return ADCinfo;
            }
            else
                return null;
        }
        public ResponseDto SaveDisbursement(DisbursementHistoryDto dto, long? userId)
        {
            var entity = new DisbursementHistory();
            ResponseDto responce = new ResponseDto();

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<DisbursementHistory>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Disburement has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<DisbursementHistory>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Disburement has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Disburement Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<DisbursementHistoryDto> LoadDisbursementHistory(long interventionid)
        {
            List<DisbursementHistoryDto> disbursementhistory = new List<DisbursementHistoryDto>();
            var data = GenService.GetAll<DisbursementHistory>().Where(x => x.InterventionId == interventionid);
            if (data != null)
            {
                disbursementhistory = Mapper.Map<List<DisbursementHistoryDto>>(data.ToList());
                return disbursementhistory;
            }
            else
                return null;
        }
        #endregion

        public IPagedList<HouseHoldInformationDto> householdList(int pageSize, int pageCount, string searchString, long? ownership, long? occupation, long? distid, long? upazilaid, long? up, long? locationid, bool? savings, bool? womenheaded, bool? proximity, bool? ethnicminority, bool? adolsentmother, bool? disable, bool? migrated, bool? embankment, double? lowerlimit, double? upperlimit, int? lowerage, int? upperage)
        {
            var data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active);

            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                data = data.Where(m => m.HouseHoldNo.ToLower().Contains(searchString) || m.Village.LocationName.ToLower().Contains(searchString));
            }

            if (distid != null)
                data = data.Where(m => m.Village.Parent.Parent.Parent.ParentID == distid);
            if (upazilaid != null)
                data = data.Where(m => m.Village.Parent.Parent.ParentID == upazilaid);
            if (up != null)
                data = data.Where(m => m.Village.Parent.ParentID == up);
            if (locationid != null)
                data = data.Where(m => m.Village.ParentID == locationid);

            if (savings!=null)
                data = data.Where(m => m.HasSavings == savings);
            if (adolsentmother != null)
                data = data.Where(m => m.HasAdolsentMother == adolsentmother);
            if (migrated != null)
                data = data.Where(m => m.WentforWork == migrated);
            if (ethnicminority != null)
                data = data.Where(m => m.IsEthnicMinority == ethnicminority);

            if (lowerlimit == null)
                lowerlimit = 0;
            if (upperlimit == null)
                upperlimit = 9999999999999999999;

            data = data.Where(m => m.MonthlyAvgIncome >=lowerlimit && m.MonthlyAvgIncome <= upperlimit);

            if (lowerage == null)
                lowerage = 0;
            if (upperage == null)
                upperage = (int)500;

            data = data.Where(m =>( m.IsLivingLifeTime==false?( m.LivingPeriod >= lowerage && m.LivingPeriod <= upperage):true));

            var hhbuildings = GenService.GetAll<HouseHoldBuilding>().Where(x=>x.Status==EntityStatus.Active);
            
            var hhmembers =   GenService.GetAll<HouseHoldMembers>().Where(x => x.Status == EntityStatus.Active && x.RelationtoHH==Relations.HHead);
            //if (disable != null)
            //    householdList = householdList.Where(m => m.IsDisableMember == disable);

            var householdList = from el in data
                                join mem in hhmembers
                                on el.Id equals mem.HHId into hhbuildmem
                                from hbm in hhbuildmem.DefaultIfEmpty()
                                select new 
                                {
                                    Id = el.Id,
                                    HouseHoldNo = el.HouseHoldNo,
                                    UpId = el.Village.Parent.ParentID,
                                    Address = el.Village.Parent.Parent.LocationName + ", " + el.Village.Parent.LocationName + ", " + el.Village.LocationName,
                                    OccupationOfHH=hbm.OccupationId, 
                                    IsWomenHeaded=(hbm.RelationtoHH==Relations.HHead && hbm.Gender==Gender.Female?true:false),
                                   
                                    
                                };
            ///IsDisableMember = (hbm.IsDisable == true ? true : false),
            if (womenheaded != null)
                householdList = householdList.Where(m => m.IsWomenHeaded == womenheaded);
            
            if (occupation != null)
                householdList = householdList.Where(m => m.OccupationOfHH == occupation);


            var result = from el in householdList
                         join bld in hhbuildings
                                on el.Id equals bld.HHId into hhbuild
                                from nt in hhbuild.DefaultIfEmpty()
                                select new HouseHoldInformationDto
                                {
                                    Id = el.Id,
                                    HouseHoldNo = el.HouseHoldNo,
                                    UpId = el.UpId,
                                    Address = el.Address,
                                    ProximitytoRiver = nt.ProximitytoRiver,
                                    RentalType = (int)nt.RentalType,
                                    IsEmbankment = nt.IsInsideEmbankment
                                };
            if (proximity != null)
                result = result.Where(m => m.ProximitytoRiver == proximity);
            if (ownership != null)
                result = result.Where(m => m.RentalType == ownership);
            if (embankment != null)
                result = result.Where(m => m.IsEmbankment == embankment);

            return result.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }
        public IPagedList<HouseHoldListDto> householdWaitingList(int pageSize, int pageCount, string searchString, long? up, long? location)
        {
            var data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active && c.IsEligibleforScheme == false);

            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                data = data.Where(m => m.HouseHoldNo.ToLower().Contains(searchString) || m.Village.LocationName.ToLower().Contains(searchString));
            }

            if (location != null)
            {
                var loc = GenService.GetById<Location>((long)location);
                if (up != null && loc.ParentID == up)
                {
                    data = data.Where(m => m.Village.ParentID == location);

                }
                else
                    data = data.Where(m => m.Village.Parent.LocationName.Contains(loc.LocationName));
            }
            else
            {
                if (up != null)
                    data = data.Where(m => m.Village.Parent.ParentID == up);
            }

            var householdWaitingList = from el in data
                                       select new HouseHoldListDto
                                       {
                                           HHId = el.Id,
                                           HouseHoldNo = el.HouseHoldNo,
                                           UPId = el.Village.Parent.ParentID,
                                           Address = el.Village.Parent.Parent.LocationName + ", " + el.Village.Parent.LocationName + ", " + el.Village.LocationName,
                                           IsSelected = el.IsEligibleforScheme,
                                       };

            return householdWaitingList.OrderBy(r => r.HHId).ToPagedList(pageCount, pageSize);

        }
        public IPagedList<HouseHoldListDto> SelectedHHList(int pageSize, int pageCount, string searchString, long? up, long? location)
        {
            var data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active  && c.IsEligibleforScheme == true);
 
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                data = data.Where(m => m.HouseHoldNo.ToLower().Contains(searchString) || m.Village.LocationName.ToLower().Contains(searchString));
            }

            if (location != null)
            {
                var loc = GenService.GetById<Location>((long)location);
                if (up != null && loc.ParentID == up)
                {
                    data = data.Where(m => m.Village.ParentID == location);

                }
                else
                    data = data.Where(m => m.Village.Parent.LocationName.Contains(loc.LocationName));
            }
            else
            {
                if (up != null)
                    data = data.Where(m => m.Village.Parent.ParentID == up);
            }

            var interventionlist = GenService.GetAll<Interventions>().Where(c => c.Status == EntityStatus.Active && (up!=null ? c.HouseHold.Village.Parent.ParentID == up:true));
            var disbursmenthistory = GenService.GetAll<DisbursementHistory>().Where(c => c.Status == EntityStatus.Active).GroupBy(x => x.InterventionId).Select(a => new { DisbursedAmount = a.Sum(b => b.DisbursedAmount), InterventionId = a.Key.Value });
            var result = (from item in data
                          join inter in interventionlist
                          on item.Id equals inter.HHId
                          into ps
                          from p in ps.DefaultIfEmpty()
                          join budget in disbursmenthistory
                          on p.Id equals budget.InterventionId
                          into final
                          from f in final.DefaultIfEmpty()
                          select new HouseHoldListDto
                          {
                              HHId = item.Id,
                              InterventionId = p.Id,
                              HouseHoldNo = item.HouseHoldNo,
                              Address = item.Village.Parent.Parent.LocationName + ", " + item.Village.Parent.LocationName + ", " + item.Village.LocationName,
                              GrantedAmount = (p.Id > 0 ? p.CRFGrant : 0),
                              DisbursedAmount = (f.InterventionId > 0 ? f.DisbursedAmount : 0),
                              IsSelected = item.IsEligibleforScheme,
                              HaveIntervertion = (p.Id > 0 ? true : false)
                          }).ToList();

            return result.OrderBy(r => r.HHId).ToPagedList(pageCount, pageSize);

        }

        public IPagedList<HouseHoldPotentialImpactDto> VulnerableHHList(int pageSize, int pageCount, string searchString, long? up)
        {

            var householdpi = GenService.GetAll<HouseHoldPotentialImpact>().Where(m => m.Status == EntityStatus.Active).GroupBy(n=>n.HouseHold);
            var householdList = from el in householdpi
                                select new HouseHoldPotentialImpactDto
                                {
                                            HHId = el.Key.Id,
                                            HouseHoldNo = el.Key.HouseHoldNo,
                                            VulnerabilityScore = el.Sum(x=>(decimal)x.VulnerabilityScore),
                                            UPId=el.Key.Village.Parent.ParentID,
                                            LocationName = el.Key.Village.Parent.Parent.LocationName + ", " + el.Key.Village.Parent.LocationName + ", " + el.Key.Village.LocationName,

                                        };


            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                householdList = householdList.Where(m => m.HouseHoldNo.ToLower().Contains(searchString) || m.LocationName.ToLower().Contains(searchString));
            }

            if (up != null)
                householdList = householdList.Where(m => m.UPId == up);

           
            

            return householdList.OrderBy(r => r.HHId).ToPagedList(pageCount, pageSize);

        }
        public List<DisbursementDashboardDataDto> LoadDisbursementData(long locid)
        {
            var disbursementinfo = GenService.GetAll<DisbursementHistory>().Where(c => c.Status == EntityStatus.Active && c.Intervention.HouseHold.Village.Parent.ParentID == locid);

            var disbursements = (from dis in disbursementinfo
                                 group dis by dis.Intervention
                                into middle
                                 select new DisbursementDashboardDataDto
                                 {
                                     SchemeCategoryName = middle.Key.InterventionCategory.CategoryName,
                                     InterventionCategoryId = middle.Key.InterventionCategoryId,
                                     InterventionCount = middle.Count(),
                                     GrantedAmount = middle.Sum(x => x.Intervention.CRFGrant),
                                     DisbursedAmount = middle.Sum(x => x.DisbursedAmount)
                                 }).ToList();

            var data = (from dis in disbursements
                        group dis by new { dis.InterventionCategoryId, dis.SchemeCategoryName }
                       into final
                        select new DisbursementDashboardDataDto
                        {
                            SchemeCategoryName = final.Key.SchemeCategoryName,
                            InterventionCategoryId = final.Key.InterventionCategoryId,
                            InterventionCount = final.Count(),
                            GrantedAmount = final.Sum(x => x.GrantedAmount),
                            DisbursedAmount = final.Sum(x => x.DisbursedAmount)

                        }).ToList();
            return data;
        }
        public HouseHoldDashboardDto LoadHouseHoldDashboardData(long locid)
        {
            var householdinfo = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active && c.Village.Parent.ParentID == locid);
            var hhcount = householdinfo.Count();
            var selectedhhcount = householdinfo.Where(x => x.IsEligibleforScheme == true).Count();
            var interventioninfo = GenService.GetAll<Interventions>().Where(c => c.Status == EntityStatus.Active && c.HouseHold.Village.Parent.ParentID == locid);
            var proposedinterventioncount = interventioninfo.Count();
            var disbursementinfo = GenService.GetAll<DisbursementHistory>().Where(c => c.Status == EntityStatus.Active && c.Intervention.HouseHold.Village.Parent.ParentID == locid);
            var totaldisbursement = disbursementinfo.Sum(x => x.DisbursedAmount);

            var data = new HouseHoldDashboardDto
            {
                HHCount = hhcount,
                SelectedHHCount = selectedhhcount,
                ProposedInterventionCount = proposedinterventioncount,
                TotalDisbursement = totaldisbursement
            };
            return data;
        }

        #region HouseHold Analysis
        public IPagedList<HouseHoldInformationDto> householdAnalysisList(int pageSize, int pageCount, string searchString, long? up, long? location)
        {

            var householdanalysis = GenService.GetAll<HouseHoldInformation>().Where(m => m.Status == EntityStatus.Active && m.IsEligibleforScheme==true);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                householdanalysis = householdanalysis.Where(m => m.HouseHoldNo.ToLower().Contains(searchString) || m.Village.LocationName.ToLower().Contains(searchString));
            }

            if (location != null)
            {
                var loc = GenService.GetById<Location>((long)location);
                if (up != null && loc.ParentID == up)
                {
                    householdanalysis = householdanalysis.Where(m => m.Village.ParentID == location);

                }
                else
                    householdanalysis = householdanalysis.Where(m => m.Village.Parent.LocationName.Contains(loc.LocationName));
            }
            else
            {
                if (up != null)
                    householdanalysis = householdanalysis.Where(m => m.Village.Parent.ParentID == up);
            }

            var householdAnalysisList = from el in householdanalysis
                                        select new HouseHoldInformationDto
                                        {
                                            Id = el.Id,
                                            HouseHoldNo = el.HouseHoldNo,
                                            VillageId = el.VillageId,
                                            Address = el.Address,
                                            Details = el.Details,
                                            LocationName = el.Village.Parent.Parent.LocationName + ", " + el.Village.Parent.LocationName + ", " + el.Village.LocationName,

                                        };

            return householdAnalysisList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        public HouseHoldInformationDto getHouseholdAnalysisInfoById(long id)
        {
            var data = GenService.GetById<HouseHoldInformation>(id);
            return Mapper.Map<HouseHoldInformationDto>(data);
        }
        public decimal getHazardPercentageById(long hazardid, long locationid)
        {
            //var location = GenService.GetById<Location>(locationid);
            var allhazard = GenService.GetAll<HazardLocationMapping>().Where(x => x.LocationId == locationid).Sum(x => x.Consequence);
            var specifichazard = GenService.GetAll<HazardLocationMapping>().Where(y => y.HazardId == hazardid && y.LocationId == locationid).FirstOrDefault().Consequence;
            return (decimal)specifichazard / allhazard;
        }
        public decimal getTypeWeightById(long id)
        {
            var data = GenService.GetById<ElementType>(id);
            return data.Weight;
        }

        public ResponseDto SaveAnalysisData(HouseHoldElementsAnalysisDto dto, long? userId)
        {
            var entity = new HouseHoldElementsAnalysis();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldElementsAnalysis>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Analysis Data been Edited Successfully";
                }
                else
                {
                    var existing = GenService.GetAll<HouseHoldElementsAnalysis>().Where(x => x.ElementTypeId == dto.ElementTypeId && x.HazardId == dto.HazardId && x.HHId == dto.HHId).ToList();
                    if (existing.Count == 0)
                    {
                        entity = Mapper.Map<HouseHoldElementsAnalysis>(dto);
                        entity.Status = EntityStatus.Active;
                        entity.CreateDate = DateTime.Now;
                        entity.CreatedBy = userId;
                        GenService.Save(entity);
                        responce.Success = true;
                        responce.Message = "Analysis Data been Saved Successfully";
                    }
                    else
                    {
                        responce.Success = false;
                        responce.Message = "Analysis Data Duplicate!";
                    }
                }
            }
            catch (Exception)
            {
                responce.Message = "Save Failed!";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<HouseHoldElementsAnalysisDto> LoadAnalysisData(long hhid)
        {
            var data = GenService.GetAll<HouseHoldElementsAnalysis>().Where(c => c.Status == EntityStatus.Active && c.HHId == hhid);
            return Mapper.Map<List<HouseHoldElementsAnalysisDto>>(data);
        }
        #endregion


        public List<LocationDto> GetChildLocations(long locationId)
        {
            var result = new List<LocationDto>();
            var ListData = GenService.GetAll<Location>().Where(l => l.Status == EntityStatus.Active && l.ParentID == locationId).ToList();
            foreach (var data in ListData)
            {
                result.Add(new LocationDto
                {
                    Id = data.Id,
                    LocationName = data.LocationName,
                    ParentID = data.ParentID,
                    ParentName = data.ParentID != null ? data.Parent.LocationName : "",
                    Level = data.Level
                });
                if (data.Level < LocationLevel.Village && GenService.GetAll<Location>().Where(l => l.ParentID == data.Id).Any())
                {
                    var children = GetChildLocations(data.Id);
                    if (children.Any())
                        result.AddRange(children);
                }
            }
            return result;
        }

        #region Potentail Impact
        public List<ElementTypeDto> GetElementTypesByRating(long hhid)
        {
            var types = GenService.GetAll<ElementType>().Where(c => c.Status == EntityStatus.Active);
            var data = GenService.GetAll<HouseHoldElementsAnalysis>().Where(x => x.HHId == hhid);
            var refined = from item in data
                          select new
                          {
                              TypeName = item.ElementType.TypeName,
                              Id = item.ElementTypeId,
                              Total = (item.ExposedValue + item.SensibilityValue)
                          };
            var result = refined.GroupBy(x => new { x.Id, x.TypeName }).Select(i => new ElementTypeDto
            {
                Id = i.Key.Id,
                TypeName = i.Key.TypeName,
                Total = i.Sum(s => s.Total)
            }).ToList();
            //result=result.Where(y => y.Total > 1);
            ///return Mapper.Map<List<ElementTypeDto>>(result);
            return result;
        }
        public ResponseDto SavePI(HouseHoldPotentialImpactDto dto, long? userId)
        {
            var entity = new HouseHoldPotentialImpact();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldPotentialImpact>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Potentail Impact Data been Edited Successfully";
                }
                else
                {
                    var existing = GenService.GetAll<HouseHoldPotentialImpact>().Where(x => x.ElementTypeId == dto.ElementTypeId && x.HHId == dto.HHId).ToList();
                    if (existing.Count == 0)
                    {
                        entity = Mapper.Map<HouseHoldPotentialImpact>(dto);
                        entity.Status = EntityStatus.Active;
                        entity.CreateDate = DateTime.Now;
                        entity.CreatedBy = userId;
                        GenService.Save(entity);
                        responce.Success = true;
                        responce.Message = "Potentail Impact Data been Saved Successfully";
                    }
                    else
                    {
                        responce.Success = false;
                        responce.Message = "Duplicate! Potentail Impact Data is already exist";
                    }
                }
            }
            catch (Exception)
            {
                responce.Message = "Save Failed!";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<HouseHoldPotentialImpactDto> LoadPIData(long hhid)
        {
            var data = GenService.GetAll<HouseHoldPotentialImpact>().Where(c => c.Status == EntityStatus.Active && c.HHId == hhid);
            return Mapper.Map<List<HouseHoldPotentialImpactDto>>(data);
        }
        #endregion

        #region Crops
        public ResponseDto SaveCrop(CropsDto dto, long? userId)
        {
            var entity = new Crops();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Crops>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Crop Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Crops>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Crop Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Crop Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<CropsDto> GetCrops()
        {
            var data = GenService.GetAll<Crops>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<CropsDto>>(data);
        }
        #endregion

        #region CultivationSeason
        public ResponseDto SaveCultivationSeason(CropSeasonDto dto, long? userId)
        {
            var entity = new CultivationSeason();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<CultivationSeason>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Season Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<CultivationSeason>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Season Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Season Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<EngBangMonthMappingDto> GetMonths()
        {
            var data = GenService.GetAll<EngBangMonthMapping>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<EngBangMonthMappingDto>>(data);
        }

        public List<CropSeasonDto> GetCultivationSeason()
        {
            var data = GenService.GetAll<CultivationSeason>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<CropSeasonDto>>(data);
        }
        #endregion

        #region SourceOfFund
        public ResponseDto SaveSourceOfFund(SourceOfFundDto dto, long? userId)
        {
            var entity = new SourceOfFund();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<SourceOfFund>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Source Of Fund Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<SourceOfFund>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Source Of Fund Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Source Of Fund Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<SourceOfFundDto> GetSourceOfFund()
        {
            var data = GenService.GetAll<SourceOfFund>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<SourceOfFundDto>>(data);
        }


        #endregion

        #region SupportType
        public ResponseDto SaveSupportType(SupportTypeDto dto, long? userId)
        {
            var entity = new SupportType();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<SupportType>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Support Type Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<SupportType>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Support Type Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Support Type Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<SupportTypeDto> GetSupportType()
        {
            var data = GenService.GetAll<SupportType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<SupportTypeDto>>(data);
        }

        #endregion

        #region WaterSource
        public ResponseDto SaveWaterSource(WaterSourceDto dto, long? userId)
        {
            var entity = new WaterSource();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<WaterSource>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Water Source Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<WaterSource>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Water Source Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Water Source Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<WaterSourceDto> GetWaterSource()
        {
            var data = GenService.GetAll<WaterSource>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<WaterSourceDto>>(data);
        }

        #endregion

        #region LandType
        public ResponseDto SaveLandType(LandTypeDto dto, long? userId)
        {
            var entity = new LandType();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<LandType>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Land Type Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<LandType>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Land Type Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Land Type Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<LandTypeDto> GetLandType()
        {
            var data = GenService.GetAll<LandType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<LandTypeDto>>(data);
        }

        #endregion

        public HouseHoldPotentialImpactDto LoadPotentialImpactInfo(long piid)
        {
            HouseHoldPotentialImpactDto piinfo = new HouseHoldPotentialImpactDto();
            var data = GenService.GetById<HouseHoldPotentialImpact>(piid);
            if (data != null)
            {
                piinfo = Mapper.Map<HouseHoldPotentialImpactDto>(data);
                var hhindicatorscores = GenService.GetAll<HHIndicatorScore>().Where(x => x.RiskId == piinfo.Id && x.Status == EntityStatus.Active);
                piinfo.HHIndicatorScores = Mapper.Map<List<HHIndicatorScoreDto>>(hhindicatorscores.ToList());
                return piinfo;
            }
            else
                return null;
        }
        public ResponseDto SaveADC(HouseHoldPotentialImpactDto dto, long? userId)
        {
            var entity = new HouseHoldPotentialImpact();
            var hhindicator = new HHIndicatorScore();
            var hhindicators = new List<HHIndicatorScore>();
            var piweight = GenService.GetById<IndicatorType>(1).Weight;
            var adcweight = GenService.GetById<IndicatorType>(2).Weight;
            decimal totalscore = 0;
            decimal totalweight = dto.HHIndicatorScores.Sum(x => x.Score);
            decimal adc = 0;
            ResponseDto responce = new ResponseDto();

            if (dto.HHIndicatorScores != null && dto.HHIndicatorScores.Count > 0)
            {
                foreach (var item in dto.HHIndicatorScores)
                {

                    if (item.Id > 0 || item.Id != null)
                    {
                        hhindicator = GenService.GetById<HHIndicatorScore>((long)item.Id);
                        item.CreatedBy = hhindicator.CreatedBy;
                        item.CreateDate = hhindicator.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = hhindicator.Status;
                        if (item.IsSelected == false)
                            item.Score = 0;
                        item.CalulatedDate = DateTime.Now;
                        Mapper.Map(item, hhindicator);
                        GenService.Save(hhindicator, false);
                        hhindicators.Add(hhindicator);
                    }
                    else
                    {
                        if (item.IsSelected == false)
                            item.Score = 0;
                        hhindicator = Mapper.Map<HHIndicatorScore>(item);
                        hhindicator.CreateDate = DateTime.Now;
                        hhindicator.CreatedBy = userId;
                        hhindicator.CalulatedDate = DateTime.Now;
                        hhindicator.Status = EntityStatus.Active;
                        hhindicators.Add(hhindicator);
                    }
                    totalscore += ((item.IsSelected == true ? item.Score * 1 : item.Score * 0));

                }
            }
            adc = totalscore / totalweight;
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldPotentialImpact>((long)dto.Id);
                    entity.HHIndicatorScores = hhindicators;
                    entity.AdaptiveCapacityScore = adc;
                    entity.VulnerabilityScore = (dto.Rating * piweight) + (adc * adcweight);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Potentail Impact Data been Edited Successfully";
                }

            }
            catch (Exception)
            {
                responce.Message = "Save Failed!";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<OtherBuildingDto> GetOtherBuildings()
        {
            var data = GenService.GetAll<OtherBuilding>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<OtherBuildingDto>>(data);
        }

        public List<AgriculturalToolsDto> GetAgriTools()
        {
            var data = GenService.GetAll<AgriculturalTools>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<AgriculturalToolsDto>>(data);
        }

        #region AgriculturalTools
        public ResponseDto SaveAgriculturalTools(AgriculturalToolsDto dto, long? userId)
        {
            var entity = new AgriculturalTools();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<AgriculturalTools>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Agricultural Tools Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<AgriculturalTools>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Agricultural Tools Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Agricultural Tools Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<AgriculturalToolsDto> GetAgriculturalTools()
        {
            var data = GenService.GetAll<AgriculturalTools>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<AgriculturalToolsDto>>(data);
        }

        #endregion

        #region EthnicMinority
        public ResponseDto SaveEthnicMinority(EthnicMinorityDto dto, long? userId)
        {
            var entity = new EthnicMinority();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<EthnicMinority>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Ethnic Minority Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<EthnicMinority>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Ethnic Minority Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Ethnic Minority Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<EthnicMinorityDto> GetEthnicMinority()
        {
            var data = GenService.GetAll<EthnicMinority>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<EthnicMinorityDto>>(data);
        }

        #endregion


        #region HouseHoldRRAP
        public ResponseDto SaveRiskReductionActionPlan(HouseHoldRRAPDto dto, long? userId)
        {
            var entity = new HouseHoldRRAP();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HouseHoldRRAP>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Reduction Action Plan Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HouseHoldRRAP>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Risk Reduction Action Plan Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Risk Reduction Action Plan Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        //public List<AgriculturalToolsDto> GetAgriculturalTools()
        //{
        //    var data = GenService.GetAll<AgriculturalTools>().Where(c => c.Status == EntityStatus.Active);
        //    return Mapper.Map<List<AgriculturalToolsDto>>(data);
        //}
        public HouseHoldRRAPDto LoadRRAPData(long hhid)
        {
            var data = GenService.GetAll<HouseHoldRRAP>().Where(c => c.Status == EntityStatus.Active && c.HHId == hhid).FirstOrDefault();
            if (data != null)
                return Mapper.Map<HouseHoldRRAPDto>(data);
            else
                return null;
        }
        #endregion

        #region List of household report 
        public List<HouseHoldInformationDto> GetListOfHousehold(long? upid)
        {
            List<HouseHoldInformation> data = new List<HouseHoldInformation>();
            if (upid != null)
                data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active && c.Village.Parent.ParentID == upid).ToList();
            else
                data = GenService.GetAll<HouseHoldInformation>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<HouseHoldInformationDto>>(data);
        }
        #endregion
    }
}
