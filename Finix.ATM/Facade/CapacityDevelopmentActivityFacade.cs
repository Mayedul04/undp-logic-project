﻿using AutoMapper;
using Finix.ATM.DTO;
using Finix.ATM.Infrastructure;
using Finix.ATM.Infrastructure.Models;
using Finix.Auth.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Facade
{
   public class CapacityDevelopmentActivityFacade :BaseFacade
    {
        #region Event Types
        public ResponseDto SaveEventType(EventTypeDto dto, long? userId)
        {
            var entity = new EventType();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<EventType>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Event Type Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<EventType>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Event Type Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Event Type Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<EventTypeDto> GetEventTypes()
        {
            var data = GenService.GetAll<EventType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<EventTypeDto>>(data);
        }
        #endregion
        #region Capacity Development Activity
        public ResponseDto SaveApacityDevelopmentActivity(CapacityDevelopmentActivityDto dto, long? userId)
        {
            var entity = new CapacityDevelopmentActivity();
            ResponseDto responce = new ResponseDto();

            var participants = new List<Participants>();
            var participant = new Participants();


            if (dto.Participants != null && dto.Participants.Count > 0)
            {
                foreach (var item in dto.Participants)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        participant = new Participants();
                        participant = GenService.GetById<Participants>((long)item.Id);
                        item.CreatedBy = participant.CreatedBy;
                        item.CreateDate = participant.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = EntityStatus.Active;

                        Mapper.Map(item, participant);
                        GenService.Save(participant, false);
                        participants.Add(participant);
                    }
                    else
                    {
                        participant = Mapper.Map<Participants>(item);
                        participant.CreateDate = DateTime.Now;
                        participant.CreatedBy = userId;
                        participant.Status = EntityStatus.Active;

                        participants.Add(participant);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<CapacityDevelopmentActivity>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.Participants = participants;

                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Capacity Development Activity has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<CapacityDevelopmentActivity>(dto);
                    entity.Participants = participants;

                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Capacity Development Activity has been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Capacity Development Activity Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public CapacityDevelopmentActivityDto LoadDevelopmentActivityData(long id)
        {
            CapacityDevelopmentActivityDto info = new CapacityDevelopmentActivityDto();
            var data = GenService.GetById<CapacityDevelopmentActivity>(id);
            if (data != null)
            {
                info = Mapper.Map<CapacityDevelopmentActivityDto>(data);
                var participants = GenService.GetAll<Participants>().Where(x => x.CapacityDevelopmentActivityId == info.Id && x.Status == EntityStatus.Active);
                info.Participants = Mapper.Map<List<ParticipantsDto>>(participants.ToList());

                return info;
            }
            else
                return null;
        }

        public List<CapacityDevelopmentActivityDto> GetDevelopmentActivityList()
        {
            var data = GenService.GetAll<CapacityDevelopmentActivity>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<CapacityDevelopmentActivityDto>>(data);
        }
        public IPagedList<CapacityDevelopmentActivityDto> CDAList(int pageSize, int pageCount, string searchString, long? up)
        {

            var cdainfo = GenService.GetAll<CapacityDevelopmentActivity>().Where(m => m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                cdainfo = cdainfo.Where(m => m.EventName.ToLower().Contains(searchString));
            }
            if (up != null)
                cdainfo = cdainfo.Where(m => m.LocationId == up);
            var cdaList = Mapper.Map<List<CapacityDevelopmentActivityDto>>(cdainfo);
            return cdaList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }
        #endregion
    }
}
