﻿using AutoMapper;
using Finix.ATM.DTO;
using Finix.ATM.Infrastructure;
using Finix.ATM.Infrastructure.Models;
using Finix.ATM.Service;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Finix.ATM.Facade
{
    public class LocationFacade : BaseFacade
    {
        #region Locations
        public List<LocationDto> GetParents(LocationLevel level)
        {
            var parents = new List<Location>();
            if (level == LocationLevel.Division)
            {
                var country = GenService.GetById<Location>(1);
                parents.Add(country);
            }
            else
            {
                var locations = GenService.GetAll<Location>().Where(c => c.Status == EntityStatus.Active && c.Level == level - 1).ToList();
                parents.AddRange(locations);
            }
            return Mapper.Map<List<LocationDto>>(parents.ToList());
        }
        public IPagedList<LocationDto> LocationList(int pageSize, int pageCount, string searchString, long? up, long? location)
        {

            var locations = GenService.GetAll<Location>().Where(m => m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                locations = locations.Where(m => m.LocationName.ToLower().Contains(searchString));
            }

            if (location != null)
                locations = locations.Where(m => m.Id == location);

            //if (location != null)
            //    locations = locations.Where(m => m.ParentID == location);

            if (up != null)
                locations = locations.Where(m => m.ParentID == up);

            var locationList = Mapper.Map<List<LocationDto>>(locations);
            //from loc in locations
            //               select new LocationDto
            //               {
            //                   Id = loc.Id,
            //                   LocationName = loc.LocationName,
            //                   Level = loc.Level,
            //                   LevelName = Util.UiUtil.GetDisplayName(loc.Level),
            //                  Details = loc.Details,
            //                  ParentID = loc.ParentID,
            //                  ParentName = loc.Parent.LocationName,
            //                  Latitude = loc.Latitude,
            //                  Longitude = loc.Longitude,
            //                  Code=loc.Code
            //              };

            return locationList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }
        public ResponseDto SaveLocations(LocationDto dto, long? userId)
        {
            var entity = new Location();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Location>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Location Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Location>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Location Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Location Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<LocationDto> GetLocationsByLevel(LocationLevel level, long? parent)
        {
            var locations = new List<Location>();
            if (level == LocationLevel.Country)
            {
                var country = GenService.GetById<Location>(1);
                locations.Add(country);
            }
            else
            {
                var location = GenService.GetAll<Location>().Where(c => c.Status == EntityStatus.Active && c.Level == level).ToList();
                locations.AddRange(location);
            }

            if (parent != null)
                locations = locations.Where(x => x.ParentID == parent).ToList();
            return Mapper.Map<List<LocationDto>>(locations.ToList());
        }

        public LocationDto GetLocationById(long id)
        {
            var data = GenService.GetById<Location>(id);
            return Mapper.Map<LocationDto>(data);
        }
        #endregion

        #region Hazard Set up
        public ResponseDto SaveHazards(HazardDto dto, long? userId)
        {
            var entity = new Hazard();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Hazard>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Hazard Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Hazard>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Hazard Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Hazard Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<HazardDto> GetHazards()
        {
            var data = GenService.GetAll<Hazard>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<HazardDto>>(data);
        }
        #endregion

        #region Element Setup
        public ResponseDto SaveElements(ElementDto dto, long? userId)
        {
            var entity = new Element();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Element>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Element Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Element>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Element Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Element Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<ElementTypeDto> GetElementTypes()
        {
            var data = GenService.GetAll<ElementType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<ElementTypeDto>>(data);
        }
        public IPagedList<ElementDto> ElementList(int pageSize, int pageCount, string searchString, long? location)
        {

            var elements = GenService.GetAll<Element>().Where(m => m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                elements = elements.Where(m => m.Name.ToLower().Contains(searchString));
            }

            if (location != null)
                elements = elements.Where(m => m.LocationId == location);

            var elementList = from el in elements
                              select new ElementDto
                              {
                                  Id = el.Id,
                                  Name = el.Name,
                                  Details = el.Details,
                                  ElementTypeId = el.ElementTypeId,
                                  ElementTypeName = el.ElementType.TypeName,
                                  LocationId = el.LocationId,
                                  LocationName = el.Location.Parent.LocationName + ", " + el.Location.LocationName,
                                  Latitude = el.Latitude,
                                  Longitude = el.Longitude,
                              };

            return elementList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        public ElementDto GetElementById(long id)
        {
            var data = GenService.GetById<Element>(id);
            return Mapper.Map<ElementDto>(data);
        }
        public List<ElementDto> GetElements(long? locationid)
        {
            List<Element> data = new List<Element>();
            if (locationid != null)
                data = GenService.GetAll<Element>().Where(c => c.Status == EntityStatus.Active && (c.LocationId == locationid || c.Location.ParentID == locationid)).ToList();
            else
                data = GenService.GetAll<Element>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<ElementDto>>(data);
        }

        public ResponseDto SaveElementType(ElementTypeDto dto, long? userId)
        {
            var entity = new ElementType();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<ElementType>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Element Type Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<ElementType>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Element Type Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Element Type Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        #endregion

        
        public ResponseDto SaveElemntAtRisk(ElementAtRiskDto dto, long? userId)
        {
            var entity = new ElementAtRisk();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<ElementAtRisk>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Data Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<ElementAtRisk>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Data Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Failed!";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<ElementAtRiskDto> GetAllElementsAtRisk()
        {
            var data = GenService.GetAll<ElementAtRisk>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<ElementAtRiskDto>>(data);
        }
        public List<HazardDto> AllHazardByLocation(long? locationid)
        {
            var data = GenService.GetAll<HazardLocationMapping>().Where(c => c.Status == EntityStatus.Active);
            if (locationid != null)
                data = data.Where(x => x.LocationId == locationid || x.Location.ParentID == locationid);
            var hazards = (from l in data
                           select new HazardDto
                           {
                               Id = l.Id,
                               Name = l.Hazard.Name,
                               Details = l.Hazard.Details
                           }).ToList();
            return hazards;
        }
        //public List<HazardLocationMappingDto> AllHazardLocationMappings()
        //{
        //    var data = GenService.GetAll<HazardLocationMapping>().Where(c => c.Status == EntityStatus.Active);
        //    return Mapper.Map<List<HazardLocationMappingDto>>(data);
        //}

        #region Indicators
        public ResponseDto SaveIndicatorType(IndicatorTypeDto dto, long? userId)
        {
            var entity = new IndicatorType();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<IndicatorType>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Indicator Type Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<IndicatorType>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Indicator Type Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Indicator Type Save Failed";
            }
            GenService.SaveChanges();
            UpdateIndicatorType((IndicatorPrimaryGroup)dto.TypeGroup);
            return responce;
        }
        public ResponseDto UpdateIndicatorType(IndicatorPrimaryGroup groupid)
        {
            var updatedindicatortypes = new List<IndicatorType>();
            var types = GenService.GetAll<IndicatorType>().Where(x => x.TypeGroup == groupid).ToList();
            ResponseDto responce = new ResponseDto();
            decimal weight = (decimal)1/types.Count();
            foreach (var ind in types)
            {
                ind.Weight = weight;
                updatedindicatortypes.Add(ind);
            }
            try
            {
                GenService.Save(updatedindicatortypes);
                responce.Success = true;
                responce.Message = "New Weight Has been Set";
            }
            catch
            {

                responce.Message = "Failed!";
            }
            return responce;
        }
        public List<IndicatorTypeDto> GetAllIndicatorTypes()
        {
            var data = GenService.GetAll<IndicatorType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<IndicatorTypeDto>>(data);
        }
        public List<IndicatorsDto> GetAllIndicatorsByType(long typeid)
        {
            var data = GenService.GetAll<Indicators>().Where(c => c.Status == EntityStatus.Active && c.Type==typeid);
            return Mapper.Map<List<IndicatorsDto>>(data);
        }

        public ResponseDto SaveIndicators(List<IndicatorsDto> dto, long? userId)
        {
            var entity = new Indicators();
            
            ResponseDto responce = new ResponseDto();
            if (dto.Count > 0)
            {
                using (var tran = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in dto)
                        {
                            if (item.Id != null && item.Id > 0)
                            {
                                entity = GenService.GetById<Indicators>((long)item.Id);
                                item.CreateDate = entity.CreateDate;
                                item.CreatedBy = entity.CreatedBy;
                                Mapper.Map(item, entity);
                                entity.EditDate = DateTime.Now;
                                entity.EditedBy = userId;
                                entity.Status = EntityStatus.Active;

                                if (item.OldWeight != item.Weight)
                                {
                                    var entitylog = new IndicatorLog();
                                    entitylog.IndicatorId = item.Id;
                                    entitylog.OldValue = (decimal)item.OldWeight;
                                    entitylog.NewValue = (decimal)item.Weight;
                                    entitylog.CreateDate = entity.CreateDate;
                                    entitylog.CreatedBy = entity.CreatedBy;
                                    GenService.Save(entitylog);
                                }
                                
                                GenService.Save(entity);
 
                            }
                            else
                            {
                                entity = Mapper.Map<Indicators>(item);
                                entity.Status = EntityStatus.Active;
                                entity.CreateDate = DateTime.Now;
                                entity.CreatedBy = userId;

                                var entitylog = new IndicatorLog();
                                
                                entitylog.OldValue = (decimal)item.OldWeight;
                                entitylog.NewValue = (decimal)item.Weight;
                                entitylog.CreateDate = entity.CreateDate;
                                entitylog.CreatedBy = entity.CreatedBy;

                                GenService.Save(entity);
                                entitylog.IndicatorId = entity.Id;
                                GenService.Save(entitylog);
                            }
                        }
                        tran.Complete();
                        responce.Success = true;
                        responce.Message = "Indicator been Saved Successfully";
                    }
                    catch (Exception)
                    {
                        tran.Dispose();
                        responce.Success = false;
                        responce.Message = "Indicator Save Failed";
                    }
                }
                
            }
            return responce;
        }

        public List<IndicatorsDto> GetAllIndicators()
        {
            var data = GenService.GetAll<Indicators>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<IndicatorsDto>>(data);
        }
        #endregion

        public List<WaterSourceDto> GetAllWaterSource()
        {
            var data = GenService.GetAll<WaterSource>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<WaterSourceDto>>(data);
        }
        public List<SourceOfFundDto> GetAllFundSource()
        {
            var data = GenService.GetAll<SourceOfFund>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<SourceOfFundDto>>(data);
        }

        public List<SupportTypeDto> GetAllSupportTypes()
        {
            var data = GenService.GetAll<SupportType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<SupportTypeDto>>(data);
        }

        public List<EthnicMinorityDto> GetAllEthnicMinority()
        {
            var data = GenService.GetAll<EthnicMinority>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<EthnicMinorityDto>>(data);
        }
    }
}
