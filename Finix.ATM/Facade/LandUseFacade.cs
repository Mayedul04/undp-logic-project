﻿using AutoMapper;
using Finix.ATM.DTO;
using Finix.ATM.Infrastructure;
using Finix.ATM.Infrastructure.Models;
using Finix.ATM.Service;
using Finix.Auth.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Facade
{

   
    public class LandUseFacade : BaseFacade
    {
        #region Land Use
        public List<LandTypeDto> GetLandTypes(bool waterbody)
        {
            var data = GenService.GetAll<LandType>().Where(c => c.IsWaterBody==waterbody && c.Status == EntityStatus.Active);
            return Mapper.Map<List<LandTypeDto>>(data);
        }

        public ResponseDto SaveLandInfo(LandUseDto dto, long? userId)
        {
            var entity = new LandUse();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<LandUse>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Land Information is Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<LandUse>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Land Information is Saved Successfully";
                }
            }
            catch (Exception e)
            {
                responce.Message = e.ToString();
                responce.Message = "Land Information is Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public LandUseDto GetLandInfoById(long id)
        {
            var data = GenService.GetById<LandUse>(id);
            return Mapper.Map<LandUseDto>(data);
        }
        public List<LandUseDto> GetLandList()
        {
            var data = GenService.GetAll<LandUse>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<LandUseDto>>(data);
        }
        #endregion


        public List<EngBangMonthMappingDto> GetMonths()
        {
            var data = GenService.GetAll<EngBangMonthMapping>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<EngBangMonthMappingDto>>(data);
        }
        public List<CropSeasonDto> GetCropSeasons()
        {
            var data = GenService.GetAll<CultivationSeason>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<CropSeasonDto>>(data);
        }

        #region Crop Calendar
        public ResponseDto SaveCropCalendar(CropCalendarDto dto, long? userId)
        {
            var entity = new CropCalender();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<CropCalender>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Calendar Information is Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<CropCalender>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Calendar Information is Saved Successfully";
                }
            }
            catch (Exception e)
            {
                responce.Message = e.ToString();
                responce.Message = "Calendar Information is Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<CropsDto> GetCrops()
        {
            var data = GenService.GetAll<Crops>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<CropsDto>>(data);
        }


        public List<CropCalendarDto> GetCropCalendar(long? locid)
        {
            var data = new List<CropCalender>();
            if(locid!=null)
                data = GenService.GetAll<CropCalender>().Where(c => c.Status == EntityStatus.Active && c.LocationId==locid).ToList();
            else
                data = GenService.GetAll<CropCalender>().Where(c => c.Status == EntityStatus.Active).ToList();
            var newdata = (from item in data
                           select new CropCalendarDto
                           {
                               Id = item.Id,
                               CropId = item.CropId,
                               CropName = item.Crop.CropName,
                               ParentLocationId=(long)item.Location.ParentID,
                               LocationId=(long)item.LocationId,
                               LocationName=item.Location.Parent.LocationName + ", " + item.Location.LocationName,
                               EndMonthId = item.EndMonthId,
                               EndMonthName = item.EndMonth.EnglishName,
                               StartMonthId = item.StartMonthId,
                               StartMonthName = item.StartMonth.EnglishName,
                               January = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.January) ? "calendar" : (item.EndMonthId >= (int)Months.January ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.January && (int)Months.January <= item.EndMonthId) ? "calendar" : "")),
                               February =(item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.February) ? "calendar" : (item.EndMonthId >= (int)Months.February ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.February && (int)Months.February <= item.EndMonthId) ? "calendar" : "")),
                               March =   (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.March) ? "calendar" : (item.EndMonthId >= (int)Months.March ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.March && (int)Months.March <= item.EndMonthId) ? "calendar" : "")),
                               April =   (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.April) ? "calendar" : (item.EndMonthId >= (int)Months.April ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.April && (int)Months.April <= item.EndMonthId) ? "calendar" : "")),
                               May =     (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.May) ? "calendar" : (item.EndMonthId >= (int)Months.May ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.May && (int)Months.May <= item.EndMonthId) ? "calendar" : "")),
                               June =    (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.June) ? "calendar" : (item.EndMonthId >= (int)Months.June ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.June && (int)Months.June <= item.EndMonthId) ? "calendar" : "")),
                               July =    (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.July) ? "calendar" : (item.EndMonthId >= (int)Months.July ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.July && (int)Months.July <= item.EndMonthId) ? "calendar" : "")),
                               August =  (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.August) ? "calendar" : (item.EndMonthId >= (int)Months.August ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.August && (int)Months.August <= item.EndMonthId) ? "calendar" : "")),
                               September = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.September) ? "calendar" : (item.EndMonthId >= (int)Months.September ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.September && (int)Months.September <= item.EndMonthId) ? "calendar" : "")),
                               October = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.October) ? "calendar" : (item.EndMonthId >= (int)Months.October ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.October && (int)Months.October <= item.EndMonthId) ? "calendar" : "")),
                               November = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.November) ? "calendar" : (item.EndMonthId >= (int)Months.November ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.November && (int)Months.November <= item.EndMonthId) ? "calendar" : "")),
                               December = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.December) ? "calendar" : (item.EndMonthId >= (int)Months.December ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.December && (int)Months.December <= item.EndMonthId) ? "calendar" : ""))
                           }).ToList();
            return newdata;

        }
        public CropCalendarDto GetCropCalendarById(long id)
        {
            var data = GenService.GetById<CropCalender>(id);
            return Mapper.Map<CropCalendarDto>(data);
        }
        #endregion

        #region Hazard Calendar

        public ResponseDto SaveHazardCalendar(HazardLocationMappingDto dto, long? userId)
        {
            var entity = new HazardLocationMapping();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<HazardLocationMapping>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Calendar Information is Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HazardLocationMapping>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Calendar Information is Saved Successfully";
                }
            }
            catch (Exception e)
            {
                responce.Message = e.ToString();
                responce.Message = "Calendar Information is Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }


        public List<HazardLocationMappingDto> GetHazardCalendar(long? locid)
        {
            var data = new List<HazardLocationMapping>();
            if(locid!=null)
                data = GenService.GetAll<HazardLocationMapping>().Where(c => c.Status == EntityStatus.Active && c.LocationId==locid).ToList();
            else
                data = GenService.GetAll<HazardLocationMapping>().Where(c => c.Status == EntityStatus.Active).ToList();
            var newdata = (from item in data
                           select new HazardLocationMappingDto
                           {
                               Id = item.Id,
                               HazardId = item.HazardId,
                               HazardName = item.Hazard.Name,
                               ParentLocationId=(long)item.Location.ParentID,
                               LocationId=(long)item.LocationId,
                               LocationName=item.Location.Parent.LocationName + ", " + item.Location.LocationName,
                               Consequence=item.Consequence,
                               EndMonthId = item.EndMonthId,
                               EndMonthName = item.EndMonth.EnglishName,
                               StartMonthId = item.StartMonthId,
                               StartMonthName = item.StartMonth.EnglishName,
                               January = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.January) ? "calendar" : (item.EndMonthId >= (int)Months.January ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.January && (int)Months.January <= item.EndMonthId) ? "calendar" : "")),
                               February = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.February) ? "calendar" : (item.EndMonthId >= (int)Months.February ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.February && (int)Months.February <= item.EndMonthId) ? "calendar" : "")),
                               March = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.March) ? "calendar" : (item.EndMonthId >= (int)Months.March ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.March && (int)Months.March <= item.EndMonthId) ? "calendar" : "")),
                               April = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.April) ? "calendar" : (item.EndMonthId >= (int)Months.April ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.April && (int)Months.April <= item.EndMonthId) ? "calendar" : "")),
                               May = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.May) ? "calendar" : (item.EndMonthId >= (int)Months.May ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.May && (int)Months.May <= item.EndMonthId) ? "calendar" : "")),
                               June = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.June) ? "calendar" : (item.EndMonthId >= (int)Months.June ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.June && (int)Months.June <= item.EndMonthId) ? "calendar" : "")),
                               July = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.July) ? "calendar" : (item.EndMonthId >= (int)Months.July ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.July && (int)Months.July <= item.EndMonthId) ? "calendar" : "")),
                               August = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.August) ? "calendar" : (item.EndMonthId >= (int)Months.August ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.August && (int)Months.August <= item.EndMonthId) ? "calendar" : "")),
                               September = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.September) ? "calendar" : (item.EndMonthId >= (int)Months.September ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.September && (int)Months.September <= item.EndMonthId) ? "calendar" : "")),
                               October = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.October) ? "calendar" : (item.EndMonthId >= (int)Months.October ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.October && (int)Months.October <= item.EndMonthId) ? "calendar" : "")),
                               November = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.November) ? "calendar" : (item.EndMonthId >= (int)Months.November ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.November && (int)Months.November <= item.EndMonthId) ? "calendar" : "")),
                               December = (item.StartMonthId > item.EndMonthId ? ((item.StartMonthId <= (int)Months.December) ? "calendar" : (item.EndMonthId >= (int)Months.December ? "calendar" : "")) : ((item.StartMonthId <= (int)Months.December && (int)Months.December <= item.EndMonthId) ? "calendar" : ""))
                           }).ToList();
            return newdata;
        }

        public HazardLocationMappingDto GetCalendarByHazardId(long hazardid)
        {
            var data = GenService.GetById<HazardLocationMapping>(hazardid);
            return Mapper.Map<HazardLocationMappingDto>(data);
        }

        public List<HazardVennDto> GetHazardVennByLocation(long locid)
        {
            var locationhazardmapping = GenService.GetAll<HazardLocationMapping>().Where(c => c.Status == EntityStatus.Active && c.Location.ParentID == locid).ToList();
            
            var hazardinfo = GenService.GetAll<Hazard>().Where(c => c.Status == EntityStatus.Active).ToList();

            var data = (from map in locationhazardmapping
                       join haz in hazardinfo
                       on map.HazardId equals haz.Id
                       select new HazardVennDto
                       {
                           HazardId=map.HazardId,
                           HazardName=map.Hazard.Name,
                           LocationId=map.LocationId,
                           LocationName=map.Location.LocationName,
                           Consequence=map.Consequence,
                           StartMonthId= map.StartMonthId,
                           EndMonthId= map.EndMonthId,
                           xAxis=(decimal)(map.StartMonthId+ map.EndMonthId)/2,
                           radius=  (map.EndMonthId - map.StartMonthId == 0 ?  20: ((decimal)(map.EndMonthId - ((decimal)(map.StartMonthId + map.EndMonthId) / 2)) * 10 * 8)),
                           FillColor=haz.FillColor,
                           LineColor=haz.LineColor
                       }).ToList();

            return data;         
        }
        #endregion

        #region Agricultural Land Info
        public ResponseDto SaveAgriculrulLandInfo(AgriculturalLandInfoDto dto, long? userId)
        {
            var entity = new AgriculturalLandInfo();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<AgriculturalLandInfo>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Land Information is Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<AgriculturalLandInfo>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Land Information is Saved Successfully";
                }
            }
            catch (Exception e)
            {
                responce.Message = e.ToString();
                responce.Message = "Failed!";
            }
            GenService.SaveChanges();
            return responce;
        }

        public IPagedList<AgriculturalLandInfoDto> AgriculturalLandList(int pageSize, int pageCount, long? location)
        {

            var lands = GenService.GetAll<AgriculturalLandInfo>().Where(m => m.Status == EntityStatus.Active);


            if (location != null)
                lands = lands.Where(m => m.LocationId == location);

            var landList = from el in lands
                           select new AgriculturalLandInfoDto
                           {
                               Id = el.Id,
                               CropId = el.CropId,
                               CropName = el.Crop.CropName,
                               CropSeasonId = el.CropSeasonId,
                               CropSeasonName = el.CropSeason.SeasonName,
                               LocationId = el.LocationId,
                               LocationName = el.Location.Parent.LocationName + ", " + el.Location.LocationName,
                               HazardId = (el.HazardId != null ? el.HazardId : 0),
                               HazardName = (el.HazardId != null ? el.Hazard.Name : ""),
                               Production = el.Production,
                               DamagePercentage = el.DamagePercentage,
                               AffectedBefore = el.AffectedBefore,
                               AffectedYear = el.AffectedYear
                           };

            return landList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        public AgriculturalLandInfoDto getAgriculturalLandInfoById(long id)
        {
            var data = GenService.GetById<AgriculturalLandInfo>(id);
            return Mapper.Map<AgriculturalLandInfoDto>(data);
        } 
        #endregion
    }
}
