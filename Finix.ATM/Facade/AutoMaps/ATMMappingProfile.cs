﻿using AutoMapper;
using Finix.ATM.DTO;
using Finix.ATM.Infrastructure.Models;
using Finix.ATM.Util;

namespace Finix.ATM.Facade.AutoMaps
{
    public class ATMMappingProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();

            CreateMap<Location, LocationDto>()
             .ForMember(d => d.LevelName, o => o.MapFrom(s  => s.Level != null ? UiUtil.GetDisplayName(s.Level) : ""))
             .ForMember(d => d.ParentName, o => o.MapFrom(s => s.ParentID !=null ? s.Parent.LocationName : "This is Primary"));
            CreateMap<LocationDto, Location>();

            CreateMap<Hazard, HazardDto>();
            CreateMap<HazardDto, Hazard>();

            CreateMap<Element, ElementDto>()
              .ForMember(d => d.ElementTypeName, o => o.MapFrom(   s => s.ElementTypeId != null ? s.ElementType.TypeName : ""))
              .ForMember(d => d.ParentLocationId, o => o.MapFrom(  s => s.LocationId != null ? s.Location.Parent.Id :0))
              .ForMember(d => d.ParentLocationName, o => o.MapFrom(s => s.LocationId != null ? s.Location.Parent.LocationName : ""))
              .ForMember(d => d.LocationName, o => o.MapFrom(      s => s.LocationId != null ? s.Location.LocationName : ""))
              .ForMember(d => d.HazardName, o => o.MapFrom(        s => s.HazardId != null ? s.Hazard.Name : ""));
            CreateMap<ElementDto, Element>();

            CreateMap<ElementType, ElementTypeDto>();
            CreateMap<ElementTypeDto, ElementType>();

            CreateMap<EventType, EventTypeDto>();
            CreateMap<EventTypeDto, EventType>();

            CreateMap<LandType, LandTypeDto>();
            CreateMap<LandTypeDto, LandType>();

            CreateMap<LandUse, LandUseDto>()
                .ForMember(d => d.LandTypeName, o => o.MapFrom(s => s.LandTypeID != null ? s.LandType.TypeName : ""))
                .ForMember(d => d.ParentLocationId, o => o.MapFrom(s => s.LocationId != null ? s.Location.ParentID : 0))
                .ForMember(d => d.ParentLocationName, o => o.MapFrom(s => s.LocationId != null ? s.Location.Parent.LocationName : ""))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.LocationId != null ? (s.Location.Parent.LocationName + ", " + s.Location.LocationName ) : ""));
            CreateMap<LandUseDto, LandUse>();

            CreateMap<Crops, CropsDto>();
            CreateMap<CropsDto, Crops>();

            CreateMap<WaterSource, WaterSourceDto>();
            CreateMap<WaterSourceDto, WaterSource>();

            CreateMap<CultivationSeason, CropSeasonDto>()
                .ForMember(d => d.StartMonthName, o => o.MapFrom(s => s.StartMonthId != null ? s.StartMonth.EnglishName : ""))
                .ForMember(d => d.EndMonthName, o => o.MapFrom(s => s.EndMonthId != null ? s.EndMonth.EnglishName : ""));
            CreateMap<CropSeasonDto, CultivationSeason>();

            CreateMap<EngBangMonthMapping, EngBangMonthMappingDto>();
            CreateMap<EngBangMonthMappingDto, EngBangMonthMapping>();

            CreateMap<CropCalender, CropCalendarDto>()
                .ForMember(d => d.StartMonthName, o => o.MapFrom(s => s.StartMonthId != null ? s.StartMonth.EnglishName : ""))
                .ForMember(d => d.EndMonthName,   o => o.MapFrom(s =>   s.EndMonthId != null ? s.EndMonth.EnglishName : ""));
            CreateMap<CropCalendarDto, CropCalender>();

            CreateMap<AgriculturalLandInfo, AgriculturalLandInfoDto>()
               .ForMember(d => d.CropName, o => o.MapFrom(s => s.CropId != null ? s.Crop.CropName : ""))
               .ForMember(d => d.ParentLocationId, o => o.MapFrom(s => s.LocationId != null ? s.Location.ParentID : 0))
               .ForMember(d => d.ParentLocationName, o => o.MapFrom(s => s.LocationId != null ? s.Location.Parent.LocationName : ""))
               .ForMember(d => d.LocationName, o => o.MapFrom(s => s.LocationId != null ? (s.Location.Parent.LocationName + ", " + s.Location.LocationName) : ""))
               .ForMember(d => d.CropSeasonName, o => o.MapFrom(s => s.CropSeasonId != null ? s.CropSeason.SeasonName : ""))
               .ForMember(d => d.HazardName, o => o.MapFrom(s => s.HazardId != null ? s.Hazard.Name : ""));
            CreateMap<AgriculturalLandInfoDto, AgriculturalLandInfo>();

            CreateMap<PopulationGroup, PopulationGroupDto>();
            CreateMap<PopulationGroupDto, PopulationGroup>();

            CreateMap<PopulationVulnerability, PopulationVulnerabilityDto>()
                .ForMember(d => d.Gender, o => o.MapFrom(s => s.Gender != null ? UiUtil.GetDisplayName(s.Gender) : ""))
                .ForMember(d => d.ParentLocationId, o => o.MapFrom(s => s.LocationId != null ? s.Location.ParentID : 0))
                .ForMember(d => d.ParentLocationName, o => o.MapFrom(s => s.LocationId != null ? s.Location.Parent.LocationName : ""))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.LocationId != null ? (s.Location.Parent.LocationName + ", " + s.Location.LocationName) : ""))
                .ForMember(d => d.GroupName, o => o.MapFrom(s => s.GroupId != null ? s.Group.GroupName : ""));
            CreateMap<PopulationVulnerabilityDto, PopulationVulnerability>();

            

            CreateMap<Employments, EmploymentsDto>();
            CreateMap<EmploymentsDto, Employments>();

            CreateMap<FoodProductionItems, FoodProductionItemsDto>()
                .ForMember(d => d.FoodProductionType, o => o.MapFrom(s => s.Type != null ? UiUtil.GetDisplayName(s.Type) : "" ));
            CreateMap<FoodProductionItemsDto, FoodProductionItems>();

            CreateMap<LandUseVulnerability, LandUseVulnerabilityDto>()
                .ForMember(d => d.LandTypeName, o => o.MapFrom(s => s.LandTypeId != null ? s.LandType.TypeName : ""));
            CreateMap<LandUseVulnerabilityDto, LandUseVulnerability>();

            CreateMap<LaborAndEmployment, LaborAndEmploymentDto>()
                .ForMember(d => d.EmploymentName, o => o.MapFrom(s => s.EmploymentId != null ? s.Employment.Name : ""));
            CreateMap<LaborAndEmploymentDto, LaborAndEmployment>();

            CreateMap<FoodProduction, FoodProductionDto>()
                .ForMember(d => d.FoodProductionTypeName, o => o.MapFrom(s => s.FoodProductionType != null ? UiUtil.GetDisplayName(s.FoodProductionType) : ""));
            CreateMap<FoodProductionDto, FoodProduction>();

            CreateMap<Poverty, PovertyDto>()
                .ForMember(d => d.PovertySituationName, o => o.MapFrom(s => s.PovertySituation != null ? UiUtil.GetDisplayName(s.PovertySituation) : ""));
            CreateMap<PovertyDto, Poverty>();

            CreateMap<LocalEconomyVulnerability, LocalEconomyVulnerabilityDto>()
                .ForMember(d => d.ParentLocationId, o => o.MapFrom  (s => s.LocationId != null ? s.Location.ParentID : 0))
                .ForMember(d => d.ParentLocationName, o => o.MapFrom(s => s.LocationId != null ? s.Location.Parent.LocationName : ""))
                .ForMember(d => d.LocationName, o => o.MapFrom      (s => s.LocationId != null ? (s.Location.Parent.LocationName + ", " + s.Location.LocationName) : ""));
            CreateMap<LocalEconomyVulnerabilityDto, LocalEconomyVulnerability>()
                  .ForMember(d => d.LaborAndEmployments, o => o.Ignore())
                  .ForMember(d => d.LandUseVulnerabilities, o => o.Ignore())
                  .ForMember(d => d.FoodProductions, o => o.Ignore())
                  .ForMember(d => d.Poverties, o => o.Ignore());

            CreateMap<ElementAtRisk, ElementAtRiskDto>()
                .ForMember(d => d.UPName, o => o.MapFrom(s => s.UPId != null ? (s.UP.Parent.LocationName + ", " + s.UP.LocationName) : ""))
                .ForMember(d => d.District, o => o.MapFrom(s => s.UPId != null ? s.UP.Parent.Parent.LocationName  : ""))
                .ForMember(d => d.HazardName, o => o.MapFrom(s => s.HazardId != null ? s.Hazard.Name : ""))
                .ForMember(d => d.ElementName, o => o.MapFrom(s => s.ElementId != null ? s.Element.Name : ""))
                .ForMember(d => d.ElementType, o => o.MapFrom(s => s.ElementId != null ? s.Element.ElementType.TypeName : ""));
            CreateMap<ElementAtRiskDto, ElementAtRisk>();

            CreateMap<RiskStatement, RiskStatementDto>()
               .ForMember(d => d.ParentLocationId, o => o.MapFrom(s => s.SpecificAreaId != null ? s.SpecificArea.ParentID : 0))
               .ForMember(d => d.ParentLocationName, o => o.MapFrom(s => s.SpecificAreaId != null ? s.SpecificArea.Parent.LocationName : ""))
               .ForMember(d => d.SpecificAreaName, o => o.MapFrom(s => s.SpecificAreaId != null ? (s.SpecificArea.Parent.LocationName + ", " + s.SpecificArea.LocationName) : ""))
               .ForMember(d => d.HazardName, o => o.MapFrom(s => s.HazardId != null ? s.Hazard.Name : ""))
               .ForMember(d => d.ElementName, o => o.MapFrom(s => s.ElementId != null ? s.Element.Name : ""));
            CreateMap<RiskStatementDto, RiskStatement>();

            CreateMap<RiskPriority, RiskPriorityDto>()
               .ForMember(d => d.UPName, o => o.MapFrom(s => s.UPId != null ? (s.UP.Parent.LocationName + ", " + s.UP.LocationName) : ""))
               .ForMember(d => d.District, o => o.MapFrom(s => s.UPId != null ? s.UP.Parent.Parent.LocationName  : ""))
               .ForMember(d => d.Element, o => o.MapFrom(s => s.RiskStatement != null ? s.RiskStatement.Element.Name : ""))
               .ForMember(d => d.HazardName, o => o.MapFrom(s => s.RiskStatement != null ? s.RiskStatement.Element.Hazard.Name : ""))
               .ForMember(d => d.RiskStatementName, o => o.MapFrom(s => s.RiskStatementId != null ? s.RiskStatement.Statement : ""));
            CreateMap<RiskPriorityDto, RiskPriority>();

            CreateMap<HazardLocationMapping, HazardLocationMappingDto>();
            CreateMap<HazardLocationMappingDto, HazardLocationMapping>();

            CreateMap<SensitivityAnalysisDetails, SensitivityAnalysisDetailsDto>();
            CreateMap<SensitivityAnalysisDetailsDto, SensitivityAnalysisDetails>();

            CreateMap<SensitivityAnalysis, SensitivityAnalysisDto>()
               .ForMember(d => d.ParentLocationId, o => o.MapFrom(s => s.LocationId != null ? s.Location.ParentID : 0))
               .ForMember(d => d.ParentLocationName, o => o.MapFrom(s => s.LocationId != null ? s.Location.Parent.LocationName : ""))
               .ForMember(d => d.LocationName, o => o.MapFrom(s => s.LocationId != null ? (s.Location.Parent.LocationName + ", " + s.Location.LocationName) : ""))
               .ForMember(d => d.RiskStatementTitle, o => o.MapFrom(s => s.RiskStatementId != null ? s.RiskStatement.Statement : ""));
            CreateMap<SensitivityAnalysisDto, SensitivityAnalysis>();

            CreateMap<IndicatorType, IndicatorTypeDto>()
                .ForMember(d => d.GroupName, o => o.MapFrom(s => s.TypeGroup >0 ? UiUtil.GetDisplayName(s.TypeGroup) : ""));
            CreateMap<IndicatorTypeDto, IndicatorType>();

            CreateMap<Indicators, IndicatorsDto>()
                .ForMember(d => d.IndicatorTypeName, o => o.MapFrom(s => s.Type != null ? s.IndicatorType.TypeName : ""));
            CreateMap<IndicatorsDto, Indicators>();

            CreateMap<SchemeCategory, SchemeCategoryDto>();
            CreateMap<SchemeCategoryDto, SchemeCategory>();

            CreateMap<SchemeNature, SchemeNatureDto>();
            CreateMap<SchemeNatureDto, SchemeNature>();

            CreateMap<EnergySource, EnergySourceDto>();
            CreateMap<EnergySourceDto, EnergySource>();

            CreateMap<Material, MaterialDto>();
            CreateMap<MaterialDto, Material>();

            CreateMap<BuildingPart, BuildingPartDto>();
            CreateMap<BuildingPartDto, BuildingPart>();

            CreateMap<HouseHoldInformation, HouseHoldInformationDto>()
                .ForMember(d => d.UpId, o => o.MapFrom(s => s.Village != null ? s.Village.Parent.ParentID : 0))
                .ForMember(d => d.UPName, o => o.MapFrom(s => s.Village != null ? s.Village.Parent.Parent.LocationName : ""))
                .ForMember(d => d.ParentLocationId, o => o.MapFrom(s => s.Village != null ? s.Village.ParentID:0))
                .ForMember(d => d.WardName, o => o.MapFrom(s => s.Village != null ? s.Village.Parent.LocationName : ""))
                .ForMember(d => d.HasBankAccount, o => o.MapFrom(s => s.AccountNumber == "" ? false : true))
                .ForMember(d => d.HasMobileNumber, o => o.MapFrom(s => s.HHeadMobileNo == "" ? false : true))
                .ForMember(d => d.HHeadReligionName, o => o.MapFrom(s => s.HHeadReligion >0 ? UiUtil.GetDisplayName(s.HHeadReligion) : ""))
                .ForMember(d => d.EthnicMinorityName, o => o.MapFrom(s => s.EthnicMinorityId >0 ? s.EthnicMinority.Name : ""))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.VillageId != null ? (s.Village.Parent.LocationName + ", " + s.Village.LocationName) : ""));
            CreateMap<HouseHoldInformationDto, HouseHoldInformation>();

            CreateMap<HouseHoldSurveyAdministration, HouseHoldSurveyAdministrationDto>()
                .ForMember(d=> d.HouseHoldNo, o=> o.MapFrom(s=>s.HouseHold!=null?s.HouseHold.HouseHoldNo:""));
            CreateMap<HouseHoldSurveyAdministrationDto, HouseHoldSurveyAdministration>();

            CreateMap<HouseHoldMembers, HouseHoldMembersDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.GenderName, o => o.MapFrom(s => s.Gender >0 ? UiUtil.GetDisplayName(s.Gender) : ""))
                .ForMember(d => d.MaritalStatusTitle, o => o.MapFrom(s => s.MaritalStatus > 0 ? UiUtil.GetDisplayName(s.MaritalStatus) : ""))
                .ForMember(d => d.RelationtoHHTitle, o => o.MapFrom(s => s.RelationtoHH > 0 ? UiUtil.GetDisplayName(s.RelationtoHH) : ""))
                .ForMember(d => d.OccupationName, o => o.MapFrom(s => s.OccupationId != null ? s.Occupation.Name : ""));
            CreateMap<HouseHoldMembersDto, HouseHoldMembers>();

            CreateMap<HouseHoldBuilding, HouseHoldBuildingDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.RentalTypeName, o => o.MapFrom(s => s.RentalType >0 ? UiUtil.GetDisplayName(s.RentalType) : ""))
                .ForMember(d => d.HouseTypeName, o => o.MapFrom(s => s.HouseType > 0 ? UiUtil.GetDisplayName(s.HouseType) : ""))
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.IsPronetoHazard, o => o.MapFrom(s => s.PronetoHazards.Count> 0? true : false));
            CreateMap<HouseHoldBuildingDto, HouseHoldBuilding>()
                .ForMember(d => d.PronetoHazards, o=>o.Ignore())
                .ForMember(d => d.OtherBuildings, o => o.Ignore());

            CreateMap<HouseHoldOtherBuildings, HouseHoldOtherBuildingsDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHoldBuilding != null ? s.HouseHoldBuilding.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.BuildingName, o => o.MapFrom(s => s.OtherBuilding != null ? s.OtherBuilding.BuildingName : ""));
            CreateMap<HouseHoldOtherBuildingsDto, HouseHoldOtherBuildings>();

            CreateMap<HouseHoldHazards, HouseHoldHazardsDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHoldBuilding != null ? s.HouseHoldBuilding.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.HazardName, o => o.MapFrom(s => s.Hazard != null ? s.Hazard.Name : ""));
            CreateMap<HouseHoldHazardsDto, HouseHoldHazards>();

            CreateMap<HouseHoldFood, HouseHoldFoodDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""));
            CreateMap<HouseHoldFoodDto, HouseHoldFood>()
                .ForMember(d => d.MainFoods, o => o.Ignore())
                .ForMember(d => d.FoodShortages, o => o.Ignore());

            CreateMap<HouseHoldMainFoods, HouseHoldMainFoodsDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHoldFood != null ? s.HouseHoldFood.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.IsPurchasedFood, o => o.MapFrom(s =>  !s.IsGrownFood));
            CreateMap<HouseHoldMainFoodsDto, HouseHoldMainFoods>();

            CreateMap<HouseHoldFoodShortages, HouseHoldFoodShortagesDto>()
               .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHoldFood != null ? s.HouseHoldFood.HouseHold.HouseHoldNo : ""));
            CreateMap<HouseHoldFoodShortagesDto, HouseHoldFoodShortages>();

            CreateMap<HouseHoldHealth, HouseHoldHealthDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.LatrineTypeName, o => o.MapFrom(s => s.LatrineType > 0 ? UiUtil.GetDisplayName(s.LatrineType) : ""));
            CreateMap<HouseHoldHealthDto, HouseHoldHealth>()
                .ForMember(d => d.MainWaterSources, o => o.Ignore());

            CreateMap<HouseHoldWaterSource, HouseHoldWaterSourceDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHoldHealth != null ? s.HouseHoldHealth.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.WaterSourceName, o => o.MapFrom(s => s.WaterSource != null ? s.WaterSource.SourceName : ""));
            CreateMap<HouseHoldWaterSourceDto, HouseHoldWaterSource>();

            CreateMap<HouseHoldEnergy, HouseHoldEnergyDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""));
            CreateMap<HouseHoldEnergyDto, HouseHoldEnergy>()
                .ForMember(d => d.HouseHoldEnergySources, o => o.Ignore());

            CreateMap<HouseHoldEnergySources, HouseHoldEnergySourcesDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHoldEnergy != null ? s.HouseHoldEnergy.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.EnergySourceName, o => o.MapFrom(s => s.EnergySource != null ? s.EnergySource.SourceName : ""));
            CreateMap<HouseHoldEnergySourcesDto, HouseHoldEnergySources>();

            CreateMap<HouseHoldCrops, HouseHoldCropsDto>()
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHoldEnergy != null ? s.HouseHoldEnergy.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.CropsName, o => o.MapFrom(s => s.Crop != null ? s.Crop.CropName : ""));
            CreateMap<HouseHoldCropsDto, HouseHoldCrops>();

            CreateMap<SupportType, SupportTypeDto>();
            CreateMap<SupportTypeDto, SupportType>();

            CreateMap<SourceOfFund, SourceOfFundDto>();
            CreateMap<SourceOfFundDto, SourceOfFund>();

            CreateMap<Interventions, InterventionsDto>()
                .ForMember(d => d.HHId, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.Id : 0))
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.HHDetails, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.Details : ""))
                .ForMember(d => d.UpName, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.Village.Parent.Parent.LocationName : ""))
                .ForMember(d => d.WardName, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.Village.Parent.LocationName : ""))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.Village.LocationName : ""))
                .ForMember(d => d.SchemeCategoryName, o => o.MapFrom(s => s.InterventionCategory != null ? s.InterventionCategory.CategoryName : ""));
            CreateMap<InterventionsDto, Interventions>()
                .ForMember(d => d.InterventionBudgets, o => o.Ignore())
                .ForMember(d => d.InterventionExpectedbenfits, o => o.Ignore())
                .ForMember(d => d.SupportTypes, o => o.Ignore())
                .ForMember(d => d.InterventionHazards, o => o.Ignore());

            CreateMap<InterventionSupport, InterventionSupportDto>()
                 .ForMember(d => d.InterventionTitle, o => o.MapFrom(s => s.Itervention != null ? s.Itervention.InterventionTitle : ""))
                 .ForMember(d => d.SupportTypeName, o => o.MapFrom(s => s.SupportType != null ? s.SupportType.SupportName : ""));
            CreateMap<InterventionSupportDto, InterventionSupport>();

            CreateMap<InterventionHazard, InterventionHazardDto>()
                .ForMember(d => d.InterventionTitle, o => o.MapFrom(s => s.Itervention != null ? s.Itervention.InterventionTitle : ""))
                .ForMember(d => d.HazardName, o => o.MapFrom(s => s.Hazards != null ? s.Hazards.Hazard.Name : ""));
            CreateMap<InterventionHazardDto, InterventionHazard>();

            CreateMap<InterventionBudget, InterventionBudgetDto>()
                .ForMember(d => d.InterventionTitle, o => o.MapFrom(s => s.Intervention != null ? s.Intervention.InterventionTitle : ""))
                .ForMember(d => d.SourceOfFundName, o => o.MapFrom(s => s.SourceOfFund != null ? s.SourceOfFund.SourceName : ""));
            CreateMap<InterventionBudgetDto, InterventionBudget>();

            CreateMap<InterventionExpectedbenfits, InterventionExpectedbenfitsDto>()
                .ForMember(d => d.InterventionTitle, o => o.MapFrom(s => s.Itervention != null ? s.Itervention.InterventionTitle : ""))
                .ForMember(d => d.ElementTypeName, o => o.MapFrom(s => s.ElementType != null ? s.ElementType.TypeName : ""));
            CreateMap<InterventionExpectedbenfitsDto, InterventionExpectedbenfits>();

            CreateMap<InterventionImplementation, InterventionImplementationDto>()
              .ForMember(d => d.InterventionTitle, o => o.MapFrom(s => s.Intervention != null ? s.Intervention.InterventionTitle : ""));
            CreateMap<InterventionImplementationDto, InterventionImplementation>()
                .ForMember(d => d.Expenses, o => o.Ignore());


            CreateMap<InterventionExpenditures, InterventionExpendituresDto>()
                .ForMember(d => d.SourceOfFundName, o => o.MapFrom(s => s.SourceOfFund != null ? s.SourceOfFund.SourceName : ""));
            CreateMap<InterventionExpendituresDto, InterventionExpenditures>();

            CreateMap<AdaptionCapacityDevelopment, AdaptionCapacityDevelopmentDto>()
               .ForMember(d => d.InterventionIdTitle, o => o.MapFrom(s => s.Intervention != null ? s.Intervention.InterventionTitle : ""));
            CreateMap<AdaptionCapacityDevelopmentDto, AdaptionCapacityDevelopment>()
                .ForMember(d => d.Trainings, o => o.Ignore());

            CreateMap<DisbursementHistory, DisbursementHistoryDto>()
                .ForMember(d => d.InterventionTitle, o => o.MapFrom(s => s.Intervention != null ? s.Intervention.InterventionTitle : ""));
            CreateMap<DisbursementHistoryDto, DisbursementHistory>();

            CreateMap<RiskReductionOption, RiskReductionOptionDto>()
                .ForMember(d => d.LocationId, o => o.MapFrom(s => s.RiskStatement.SpecificArea != null ? s.RiskStatement.SpecificArea.ParentID : 0))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.RiskStatement.SpecificArea != null ? s.RiskStatement.SpecificArea.Parent.LocationName : ""))
                .ForMember(d => d.RiskStatementName, o => o.MapFrom(s => s.RiskStatement != null ? s.RiskStatement.Statement : ""));
            CreateMap<RiskReductionOptionDto, RiskReductionOption>();

            CreateMap<OptionActionPlan, OptionActionPlanDto>()
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.UPId != null ? s.Location.LocationName : ""))
                .ForMember(d => d.Statement, o => o.MapFrom(s => s.RiskStatement != null ? s.RiskStatement.Statement : ""))
                .ForMember(d => d.OptionName, o => o.MapFrom(s => s.Option != null ? s.Option.OptionName : ""));
            CreateMap<OptionActionPlanDto, OptionActionPlan>();

            CreateMap<HouseHoldElementsAnalysis, HouseHoldElementsAnalysisDto>()
                .ForMember(d => d.ElementTypeName, o => o.MapFrom(s => s.ElementType != null ? s.ElementType.TypeName : ""))
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""))
                .ForMember(d => d.HazardName, o => o.MapFrom(s => s.Hazard != null ? s.Hazard.Name : ""));
            CreateMap<HouseHoldElementsAnalysisDto, HouseHoldElementsAnalysis>();

            CreateMap<HouseHoldPotentialImpact, HouseHoldPotentialImpactDto>()
                .ForMember(d => d.ElementTypeName, o => o.MapFrom(s => s.ElementType != null ? s.ElementType.TypeName : ""))
                .ForMember(d => d.UPId, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.Village.Parent.ParentID :0 ))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.Village.LocationName : ""))
                .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""));
            CreateMap<HouseHoldPotentialImpactDto, HouseHoldPotentialImpact>()
                .ForMember(d => d.HHIndicatorScores, o => o.Ignore());

            CreateMap<IndicatorLog, IndicatorLogDto>();
            CreateMap<IndicatorLogDto, IndicatorLog>();

            CreateMap<HHIndicatorScore, HHIndicatorScoreDto>()
                .ForMember(d => d.IndicatorTitle, o => o.MapFrom(s => s.Indicator != null ? s.Indicator.IndicatorName : ""))
                .ForMember(d => d.PotentialImpact, o => o.MapFrom(s => s.HHRisk != null ? s.HHRisk.PotentialImpact : ""))
                .ForMember(d => d.IsSelected, o => o.MapFrom(s => s.Score ==(decimal)0.0 ? false : true))
                .ForMember(d => d.Score, o => o.MapFrom(s => s.Indicator !=null ? s.Indicator.Weight : 0));
            CreateMap<HHIndicatorScoreDto, HHIndicatorScore>();

            CreateMap<EthnicMinority, EthnicMinorityDto>();
            CreateMap<EthnicMinorityDto, EthnicMinority>();

            CreateMap<OtherBuilding, OtherBuildingDto>();
            CreateMap<OtherBuildingDto, OtherBuilding>();

            CreateMap<AgriculturalTools, AgriculturalToolsDto>();
            CreateMap<AgriculturalToolsDto, AgriculturalTools>();

            CreateMap<HouseHoldAgriTools, HouseHoldAgriToolsDto>();
            CreateMap<HouseHoldAgriToolsDto, HouseHoldAgriTools>();

            CreateMap<HouseHoldLandAsset, HouseHoldLandAssetDto>()
               .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""));
            CreateMap<HouseHoldLandAssetDto, HouseHoldLandAsset>()
                 .ForMember(d => d.AgriculturalTools, o => o.Ignore());

            CreateMap<HouseHoldDisasters, HouseHoldDisastersDto>()
               .ForMember(d => d.HouseHoldNo, o => o.MapFrom(s => s.HouseHold != null ? s.HouseHold.HouseHoldNo : ""));
            CreateMap<HouseHoldDisastersDto, HouseHoldDisasters>();

            CreateMap<SchemeInformation, SchemeInformationDto>()
               .ForMember(d => d.UpId, o => o.MapFrom(s => s.Location != null ? s.Location.ParentID : 0))
               .ForMember(d => d.LocationName, o => o.MapFrom(s => s.Location !=null ? s.Location.LocationName : ""))
               .ForMember(d => d.SchemeNatureName, o => o.MapFrom(s => s.SchemeNature !=null ? s.SchemeNature.NatureName : ""))
               .ForMember(d => d.SchemeCategoryName, o => o.MapFrom(s => s.SchemeCategory != null ? s.SchemeCategory.CategoryName : ""))
               .ForMember(d => d.RiskStatementName, o => o.MapFrom(s => s.RiskStatement != null ? s.RiskStatement.Statement : ""));
            CreateMap<SchemeInformationDto, SchemeInformation>()
                .ForMember(d => d.Funds, o => o.Ignore())
                .ForMember(d => d.ExpectedBenefits, o => o.Ignore())
                .ForMember(d => d.Expenses, o => o.Ignore())
                .ForMember(d => d.Beneficiaries, o => o.Ignore())
                .ForMember(d => d.Employements, o => o.Ignore());

            CreateMap<SchemeFunds, SchemeFundsDto>()
              .ForMember(d => d.SourceOfFundName, o => o.MapFrom(s => s.SourceOfFund != null ? s.SourceOfFund.SourceName : ""))
              .ForMember(d => d.SchemeTitle, o => o.MapFrom(s => s.Scheme != null ? s.Scheme.SchemeTitle : ""));
            CreateMap<SchemeFundsDto, SchemeFunds>();

            CreateMap<SchemeHazard, SchemeHazardDto>()
              .ForMember(d => d.HazardName, o => o.MapFrom(s => s.Hazards != null ? s.Hazards.Name : ""))
              .ForMember(d => d.SchemeTitle, o => o.MapFrom(s => s.Scheme != null ? s.Scheme.SchemeTitle : ""));
            CreateMap<SchemeHazardDto, SchemeHazard>();

            CreateMap<SchemeBeneficiaries, SchemeBeneficiariesDto>();
            CreateMap<SchemeBeneficiariesDto, SchemeBeneficiaries>();

            CreateMap<SchemeEmployment, SchemeEmploymentDto>();
            CreateMap<SchemeEmploymentDto, SchemeEmployment>();

            //CreateMap<Expectedbenfits, ExpectedbenfitsDto>()
            //  .ForMember(d => d.ElementName, o => o.MapFrom(s => s.Element != null ? s.Element.Name : ""))
            //  .ForMember(d => d.SchemeTitle, o => o.MapFrom(s => s.Scheme != null ? s.Scheme.SchemeTitle : ""));
            //CreateMap<ExpectedbenfitsDto, Expectedbenfits>();
            
            CreateMap<Expenditures, ExpendituresDto>()
              .ForMember(d => d.SourceOfFundName, o => o.MapFrom(s => s.SourceOfFund != null ? s.SourceOfFund.SourceName : ""));
            CreateMap<ExpendituresDto, Expenditures>();

            CreateMap<CapacityDevelopmentActivity, CapacityDevelopmentActivityDto>()
             .ForMember(d => d.EventTypeName, o => o.MapFrom(s => s.EventType != null ? s.EventType.TypeName : ""))
             .ForMember(d => d.LocationName, o => o.MapFrom(s => s.Location != null ? (s.Location.Parent.LocationName + ", " + s.Location.LocationName) : ""));
            CreateMap<CapacityDevelopmentActivityDto, CapacityDevelopmentActivity>()
                .ForMember(d => d.Participants, o => o.Ignore());

            CreateMap<Trainings, TrainingsDto>();
            CreateMap<TrainingsDto, Trainings>();

            CreateMap<HouseHoldRRAP, HouseHoldRRAPDto>();
            CreateMap<HouseHoldRRAPDto, HouseHoldRRAP>();

            CreateMap<Participants, ParticipantsDto>();
            CreateMap<ParticipantsDto, Participants>();
        }
    }
}
