﻿using AutoMapper;
using Finix.ATM.DTO;
using Finix.ATM.Infrastructure;
using Finix.ATM.Infrastructure.Models;
using Finix.ATM.Service;
using Finix.Auth.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Facade
{

    public class SchemeFacade : BaseFacade
    {
        #region SchemeCategory
        public ResponseDto SaveSchemeCategory(SchemeCategoryDto dto, long? userId)
        {
            var entity = new SchemeCategory();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<SchemeCategory>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Scheme Category Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<SchemeCategory>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Scheme Category Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Scheme Category Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<SchemeCategoryDto> GetSchemeCategories()
        {
            var data = GenService.GetAll<SchemeCategory>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<SchemeCategoryDto>>(data);
        }
        #endregion

        #region Scheme Natures
        public ResponseDto SaveSchemeNature(SchemeNatureDto dto, long? userId)
        {
            var entity = new SchemeNature();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<SchemeNature>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Scheme Nature Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<SchemeNature>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Scheme Nature Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Scheme Nature Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<SchemeNatureDto> GetSchemeNatures()
        {
            var data = GenService.GetAll<SchemeNature>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<SchemeNatureDto>>(data);
        }
        #endregion

        public ResponseDto SaveSchemePBCRG(SchemeInformationDto dto, long? userId)
        {
            var entity = new SchemeInformation();
            ResponseDto responce = new ResponseDto();

            var schemefunds = new List<SchemeFunds>();
            var fund = new SchemeFunds();

            var expenses = new List<Expenditures>();
            var expense = new Expenditures();

            var hazards = new List<SchemeHazard>();
            var hazard = new SchemeHazard();

            var beneficiaries = new List<SchemeBeneficiaries>();
            var beneficiary = new SchemeBeneficiaries();

            var employments = new List<SchemeEmployment>();
            var employment = new SchemeEmployment();

            if (dto.SchemeEmployments != null && dto.SchemeEmployments.Count > 0)
            {
                foreach (var item in dto.SchemeEmployments)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        employment = new SchemeEmployment();
                        employment = GenService.GetById<SchemeEmployment>((long)item.Id);
                        item.CreatedBy = employment.CreatedBy;
                        item.CreateDate = employment.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = employment.Status;
                        Mapper.Map(item, employment);
                        GenService.Save(employment, false);
                        employments.Add(employment);
                    }
                    else
                    {
                        employment = Mapper.Map<SchemeEmployment>(item);
                        employment.CreateDate = DateTime.Now;
                        employment.CreatedBy = userId;
                        employment.Status = EntityStatus.Active;
                        employments.Add(employment);
                    }
                }
            }

            if (dto.Beneficiaries != null && dto.Beneficiaries.Count > 0)
            {
                foreach (var item in dto.Beneficiaries)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        beneficiary = new SchemeBeneficiaries();
                        beneficiary = GenService.GetById<SchemeBeneficiaries>((long)item.Id);
                        item.CreatedBy = beneficiary.CreatedBy;
                        item.CreateDate = beneficiary.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = beneficiary.Status;
                        Mapper.Map(item, beneficiary);
                        GenService.Save(beneficiary, false);
                        beneficiaries.Add(beneficiary);
                    }
                    else
                    {
                        beneficiary = Mapper.Map<SchemeBeneficiaries>(item);
                        beneficiary.CreateDate = DateTime.Now;
                        beneficiary.CreatedBy = userId;
                        beneficiary.Status = EntityStatus.Active;
                        beneficiaries.Add(beneficiary);
                    }
                }
            }

            if (dto.Funds != null && dto.Funds.Count > 0)
            {
                foreach (var item in dto.Funds)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        fund = new SchemeFunds();
                        fund = GenService.GetById<SchemeFunds>((long)item.Id);
                        item.CreatedBy = fund.CreatedBy;
                        item.CreateDate = fund.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = fund.Status;
                        Mapper.Map(item, fund);
                        GenService.Save(fund, false);
                        schemefunds.Add(fund);
                    }
                    else
                    {
                        fund = Mapper.Map<SchemeFunds>(item);
                        fund.CreateDate = DateTime.Now;
                        fund.CreatedBy = userId;
                        fund.Status = EntityStatus.Active;
                        schemefunds.Add(fund);
                    }
                }
            }

            if (dto.SchemeHazards != null && dto.SchemeHazards.Count > 0)
            {
                foreach (var item in dto.SchemeHazards)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        hazard = new SchemeHazard();
                        hazard = GenService.GetById<SchemeHazard>((long)item.Id);
                        item.CreatedBy = hazard.CreatedBy;
                        item.CreateDate = hazard.CreateDate;
                        item.EditDate = DateTime.Now;
                        item.EditedBy = userId;
                        item.Status = hazard.Status;
                        Mapper.Map(item, hazard);
                        GenService.Save(hazard, false);
                        hazards.Add(hazard);
                    }
                    else
                    {
                        hazard = Mapper.Map<SchemeHazard>(item);
                        hazard.CreateDate = DateTime.Now;
                        hazard.CreatedBy = userId;
                        hazard.Status = EntityStatus.Active;
                        hazards.Add(hazard);
                    }
                }
            }

            if (dto.Expenses != null && dto.Expenses.Count > 0)
            {
                foreach (var item in dto.Expenses)
                {
                    if (item.Id > 0 || item.Id != null)
                    {
                        expense = new Expenditures();
                        expense = GenService.GetById<Expenditures>((long)item.Id);
                        item.CreatedBy = expense.CreatedBy;
                        item.CreateDate = expense.CreateDate;
                        expense.EditDate = DateTime.Now;
                        expense.EditedBy = userId;
                        item.Status = expense.Status;
                        Mapper.Map(item, expense);
                        GenService.Save(expense, false);
                        expenses.Add(expense);
                    }
                    else
                    {
                        expense = Mapper.Map<Expenditures>(item);
                        expense.CreateDate = DateTime.Now;
                        expense.CreatedBy = userId;
                        expense.Status = EntityStatus.Active;
                        expenses.Add(expense);
                    }
                }
            }

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<SchemeInformation>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.SchemeHazards = hazards;
                    entity.Beneficiaries = beneficiaries;
                    entity.Employements = employments;
                    entity.Expenses = expenses;
                    entity.Funds = schemefunds;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Scheme information been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<SchemeInformation>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.SchemeHazards = hazards;
                    entity.Beneficiaries = beneficiaries;
                    entity.Employements = employments;
                    entity.Expenses = expenses;
                    entity.Funds = schemefunds;
                    entity.CreatedBy = userId;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Scheme information been Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Scheme information Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }


        #region Scheme List
        public IPagedList<SchemeInformationDto> SchemeList(int pageSize, int pageCount, string searchString, long? thana, long? up)
        {

            var schemeList = GenService.GetAll<SchemeInformation>().Where(m => m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                schemeList = schemeList.Where(m => m.SchemeTitle.ToLower().Contains(searchString));
            }

            if (thana != null)
                schemeList = schemeList.Where(m => m.Location.Parent.ParentID == thana);

            if (up != null)
                schemeList = schemeList.Where(m => m.Location.ParentID == up);

            //var SchemeList = from el in schemeList
            //                            select new HouseHoldInformationDto
            //                            {
            //                                Id = el.Id,
            //                                HouseHoldNo = el.HouseHoldNo,
            //                                VillageId = el.VillageId,

            //                                //Address = el.Address,
            //                                //Details = el.Details,
            //                                //LocationId = el.LocationId,
            //                                LocationName = el.Village.Parent.LocationName + ", " + el.Village.LocationName,
            //                                //Latitude = el.Latitude,
            //                                //Longitude = el.Longitude,
            //                            };
           var SchemeList= Mapper.Map<List<SchemeInformationDto>>(schemeList);

            return SchemeList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        #endregion
        public SchemeInformationDto LoadSchemeInfo(long id)
        {
            SchemeInformationDto schemeinfo = new SchemeInformationDto();
            var data = GenService.GetById<SchemeInformation>(id);
            if (data != null)
            {
                schemeinfo = Mapper.Map<SchemeInformationDto>(data);
                var funds = GenService.GetAll<SchemeFunds>().Where(x => x.SchemeId == schemeinfo.Id && x.Status == EntityStatus.Active);
                schemeinfo.Funds = Mapper.Map<List<SchemeFundsDto>>(funds.ToList());
                var expenses = GenService.GetAll<Expenditures>().Where(x => x.SchemeId == schemeinfo.Id && x.Status == EntityStatus.Active);
                schemeinfo.Expenses = Mapper.Map<List<ExpendituresDto>>(expenses.ToList());
                var hazards = GenService.GetAll<SchemeHazard>().Where(x => x.SchemeId == schemeinfo.Id && x.Status == EntityStatus.Active);
                schemeinfo.SchemeHazards = Mapper.Map<List<SchemeHazardDto>>(hazards.ToList());
                var beneficairies = GenService.GetAll<SchemeBeneficiaries>().Where(x => x.SchemeId == schemeinfo.Id && x.Status == EntityStatus.Active);
                schemeinfo.Beneficiaries = Mapper.Map<List<SchemeBeneficiariesDto>>(beneficairies.ToList());
                var employmnts = GenService.GetAll<SchemeEmployment>().Where(x => x.SchemeId == schemeinfo.Id && x.Status == EntityStatus.Active);
                schemeinfo.SchemeEmployments = Mapper.Map<List<SchemeEmploymentDto>>(employmnts.ToList());
                return schemeinfo;
            }
            else
                return null;
        }
    }
}
