﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
  public  class InterventionsDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public string UpName { get; set; }
        public string WardName { get; set; }
        public string LocationName { get; set; }
        public string HHHeadName { get; set; }
        public string HHDetails { get; set; }
        public long? InterventionCategoryId { get; set; }
        public string SchemeCategoryName { get; set; }
        public string InterventionTitle { get; set; }
        public string ShortDetails { get; set; }
        public string ProposalRefNo { get; set; }
        public bool IsCapable { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsSupportNeeded { get; set; }
        public List<InterventionSupportDto> SupportTypes { get; set; }
        public List<InterventionBudgetDto> InterventionBudgets { get; set; }
        public List<InterventionHazardDto> InterventionHazards { get; set; }
        public List<InterventionExpectedbenfitsDto> InterventionExpectedbenfits { get; set; }
        public double CRFGrant { get; set; }
        public double EstimatedBudget { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }

    public class InterventionSupportDto
    {
        public long? Id { get; set; }
        public long? InterventionId { get; set; }
        public string InterventionTitle { get; set; }
        public long? SupportTypeId { get; set; }
        public string SupportTypeName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
    public class InterventionHazardDto 
    {
        public long? Id { get; set; }
        public long? InterventionId { get; set; }
        public string InterventionTitle { get; set; }
        public long? HazardId { get; set; }
        public string HazardName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
    public class InterventionExpectedbenfitsDto 
    {
        public long? Id { get; set; }
        public long? InterventionId { get; set; }
        public string InterventionTitle { get; set; }
        public long? ElementTypeId { get; set; }
        public string ElementTypeName { get; set; }
        public string Expectation { get; set; }
        public string Benefit { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
