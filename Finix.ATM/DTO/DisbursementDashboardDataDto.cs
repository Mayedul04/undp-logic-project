﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class DisbursementDashboardDataDto
    {
        public long? InterventionCategoryId { get; set; }
        public string SchemeCategoryName { get; set; }
        public int InterventionCount { get; set; }
        public double GrantedAmount { get; set; }
        public double DisbursedAmount { get; set; }
    }
}
