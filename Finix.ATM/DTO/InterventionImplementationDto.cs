﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class InterventionImplementationDto
    {
        public long? Id { get; set; }
        public long? InterventionId { get; set; }
        public string InterventionTitle { get; set; }
        public double ActualCost { get; set; }
        public double? CRFGrant { get; set; }
        public double? OwnContribution { get; set; }
        public string ImplementedBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ICollection<InterventionExpendituresDto> Expenses { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class InterventionExpendituresDto 
    {
        public long? Id { get; set; }
        public long? ImplementationId { get; set; }
        public long? SourceId { get; set; }
        public string SourceOfFundName { get; set; }
        public double CostAmount { get; set; }
        public DateTime? CostDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
