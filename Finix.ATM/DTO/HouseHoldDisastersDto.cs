﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldDisastersDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public bool IsLatestAfftected { get; set; }
        public string LatestDisasterYear { get; set; }
        public bool HasOccupationAffected { get; set; }
        public bool AnybodyDied { get; set; }
        public bool IsClimateChanged { get; set; }
        public string Changes { get; set; }
        public string CauseofChange { get; set; }
        public string Damage { get; set; }
        public long? LatestHazardId { get; set; }
        public string HazardName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }

    }
}
