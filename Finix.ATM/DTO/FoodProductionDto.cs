﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class FoodProductionDto
    {
        public long? Id { get; set; }
        public long? LocalEconomyId { get; set; }
        public long? LocationId { get; set; }
        public FoodProductionType FoodProductionType { get; set; }
        public string FoodProductionTypeName { get; set; }
        public Boolean IsSelfSufficient { get; set; }
        public decimal IsExportable { get; set; }
        public decimal IsProductionDeficiency { get; set; }
        public string Vulnerabilities { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
