﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
    public class SchemeInformationDto
    {
        public long? Id { get; set; }
        public long? UpId { get; set; }
        public long? LocationId { get; set; }
        public string LocationName { get; set; }
        public string SchemeTitle { get; set; }
        public string Description { get; set; }
        public string SchemeNo { get; set; }
        public long SchemeNatureId { get; set; }
        public string SchemeNatureName { get; set; }
        public long SchemeCategoryId { get; set; }
        public string SchemeCategoryName { get; set; }
        public long? RiskId { get; set; }
        public string RiskStatementName { get; set; }
        public string ProposalRefNo { get; set; }
        public string ImplementedBy { get; set; }
        public string Coverage { get; set; }
        public double EstimatedBudget { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<SchemeFundsDto> Funds { get; set; }
        public string GainedBenefits { get; set; }
        public string ExpectedBenefits { get; set; }
        public List<SchemeHazardDto> SchemeHazards { get; set; }
        public List<ExpendituresDto> Expenses { get; set; }
        public List<SchemeEmploymentDto> SchemeEmployments { get; set; }
        public List<SchemeBeneficiariesDto> Beneficiaries { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class SchemeFundsDto 
    {
        public long? Id { get; set; }
        public long? SchemeId { get; set; }
        public string SchemeTitle { get; set; }
        public long? SourceId { get; set; }
        public string SourceOfFundName { get; set; }
        public double Amount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    
    public class SchemeHazardDto 
    {
        public long? Id { get; set; }
        public long? SchemeId { get; set; }
        public string SchemeTitle { get; set; }
        public long? HazardId { get; set; }
        public string HazardName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class ExpendituresDto
    {
        public long? Id { get; set; }
        public long? SchemeId { get; set; }
        public long? SourceId { get; set; }
        public string SourceOfFundName { get; set; }
        public double CostAmount { get; set; }
        public DateTime? CostDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class SchemeBeneficiariesDto
    {
        public long? Id { get; set; }
        public long? SchemeId { get; set; }
        public string Beneficiary { get; set; }
        public int BeneficiaryCount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class SchemeEmploymentDto
    {
        public long? Id { get; set; }
        public long? SchemeId { get; set; }
        public string PeopleType { get; set; }
        public int EmploymentCount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
