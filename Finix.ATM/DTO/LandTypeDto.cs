﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.ATM.Infrastructure;

namespace Finix.ATM.DTO
{
   public class LandTypeDto
    {
        public long? Id { get; set; }
        public string TypeName { get; set; }
        public Boolean IsWaterBody { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
