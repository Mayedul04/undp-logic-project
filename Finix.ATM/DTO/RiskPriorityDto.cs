﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class RiskPriorityDto
    {
        public long? Id { get; set; }
        public string Element { get; set; }
        public string HazardName { get; set; }
        public long RiskStatementId { get; set; }
        public string RiskStatementName { get; set; }
        public long? UPId { get; set; }
        public string UPName { get; set; }
        public string District { get; set; }
        public int Likelihood { get; set; }
        public int Consequence { get; set; }
        public int RiskPercentage { get; set; }
        public int Rating { get; set; }
        public decimal PeopleAffected { get; set; }
        public decimal LocalEconomyAffected { get; set; }
        public PotentialToProperSolution PotentialToProperSolution { get; set; }
        public decimal PriorityPoint { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
