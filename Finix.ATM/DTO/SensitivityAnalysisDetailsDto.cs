﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class SensitivityAnalysisDetailsDto
    {
        public long? Id { get; set; }
        public long? SensitivityAnalysisId { get; set; }
        public string MainElement { get; set; }
        public string Sensitivity1 { get; set; }
        public string Sensitivity2 { get; set; }
        public string Sensitivity3 { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
