﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
  public  class RiskReductionOptionDto
    {
        public long? Id { get; set; }
        public long LocationId { get; set; }
        public string LocationName { get; set; }
        public long RiskStatementId { get; set; }
        public string RiskStatementName { get; set; }
        public string OptionName { get; set; }
        public bool IsSensitive { get; set; }
        public bool IsExposure { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
