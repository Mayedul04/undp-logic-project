﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
  public  class HouseHoldListDto
    {
        public long? HHId { get; set; }
        public long? InterventionId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? UPId { get; set; }
        public string Address { get; set; }
        public double GrantedAmount { get; set; }
        public double DisbursedAmount { get; set; }
        public bool IsSelected { get; set; }
        public bool HaveIntervertion { get; set; }
    }
}
