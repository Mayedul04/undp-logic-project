﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldEnergyDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public List<HouseHoldEnergySourcesDto> HouseHoldEnergySources { get; set; }
        public string OtherEnergySource { get; set; }
        public decimal SubsistenceDomesticTypePercentage { get; set; }
        public decimal CommercialTypePercentage { get; set; }
        public decimal OthersTypePercentage { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
    public class HouseHoldEnergySourcesDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? SourceId { get; set; }
        public string EnergySourceName{ get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
    public class HouseHoldCropsDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? CropId { get; set; }
        public string CropsName{ get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
}
