﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class CropSeasonDto
    {
        public long? Id { get; set; }
        public string SeasonName { get; set; }
        public string Details { get; set; }
        public long StartMonthId { get; set; }
        public string StartMonthName { get; set; }
        public long EndMonthId { get; set; }
        public string EndMonthName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
