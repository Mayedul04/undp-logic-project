﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class LocationDto
    {
        public long? Id { get; set; }
        public string LocationName { get; set; }
        public string Code { get; set; }
        public LocationLevel Level { get; set; }
        public string LevelName { get; set; }
        public long? ParentID { get; set; }
        public string ParentName { get; set; }
        public string Details { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
