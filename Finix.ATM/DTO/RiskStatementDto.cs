﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class RiskStatementDto
    {
        public long? Id { get; set; }
        public long ElementId { get; set; }
        public string ElementName { get; set; }
        public long ParentLocationId { get; set; }
        public string ParentLocationName { get; set; }
        public long SpecificAreaId { get; set; }
        public string SpecificAreaName { get; set; }
        public long HazardId { get; set; }
        public string HazardName { get; set; }
        public string AfftectedPeriod { get; set; }
        public string Statement { get; set; }
        public string Consequences { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
