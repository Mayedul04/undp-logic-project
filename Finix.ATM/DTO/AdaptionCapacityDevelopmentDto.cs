﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class AdaptionCapacityDevelopmentDto
    {
        public long? Id { get; set; }
        public long? InterventionId { get; set; }
        public string InterventionIdTitle { get; set; }
        public bool GotAnyTraining { get; set; }
        public List<TrainingsDto> Trainings { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }

    public class TrainingsDto
    {
        public long? Id { get; set; }
        public long? AdaptionCapacityId { get; set; }
        public string TrainingName { get; set; }
        public int Duration { get; set; }
        public string ProvidedBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
