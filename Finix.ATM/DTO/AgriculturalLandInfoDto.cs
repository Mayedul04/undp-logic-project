﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class AgriculturalLandInfoDto
    {
        public long? Id { get; set; }
        public long ParentLocationId { get; set; }
        public string ParentLocationName { get; set; }
        public long? LocationId { get; set; }
        public string LocationName { get; set; }
        public long CropId { get; set; }
        public string CropName { get; set; }
        public long CropSeasonId { get; set; }
        public string CropSeasonName { get; set; }
        public decimal Production { get; set; }
        public Boolean AffectedBefore { get; set; }
        public long? HazardId { get; set; }
        public string HazardName { get; set; }
        public string AffectedYear { get; set; }
        public decimal DamagePercentage { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
