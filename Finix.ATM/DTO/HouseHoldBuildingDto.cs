﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldBuildingDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public int RentalType { get; set; }
        public string RentalTypeName { get; set; }
        public decimal? LandAmount { get; set; }
        public double? LandValue { get; set; }
        public int AgeofHouse { get; set; }
        public long? OutsideWallMadeof { get; set; }
        public long? FondationMadeof { get; set; }
        public long? RoofMadeof { get; set; }
        public int RoomCount { get; set; }
        public int HouseType { get; set; }
        public string HouseTypeName { get; set; }
        public int DistancetoUP { get; set; }
        public int DistancetoMarket { get; set; }
        public int? DistancetoShelter { get; set; }
        public bool? ProximitytoRiver { get; set; }
        public bool? IsInsideEmbankment { get; set; }
        public double MainBuildingSize { get; set; }
        public double MainBuildingValue { get; set; }
        public int BuildingCondition { get; set; }
        public int RoofCondition { get; set; }
        public bool IsPronetoHazard { get; set; }
        public List<HouseHoldHazardsDto> PronetoHazards { get; set; }
        public List<HouseHoldOtherBuildingsDto> OtherBuildings { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }

    public class HouseHoldOtherBuildingsDto
    {
        public long? Id { get; set; }
        public long? HouseHoldBuildingId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? BuildingId { get; set; }
        public string BuildingName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }

    public class HouseHoldHazardsDto
    {
        public long? Id { get; set; }
        public long? HouseHoldBuildingId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? HazardId { get; set; }
        public string HazardName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
}
