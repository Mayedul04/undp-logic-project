﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
  public  class CapacityDevelopmentActivityDto
    {
        public long? Id { get; set; }
        public string EventName { get; set; }
        public long? EventTypeId { get; set; }
        public string EventTypeName { get; set; }
        public double ActualCost { get; set; }
        public int Duration { get; set; }
        public string Venue { get; set; }
        public long? LocationId { get; set; }
        public string LocationName { get; set; }
        public string ExecutedBy { get; set; }
        public string OrganizedBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Renarks { get; set; }
        public List<ParticipantsDto> Participants { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }

    public class ParticipantsDto
    {
        public long? Id { get; set; }
        public long? CapacityDevelopmentActivityId { get; set; }
        public string PeopleType { get; set; }
        public int Count { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
