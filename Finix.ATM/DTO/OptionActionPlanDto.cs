﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
  public  class OptionActionPlanDto
    {
        public long? Id { get; set; }
        public long UPId { get; set; }
        public string LocationName { get; set; }
        public long RiskStatementId { get; set; }
        public string Statement { get; set; }
        public string OptionId { get; set; }
        public string OptionName { get; set; }
        public string WhoWilldo { get; set; }
        public string When { get; set; }
        public string How { get; set; }
        public string Where { get; set; }
        public string Consideration { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
