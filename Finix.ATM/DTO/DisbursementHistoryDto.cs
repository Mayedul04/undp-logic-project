﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class DisbursementHistoryDto
    {
        public long? Id { get; set; }
        public long? InterventionId { get; set; }
        public string InterventionTitle { get; set; }
        public DateTime DisburseDate { get; set; }
        public double DisbursedAmount { get; set; }
        public double RemainingFund { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
