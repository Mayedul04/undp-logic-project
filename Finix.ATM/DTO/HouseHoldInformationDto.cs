﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
  public  class HouseHoldInformationDto
    {
        public long? Id { get; set; }
        public string HouseHoldNo { get; set; }
        public string HouseHoldHead { get; set; }
        public int MemberCount { get; set; }
        public long? OccupationOfHH { get; set; }
        public string AffectedBy { get; set; }
        public string Hazards { get; set; }
        public string HazardName { get; set; }
        public bool AgentBanking { get; set; }
        public long? UpId { get; set; }
        public string UPName { get; set; }
        public long? ParentLocationId { get; set; }
        public string WardName { get; set; }
        public long? VillageId { get; set; }
        public string LocationName{ get; set; }
        public string Address { get; set; }
        public string Details { get; set; }
        public bool HasBankAccount { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public bool HasMobileNumber { get; set; }
        public string HHeadMobileNo { get; set; }
        public int? HHeadReligion { get; set; }
        public string HHeadReligionName { get; set; }
        public int OccupantNumber { get; set; }
        public decimal MonthlyAvgIncome { get; set; }
        public int LivingPeriod { get; set; }
        public bool? HasSavings { get; set; }
        public int? RentalType { get; set; }
        public bool? HaveLoan { get; set; }
        public bool SafetyNetMember { get; set; }
        public double SavingAmount { get; set; }
        public bool? IsNGOMember { get; set; }
        public bool? IsWomenHeaded { get; set; }
        public bool? IsEthnicMinority { get; set; }
        public long? EthnicMinorityId { get; set; }
        public string EthnicMinorityName { get; set; }
        public bool? HasAdolsentMother { get; set; }
        public bool? WentforWork { get; set; }
        public bool? ProximitytoRiver { get; set; }
        public bool? IsEmbankment { get; set; }
        public bool? OutsideHomeDistrict { get; set; }
        public bool IsLivingLifeTime { get; set; }
        public bool? IsDisableMember { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
    public class HouseHoldSurveyAdministrationDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public string Admininstrator { get; set; }
        public string SurveyConductor { get; set; }
        public string InformationProvider { get; set; }
        public DateTime? SurveyDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
    public class HouseHoldMembersDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public string MemberName { get; set; }
        public int Gender { get; set; }
        public string GenderName { get; set; }
        public int Age { get; set; }
        public int? MaritalStatus { get; set; }
        public string MaritalStatusTitle { get; set; }
        public string Education { get; set; }
        public long? OccupationId { get; set; }
        public string OccupationName { get; set; }
        public double PrimaryIncome { get; set; }
        public string SecondaryOccupation { get; set; }
        public double SecondaryIncome { get; set; }
        public int? RelationtoHH { get; set; }
        public string RelationtoHHTitle { get; set; }
        public string NID { get; set; }
        public bool IsSafetyNet { get; set; }
        public bool IsHeadPerson { get; set; }
        public bool IsDisable { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
}
