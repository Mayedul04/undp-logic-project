﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class ElementAtRiskDto
    {
        public long? Id { get; set; }
        public long ElementId { get; set; }
        public string ElementName { get; set; }
        public string ElementType { get; set; }
        public long HazardId { get; set; }
        public string HazardName { get; set; }
        public long UPId { get; set; }
        public string UPName { get; set; }
        public string District { get; set; }
        public decimal Exposure { get; set; }
        public int Likelihood { get; set; }
        public decimal Consequences { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
