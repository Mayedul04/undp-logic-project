﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class LandUseDto
    {
        public long? Id { get; set; }
        public string LandName { get; set; }
        public long? ParentLocationId { get; set; }
        public string ParentLocationName { get; set; }
        public long? LocationId { get; set; }
        public  string LocationName { get; set; }
        public long LandTypeID { get; set; }
        public string LandTypeName { get; set; }
        public Boolean IsAnyChange { get; set; }
        public string Changes { get; set; }
        public Boolean AnyHazardCreatedBy { get; set; }
        public long? CreatedHazardId { get; set; }
        public string CreatedHazardName { get; set; }
        public Boolean IsNavigable { get; set; }
        public Boolean PreviouslyAffectedbyDisaster { get; set; }
        public long? DisasterId { get; set; }
        public string DisasterName { get; set; }
        public string PresentUse { get; set; }
        public Boolean IsWaterAvailabeThroughOutYear { get; set; }
        public bool IsTackenByThirdParty { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; } 


    }
}
