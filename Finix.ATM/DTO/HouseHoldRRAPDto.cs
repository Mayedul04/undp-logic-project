﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
    public class HouseHoldRRAPDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public bool IsThereAnyCommunityInitiative { get; set; }
        public string WhatAreTheInitiatives { get; set; }
        public string WhatAreTheBenefitsReceivingByTheHH { get; set; }
        public bool IsThereAnyThreatForTheHHFromTheCommunityInitiative { get; set; }
        public string WhatAreTheThreats { get; set; }
        public string ImpactsOfCommunityLevelInitiativeOnHHThreatIdentified { get; set; }
        public string GapsForHHIntervention { get; set; }
        public string LivelihoodRiskReductionPlan { get; set; } 
        public string LifeAndAssetLossReductionPlan { get; set; }
        public string PlanForSupportingNeighborhoodThreats { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
