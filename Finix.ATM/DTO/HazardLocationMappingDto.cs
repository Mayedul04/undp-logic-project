﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HazardLocationMappingDto
    {
        public long? Id { get; set; }
        public long HazardId { get; set; }
        public string HazardName { get; set; }
        public long ParentLocationId { get; set; }
        public string ParentLocationName { get; set; }
        public long LocationId { get; set; }
        public string   LocationName { get; set; }
        public long StartMonthId { get; set; }
        public string StartMonthName { get; set; }
        public long EndMonthId { get; set; }
        public string EndMonthName { get; set; }
        public int Consequence { get; set; }
        public string January { get; set; }
        public string February { get; set; }
        public string March { get; set; }
        public string April { get; set; }
        public string May { get; set; }
        public string June { get; set; }
        public string July { get; set; }
        public string August { get; set; }
        public string September { get; set; }
        public string October { get; set; }
        public string November { get; set; }
        public string December { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
