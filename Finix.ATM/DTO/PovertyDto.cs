﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class PovertyDto
    {
        public long? Id { get; set; }
        public long? LocalEconomyId { get; set; }
        public long? LocationId { get; set; }
        public PovertySituation PovertySituation { get; set; }
        public string PovertySituationName { get; set; }
        public decimal PopulationPercentage { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
