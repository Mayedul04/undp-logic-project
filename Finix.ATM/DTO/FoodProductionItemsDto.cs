﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class FoodProductionItemsDto
    {
        public string ItemName { get; set; }
        public String FoodProductionType { get; set; }
    }
}
