﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldPotentialImpactDto
    {
        public long? Id { get; set; }
        public string PotentialImpact { get; set; }
        public long? ElementTypeId { get; set; }
        public string ElementTypeName { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? UPId { get; set; }
        public string LocationName { get; set; }
        public int Likelihood { get; set; }
        public int Consequence { get; set; }
        public string Consequences { get; set; }
        public decimal Rating { get; set; }
        public decimal AdaptiveCapacityScore { get; set; }
        public decimal VulnerabilityScore { get; set; }
        public List<HHIndicatorScoreDto> HHIndicatorScores { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class HHIndicatorScoreDto
    {
        public long? Id { get; set; }
        public long RiskId { get; set; }
        public string PotentialImpact { get; set; }
        public string IndicatorId { get; set; }
        public string IndicatorTitle { get; set; }
        public decimal Score { get; set; }
        public DateTime? CalulatedDate { get; set; }
        public bool IsSelected { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
