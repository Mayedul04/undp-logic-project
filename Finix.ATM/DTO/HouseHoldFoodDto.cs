﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldFoodDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public List<HouseHoldMainFoodsDto> MainFoods { get; set; }
        public bool HadShortage { get; set; }
        public List<HouseHoldFoodShortagesDto> FoodShortages { get; set; }
        public float PurchasedFood { get; set; }
        public float GrownFood { get; set; }
        public bool IsStocked { get; set; }
        public int StockDuration { get; set; }
        public float Firewood { get; set; }
        public float Electricity { get; set; }
        public float Gas { get; set; }
        public float Others { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
    public class HouseHoldMainFoodsDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public string FoodName { get; set; }
        public bool IsGrownFood { get; set; }
        public bool IsPurchasedFood { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
    public class HouseHoldFoodShortagesDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public string ShortageName { get; set; }
        public string CausedBy { get; set; }
        public int ShortageLength { get; set; }
        public string FoodType { get; set; }
        public DateTime? ShortageDate { get; set; }
        public string ActionTaken { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
}
