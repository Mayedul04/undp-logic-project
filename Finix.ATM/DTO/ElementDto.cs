﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class ElementDto
    {
        public long? Id { get; set; }
        public long ElementTypeId { get; set; }
        public string ElementTypeName { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public long  ParentLocationId { get; set; }
        public string ParentLocationName { get; set; }
        public long LocationId { get; set; }
        public string LocationName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool IsAffectedBefore { get; set; }
        public long Year { get; set; }
        public long HazardId { get; set; }
        public string HazardName { get; set; }
        public decimal DamagePercentage { get; set; }
        public string PreviousPosition { get; set; }
        public string CurrentPosition { get; set; }
        public string PreviousType { get; set; }
        public string CurrentType { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
