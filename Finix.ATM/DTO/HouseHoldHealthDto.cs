﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldHealthDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public double AnnualHealthExpenditure { get; set; }
        public bool SufferedLastThreeMonth { get; set; }
        public string DiseaseName { get; set; }
        public bool IsWaterSafe { get; set; }
        public List<HouseHoldWaterSourceDto> MainWaterSources { get; set; }
        public bool? HasLatrine { get; set; }
        public int? LatrineType { get; set; }
        public string LatrineTypeName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
    public class HouseHoldWaterSourceDto
    {
        public long? Id { get; set; }
        public long? HouseHoldHealthId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? SourceId { get; set; }
        public string WaterSourceName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }

    }
}
