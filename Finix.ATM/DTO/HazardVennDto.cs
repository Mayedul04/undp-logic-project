﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HazardVennDto
    {
        public long HazardId { get; set; }
        public string HazardName { get; set; }
        public long LocationId { get; set; }
        public string LocationName { get; set; }
        public long StartMonthId { get; set; }
        public long EndMonthId { get; set; }
        public int Consequence { get; set; }
        public decimal radius { get; set; }
        public decimal xAxis { get; set; }
        public string FillColor { get; set; }
        public string LineColor { get; set; }
    }
}
