﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldElementsAnalysisDto
    {
        public long? Id { get; set; }
        public long? ElementTypeId { get; set; }
        public string ElementTypeName { get; set; }
        public long HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public long? HazardId { get; set; }
        public string HazardName { get; set; }
        public int Exposed { get; set; }
        public decimal ExposedValue { get; set; }
        public int Sensibility { get; set; }
        public decimal SensibilityValue { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
