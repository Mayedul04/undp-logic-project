﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldDashboardDto
    {
        public int HHCount { get; set; }
        public int SelectedHHCount { get; set; }
        public int ProposedInterventionCount { get; set; }
        public double TotalDisbursement { get; set; }
    }
}
