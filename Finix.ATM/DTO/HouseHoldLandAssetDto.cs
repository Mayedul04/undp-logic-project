﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class HouseHoldLandAssetDto
    {
        public long? Id { get; set; }
        public long? HHId { get; set; }
        public string HouseHoldNo { get; set; }
        public decimal CropLandSize { get; set; }
        public double CropLandValue { get; set; }
        public decimal WaterBodySize { get; set; }
        public double WaterBodyValue { get; set; }
        public List<HouseHoldAgriToolsDto> AgriculturalTools { get; set; }
        public double AgriToolsValue { get; set; }
        public int PoultryCount { get; set; }
        public int GoatCowCount { get; set; }
        public int OthersCount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class HouseHoldAgriToolsDto
    {
        public long? Id { get; set; }
        public long? AssetId { get; set; }
        public long? ToolId { get; set; }
        public string AgriculturalToolName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
