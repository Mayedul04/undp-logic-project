﻿using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.DTO
{
   public class IndicatorsDto
    {
        public long? Id { get; set; }
        public string IndicatorName { get; set; }
        public long? Type { get; set; }
        public string IndicatorTypeName { get; set; }
        public float Weight { get; set; }
        public float OldWeight { get; set; }
        public decimal TargetYear1 { get; set; }
        public decimal TargetYear2 { get; set; }
        public decimal TargetYear3 { get; set; }
        public decimal TargetYear4 { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
}
