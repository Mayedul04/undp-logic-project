﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure
{

    //public enum WaterSource { OwnTubewell = 1, CommunityTubewell = 2, OwnPond=3, CommunityPond = 4, Bottled= 5, Catchments = 6 }
    public enum LocationLevel { Country=0, Division = 1, District = 2, Upazila = 3, UP = 4, Ward = 5, Village = 6 }
    public enum Months { January = 1, February = 2, March = 3, April = 4, May = 5, June = 6, July = 7, August=8, September=9, October=10, November=11, December=12 }
    public enum Gender { Male=1, Female=2, Transgender=3 }
    public enum MaritalStatus { Single = 1, Married = 2, Divorced = 3 }
    public enum Religion { Muslim = 1, Hindu = 2, Buddist = 3, Christian=4, Others=5 }
    public enum Relations {[Display(Name = "House Hold Head")]HHead = 1, [Display(Name = "Father")]Father = 2, [Display(Name = "Mother")]Mother = 3, [Display(Name = "Son")]Son = 4, [Display(Name = "Daughter")]Daughter =5, [Display(Name = "Son in Law")]SoninLaw =6, [Display(Name = "Daugher in Law")]DaughterinLaw =7, [Display(Name = "Father in Law")]FatherinLaw =8, [Display(Name = "Mother in Law")]MotherinLaw =9 }
    public enum BuildingCondition { Bad = 1, Fair = 2, Good = 3, Excellent=4 }
    public enum PovertySituation {[Display(Name = "Extreme Poor")]ExtremePoor  = 1, [Display(Name = "Poor")]Poor = 2, [Display(Name = "Marginal Poor")]MarginalPoor = 3, [Display(Name = "Non-Poor")]NonPoor =4 }

    public enum FoodProductionType {[Display(Name = "Crop")]Crop=1, [Display(Name = "Fisheries")]Fisheries = 2, [Display(Name = "LiveStocks")]LiveStocks = 3, [Display(Name = "Poultry")]Poultry = 4 }
    public enum PotentialToProperSolution {[Display(Name = "Very High")]VeryHigh = 100, [Display(Name = "High")]High = 80, [Display(Name = "Moderate")]Moderate = 60, [Display(Name = "Low")]Low = 40, [Display(Name = "Rare")]Rare = 20, [Display(Name = "Not Potential")]No = 0 }

    public enum IndicatorPrimaryGroup {[Display(Name = "HH Vulnerability Calculation")]HHVC = 1, [Display(Name = "Measuring")]Measuring = 2, [Display(Name = "Tracking")]Tracking = 3 }
    public enum HouseType { Jhupri = 1, Kacca = 2, SemiKacca = 3, Pacca=4 }
    public enum HouseRenatalType {[Display(Name = "Own")]Own = 1, [Display(Name = "Rent")]Rent = 2, [Display(Name = "Others House")]Others = 3, [Display(Name = "Khas Land")]KhasLand = 4 }

    public enum LatrineType {[Display(Name = "Sanitary")]Sanitary = 1, [Display(Name = "Slab")]Slab = 2, [Display(Name = "Semi Slab")]SemiSlab = 3, [Display(Name = "Kaccha")]Kaccha = 4 }
    public enum SchemeType { CRF = 1, PBCRG = 2}
}