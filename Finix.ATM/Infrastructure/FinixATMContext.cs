﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using Finix.ATM.Util;
using Finix.ATM.Infrastructure.Models;

namespace Finix.ATM.Infrastructure
{
    public interface IFinixATMContext
    {
    }

    public partial class FinixATMContext : DbContext, IFinixATMContext
    {
        #region ctor
        static FinixATMContext()
        {
            Database.SetInitializer<FinixATMContext>(null);
        }

        public FinixATMContext()
            : base("Name=FinixATMContext")
        {
            Database.SetInitializer(new FinixATMContextInitializer());
        }
        #endregion

        #region Models

        public DbSet<Location> Location { get; set; }
        public DbSet<Consequnces> Consequnces { get; set; }
        public DbSet<Exposure> Exposure { get; set; }
        public DbSet<Sensitivity> Sensitivity { get; set; }
        public DbSet<Element> Element { get; set; }
        public DbSet<ElementType> ElementType { get; set; }
        public DbSet<BuildingPart> BuildingPart { get; set; }
        public DbSet<AdaptiveCapacityStatement> AdaptiveCapacityStatement { get; set; }
        public DbSet<Crops> Crops { get; set; }
        public DbSet<CropCalender> CropCalender { get; set; }
        public DbSet<CultivationSeason> CultivationSeason { get; set; }
        public DbSet<EnergySource> EnegySource { get; set; }
        public DbSet<EngBangMonthMapping> EngBangMonthMapping { get; set; }
        public DbSet<EventType> EventType { get; set; }
        public DbSet<Hazard> Hazard { get; set; }
        
        public DbSet<HazardLocationMapping> HazardLocationMapping { get; set; }
        public DbSet<HouseHoldFood> HouseHoldFood { get; set; }
        public DbSet<HouseHoldHealth> HouseHoldHealth { get; set; }
        public DbSet<HouseHoldInformation> HouseHoldInformation { get; set; }
        public DbSet<HouseHoldMembers> HouseHoldMembers { get; set; }
        public DbSet<Indicators> Indicators { get; set; }
        public DbSet<IndicatorType> IndicatorType { get; set; }
        public DbSet<HouseHoldMainFoods> MainFoods { get; set; }
        public DbSet<Material> Material { get; set; }
        public DbSet<RiskStatement> RiskStatement { get; set; }
        public DbSet<RiskReductionOption> RiskReductionOption { get; set; }
        public DbSet<LandType> LandType { get; set; }
        public DbSet<LandUse>  LandUse { get; set; }
        public DbSet<SchemeCategory> SchemeCategory { get; set; }
        public DbSet<SchemeNature> SchemeNature { get; set; }
        public DbSet<AgriculturalLandInfo> AgriculturalLandInfo { get; set; }
        public DbSet<PopulationGroup> PopulationGroup { get; set; }
        public DbSet<PopulationVulnerability> PopulationVulnerability { get; set; }
        public DbSet<Employments> Employments { get; set; }
        public DbSet<FoodProductionItems> FoodProductionItems { get; set; }
        public DbSet<LocalEconomyVulnerability> LocalEconomyVulnerability { get; set; }
        public DbSet<ElementAtRisk> ElementAtRisk { get; set; }
        public DbSet<RiskPriority> RiskPriority { get; set; }
        public DbSet<SensitivityAnalysis> SensitivityAnalysis { get; set; }
        public DbSet<HouseHoldSurveyAdministration> HouseHoldSurveyAdministration { get; set; }
        public DbSet<HouseHoldBuilding> HouseHoldBuilding { get; set; }
        public DbSet<HouseHoldEnergy> HouseHoldEnergy { get; set; }
        public DbSet<SourceOfFund> SourceOfFund { get; set; }
        public DbSet<Interventions> Interventions { get; set; }
        public DbSet<SupportType> SupportType { get; set; }
        public DbSet<InterventionImplementation> InterventionImplementation { get; set; }
        public DbSet<DisbursementHistory> DisbursementHistory { get; set; }
        public DbSet<OptionActionPlan> OptionActionPlan { get; set; }
        public DbSet<HouseHoldElementsAnalysis> HouseHoldElementsAnalysis { get; set; }
        public DbSet<HouseHoldPotentialImpact> HouseHoldPotentialImpact { get; set; }
        public DbSet<IndicatorLog> IndicatorLog { get; set; }
        public DbSet<EthnicMinority> EthnicMinority { get; set; }
        public DbSet<OtherBuilding> OtherBuilding { get; set; }
        public DbSet<AgriculturalTools> AgriculturalTools { get; set; }
        public DbSet<HouseHoldLandAsset> HouseHoldLandAsset { get; set; }
        public DbSet<HouseHoldDisasters> HouseHoldDisasters { get; set; }
        public DbSet<SchemeInformation> SchemeInformation { get; set; }
        public DbSet<AdaptionCapacityDevelopment> AdaptionCapacityDevelopment { get; set; }
        public DbSet<HouseHoldRRAP> HouseHoldRRAPs { get; set; }
        public DbSet<CapacityDevelopmentActivity> CapacityDevelopmentActivity { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            FluentConfigurator.ConfigureGenericSettings(modelBuilder);
            FluentConfigurator.ConfigureMany2ManyMappings(modelBuilder);
            FluentConfigurator.ConfigureOne2OneMappings(modelBuilder);
        }
    }

    internal class FinixATMContextInitializer : CreateDatabaseIfNotExists<FinixATMContext> //CreateDatabaseIfNotExists<CardiacCareContext>
    {
        protected override void Seed(FinixATMContext context)
        {
            var seedDataPath = ATMSystem.SeedDataPath;
            var dropDb = ATMSystem.DropDB;
            if (string.IsNullOrWhiteSpace(seedDataPath))
                return;
            var folders = Directory.GetDirectories(seedDataPath).ToList().OrderBy(x => x);
            var msg = "";
            bool error = false;
            try
            {
                foreach (var folder in folders)
                {
                    msg += string.Format("processing for: {0}{1}", folder, Environment.NewLine);

                    var fileDir = Path.Combine(new[] { seedDataPath, folder });
                    var sqlFiles = Directory.GetFiles(fileDir, "*.sql").OrderBy(x => x).ToList();
                    foreach (var file in sqlFiles)
                    {
                        try
                        {
                            msg += string.Format("processing for: {0}{1}", file, Environment.NewLine);
                            context.Database.ExecuteSqlCommand(File.ReadAllText(file));
                            msg += string.Format("Done....{0}", Environment.NewLine);
                        }
                        catch (Exception ex)
                        {
                            error = true;
                            msg += string.Format("Failed!....{0}", Environment.NewLine);
                            msg += string.Format("{0}{1}", ex.Message, Environment.NewLine);
                        }
                    }
                }
                if (error)
                {
                    throw new Exception(msg);
                }
                base.Seed(context);
            }
            catch (Exception ex)
            {
                msg = "Error Occured while inserting seed data" + Environment.NewLine;
                if (dropDb)
                {
                    context.Database.Delete();
                    msg += ("Database is droped" + Environment.NewLine);
                }
                var errFile = seedDataPath + "\\seed_data_error.txt";
                msg += ex.Message;
                File.WriteAllText(errFile, msg);

            }
        }

    }
}