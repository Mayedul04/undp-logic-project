﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HazardLocationMapping:Entity
    {
        public long HazardId { get; set; }
        public virtual Hazard Hazard { get; set; }
        public long LocationId { get; set; }
        public virtual Location Location { get; set; }
        public long StartMonthId { get; set; }
        [ForeignKey("StartMonthId")]
        public virtual EngBangMonthMapping StartMonth { get; set; }
        public long EndMonthId { get; set; }
        [ForeignKey("EndMonthId")]
        public virtual EngBangMonthMapping EndMonth { get; set; }
        public int Consequence { get; set; }
    }
}
