﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
  public  class ElementType : Entity
    {
        public string TypeName { get; set; }
        public decimal Weight { get; set; }
    }
}
