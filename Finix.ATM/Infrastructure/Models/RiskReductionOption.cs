﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class RiskReductionOption :Entity
    {
        public long RiskStatementId { get; set; }
        public virtual  RiskStatement RiskStatement  { get; set; }
        public string OptionName { get; set; }
        public bool IsSensitive { get; set; }
        public bool IsExposure { get; set; }
    }
}
