﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class CultivationSeason :Entity
    {
        public string SeasonName { get; set; }
        public string Details { get; set; }
        public long StartMonthId  { get; set; }
        public virtual EngBangMonthMapping StartMonth { get; set; }
        public long EndMonthId { get; set; }
        public virtual EngBangMonthMapping EndMonth { get; set; }
    }
}
