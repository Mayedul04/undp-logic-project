﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class DisbursementHistory :Entity
    {
        public long? InterventionId { get; set; }
        [ForeignKey("InterventionId")]
        public virtual Interventions Intervention { get; set; }
        public DateTime DisburseDate { get; set; }
        public double DisbursedAmount { get; set; }
        public double RemainingFund { get; set; }

    }
}
