﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldPotentialImpact :Entity
    {
        public string PotentialImpact { get; set; }
        public long? ElementTypeId { get; set; }
        public virtual ElementType ElementType { get; set; }
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public int Likelihood { get; set; }
        public int Consequence { get; set; }
        public string Consequences { get; set; }
        public decimal Rating { get; set; }
        public decimal? AdaptiveCapacityScore { get; set; }
        public decimal? VulnerabilityScore { get; set; }
        public ICollection<HHIndicatorScore> HHIndicatorScores { get; set; }
    }

    public class HHIndicatorScore : Entity
    {
        public long RiskId { get; set; }
        [ForeignKey("RiskId"), InverseProperty("HHIndicatorScores")]
        public virtual HouseHoldPotentialImpact HHRisk { get; set; }
        public long IndicatorId { get; set; }
        [ForeignKey("IndicatorId")]
        public virtual Indicators Indicator { get; set; }
        public decimal Score { get; set; }
        public DateTime? CalulatedDate { get; set; }
    }
}
