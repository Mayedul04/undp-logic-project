﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldElementsAnalysis :Entity
    {
        public long? ElementTypeId { get; set; }
        public virtual ElementType ElementType { get; set; }
        public long HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public string HouseHoldNo { get; set; }
        public long? HazardId { get; set; }
        public virtual Hazard Hazard { get; set; }
        public int Exposed { get; set; }
        public decimal ExposedValue { get; set; }
        public int Sensibility { get; set; }
        public decimal SensibilityValue { get; set; }
        

    }
}
