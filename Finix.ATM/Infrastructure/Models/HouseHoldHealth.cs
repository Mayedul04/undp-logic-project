﻿using Finix.Auth.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldHealth:Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public double AnnualHealthExpenditure { get; set; }
        public bool SufferedLastThreeMonth { get; set; }
        public string DiseaseName { get; set; }
        public bool IsWaterSafe { get; set; }
        public bool? HasLatrine { get; set; }
        public LatrineType? LatrineType { get; set; }
        public virtual ICollection<HouseHoldWaterSource> MainWaterSources { get; set; }
    
    }
    public class HouseHoldWaterSource : Entity
    {
        public long? HouseHoldHealthId { get; set; }
        [ForeignKey("HouseHoldHealthId")]
        public virtual HouseHoldHealth HouseHoldHealth { get; set; }
        public long? SourceId { get; set; }
        [ForeignKey("SourceId")]
        public virtual WaterSource WaterSource { get; set; }
        
    }
}
