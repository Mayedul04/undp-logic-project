﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldInformation :Entity
    {
        public string HouseHoldNo { get; set; }
        public long? VillageId { get; set; }
        public virtual Location Village { get; set; }
        public string Address { get; set; }
        public string HHeadMobileNo { get; set; }
        public Religion? HHeadReligion { get; set; }
        public string Details { get; set; }
        public bool HasBankAccount { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public int OccupantNumber { get; set; }
        public double MonthlyAvgIncome { get; set; }
        public int LivingPeriod { get; set; }
        public bool IsLivingLifeTime { get; set; }
        public bool HasLoan { get; set; }
        public double LoanAmount { get; set; }
        public bool? HasSavings { get; set; }
        public double? SavingAmount { get; set; }
        public bool? IsNGOMember { get; set; }
        public bool? IsEthnicMinority { get; set; }
        public long? EthnicMinorityId { get; set; }
        public virtual EthnicMinority EthnicMinority { get; set; }
        public bool? HasAdolsentMother { get; set; }
        public bool? WentforWork { get; set; }
        public bool? OutsideHomeDistrict { get; set; }
        public bool IsEligibleforScheme { get; set; }
        
    }

    public class HouseHoldSurveyAdministration : Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public string Admininstrator { get; set; }
        public string SurveyConductor { get; set; }
        public string InformationProvider { get; set; }
        public DateTime? SurveyDate { get; set; }
    }

    public class HouseHoldMembers : Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public string MemberName { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public MaritalStatus? MaritalStatus { get; set; }
        public string Education { get; set; }
        public long? OccupationId { get; set; }
        [ForeignKey("OccupationId")]
        public virtual Employments Occupation { get; set; }
        public double PrimaryIncome { get; set; }
        public string SecondaryOccupation { get; set; }
        public double SecondaryIncome { get; set; }
        public Relations? RelationtoHH { get; set; }
        public string NID { get; set; }
        public bool IsSafetyNet { get; set; }
        public bool IsHeadPerson { get; set; }
        public bool IsDisable { get; set; }
    }
}
