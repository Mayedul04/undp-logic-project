﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
    public class BuildingPart: Entity
    {
        public string PartName { get; set; }
    }
}
