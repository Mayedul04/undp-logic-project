﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class AdaptionCapacityDevelopment :Entity
    {
        public long? InterventionId { get; set; }
        [ForeignKey("InterventionId")]
        public virtual Interventions Intervention { get; set; }
        public bool GotAnyTraining { get; set; }
        public ICollection<Trainings> Trainings { get; set; }
    }
    public class Trainings : Entity
    {
        public long? AdaptionCapacityId { get; set; }
        [ForeignKey("AdaptionCapacityId")]
        public virtual AdaptionCapacityDevelopment AdaptionCapacityDevelopment { get; set; }
        public string TrainingName { get; set; }
        public int Duration { get; set; }
        public string ProvidedBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
