﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class OptionActionPlan :Entity
    {
        public long? UPId { get; set; }
        [ForeignKey("UPId")]
        public virtual Location Location { get; set; }
        public long? RiskStatementId { get; set; }
        [ForeignKey("RiskStatementId")]
        public virtual RiskStatement RiskStatement { get; set; }
        public long? OptionId { get; set; }
        [ForeignKey("OptionId")]
        public virtual RiskReductionOption Option { get; set; }
        public string   WhoWilldo { get; set; }
        public string When { get; set; }
        public string How { get; set; }
        public string Where { get; set; }
        public string Consideration { get; set; }
    }
}
