﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class AgriculturalLandInfo :Entity
    {
        public long? LocationId { get; set; }
        public virtual Location Location { get; set; }
        public long CropId { get; set; }
        [ForeignKey("CropId")]
        public virtual Crops Crop { get; set; }
        public long CropSeasonId { get; set; }
        [ForeignKey("CropSeasonId")]
        public virtual CultivationSeason CropSeason { get; set; }
        public decimal Production { get; set; }
        public Boolean AffectedBefore { get; set; }
        public long? HazardId { get; set; }
        [ForeignKey("HazardId")]
        public virtual Hazard Hazard { get; set; }
        public string AffectedYear { get; set; }
        public decimal DamagePercentage { get; set; }
    }
}
