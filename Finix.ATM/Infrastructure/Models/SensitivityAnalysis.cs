﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class SensitivityAnalysis :Entity
    {
        public long? LocationId { get; set; }
        public virtual Location Location { get; set; }
        public long? RiskStatementId { get; set; }
        public virtual RiskStatement RiskStatement { get; set; }
        public virtual ICollection<SensitivityAnalysisDetails> SensitivityAnalysisDetail { get; set; }

    }
}
