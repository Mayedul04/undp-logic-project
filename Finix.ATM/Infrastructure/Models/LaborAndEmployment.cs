﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
    public class LaborAndEmployment : Entity
    {
        public long? LocalEconomyId { get; set; }
        [ForeignKey("LocalEconomyId"), InverseProperty("LaborAndEmployments")]
        public virtual LocalEconomyVulnerability LocalEconomy { get; set; }
        public long? LocationId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public long? EmploymentId { get; set; }
        [ForeignKey("EmploymentId")]
        public virtual Employments Employment { get; set; }
        public decimal PeopleEngaged { get; set; }
        public string Vulnerabilities { get; set; }
    }
}
