﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
  public  class Exposure:Entity
    {
        public long ElementId { get; set; }
        public virtual Element Element { get; set; }
        public long HazardId { get; set; }
        public virtual Hazard Hazard { get; set; }
        public decimal ExposurePercentage { get; set; }
        public int Likelihood { get; set; }
        public List<Consequnces> Consequences { get; set; }
    }
}
