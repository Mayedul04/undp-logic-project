﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldFood:Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public ICollection<HouseHoldMainFoods> MainFoods { get; set; }
        public bool HadShortage { get; set; }
        public ICollection<HouseHoldFoodShortages> FoodShortages { get; set; }
        public float PurchasedFood { get; set; }
        public float GrownFood { get; set; }
        public bool IsStocked { get; set; }
        public int StockDuration { get; set; }
        public float Firewood { get; set; }
        public float Electricity { get; set; }
        public float Gas { get; set; }
        public float Others { get; set; }

    }
    public class HouseHoldMainFoods : Entity
    {
        public string FoodName { get; set; }
        public long? HouseHoldFoodId { get; set; }
        [ForeignKey("HouseHoldFoodId"), InverseProperty("MainFoods")]
        public virtual HouseHoldFood HouseHoldFood { get; set; }
        public bool IsGrownFood { get; set; }
    }
    public class HouseHoldFoodShortages : Entity
    {
        public long? HouseHoldFoodId { get; set; }
        [ForeignKey("HouseHoldFoodId"), InverseProperty("FoodShortages")]
        public virtual HouseHoldFood HouseHoldFood { get; set; }
        public string CausedBy { get; set; }
        public int ShortageLength { get; set; }
        public string FoodType { get; set; }
        public DateTime? ShortageDate { get; set; }
        public string ActionTaken { get; set; }
    }
}
