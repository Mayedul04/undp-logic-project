﻿using Finix.Auth.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class Location :Entity
    {
        public string LocationName { get; set; }
        public string Code { get; set; }
        public LocationLevel Level { get; set; }
        public long? ParentID { get; set; }
        [ForeignKey("ParentID")]
        public virtual Location Parent { get; set; }
        public string Details { get; set; } 
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
