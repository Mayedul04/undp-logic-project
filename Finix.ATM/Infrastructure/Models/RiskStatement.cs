﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public  class RiskStatement :Entity
    {
        public long ElementId { get; set; }
        public virtual Element Element { get; set; }
        public long SpecificAreaId { get; set; }
        public virtual Location SpecificArea { get; set; }
        public long HazardId { get; set; }
        public virtual Hazard Hazard { get; set; }
        public string AfftectedPeriod { get; set; }
        public string Statement { get; set; }
        public string Consequences { get; set; }
        

    }
}
