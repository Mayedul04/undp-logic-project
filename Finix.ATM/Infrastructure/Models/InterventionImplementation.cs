﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class InterventionImplementation :Entity
    {
        public long? InterventionId { get; set; }
        [ForeignKey("InterventionId")]
        public virtual Interventions Intervention { get; set; }
        public double ActualCost { get; set; }
        public double? CRFGrant { get; set; }
        public double? OwnContribution { get; set; }
        public string ImplementedBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ICollection<InterventionExpenditures> Expenses { get; set; }
    }
    public class InterventionExpenditures : Entity
    {
        public long? ImplementationId { get; set; }
        [ForeignKey("ImplementationId")]
        public virtual InterventionImplementation InterventionImplementation { get; set; }
        public long? SourceId { get; set; }
        [ForeignKey("SourceId")]
        public virtual SourceOfFund SourceOfFund { get; set; }
        public double CostAmount { get; set; }
        public DateTime? CostDate { get; set; }
    }
}
