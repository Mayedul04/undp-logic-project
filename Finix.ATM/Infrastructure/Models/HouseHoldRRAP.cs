﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldRRAP :Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public bool IsThereAnyCommunityInitiative { get; set; }
        public string WhatAreTheInitiatives { get; set; }
        public string WhatAreTheBenefitsReceivingByTheHH { get; set; }
        public bool IsThereAnyThreatForTheHHFromTheCommunityInitiative { get; set; }
        public string WhatAreTheThreats { get; set; }
        public string ImpactsOfCommunityLevelInitiativeOnHHThreatIdentified { get; set; }
        public string GapsForHHIntervention { get; set; }
        public string LivelihoodRiskReductionPlan { get; set; }
        public string LifeAndAssetLossReductionPlan { get; set; }
        public string PlanForSupportingNeighborhoodThreats { get; set; }
    }
}
