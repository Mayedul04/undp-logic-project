﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class InterventionBudget :Entity
    {
        public long? InterventionId { get; set; }
        [ForeignKey("InterventionId")]
        public virtual Interventions Intervention { get; set; }
        public string Details { get; set; }
        public long? FundSourceId { get; set; }
        [ForeignKey("FundSourceId")]
        public virtual SourceOfFund SourceOfFund { get; set; }
        public double FundAmount { get; set; }
    }
}
