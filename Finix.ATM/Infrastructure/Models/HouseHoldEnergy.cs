﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldEnergy:Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public virtual ICollection<HouseHoldEnergySources> HouseHoldEnergySources { get; set; }
        public decimal SubsistenceDomesticTypePercentage { get; set; }
        public decimal CommercialTypePercentage { get; set; }
        public decimal OthersTypePercentage { get; set; }
        
    }

    public class HouseHoldEnergySources : Entity
    {
        public long? HouseHoldEnergyId { get; set; }
        [ForeignKey("HouseHoldEnergyId")]
        public virtual HouseHoldEnergy HouseHoldEnergy { get; set; }
        public long? SourceId { get; set; }
        [ForeignKey("SourceId")]
        public virtual EnergySource EnergySource { get; set; }
    }
    public class HouseHoldCrops : Entity
    {
        public long? HouseHoldEnergyId { get; set; }
        [ForeignKey("HouseHoldEnergyId")]
        public virtual HouseHoldEnergy HouseHoldEnergy { get; set; }
        public long? CropId { get; set; }
        [ForeignKey("CropId")]
        public virtual Crops Crop { get; set; }
    }
}
