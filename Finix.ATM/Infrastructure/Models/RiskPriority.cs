﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
  public  class RiskPriority :Entity
    {
        public long RiskStatementId { get; set; }
        public virtual RiskStatement RiskStatement { get; set; }
        public long? UPId { get; set; }
        public virtual Location UP { get; set; }
        public int Likelihood { get; set; }
        public int Consequence { get; set; }
        public int RiskPercentage { get; set; }
        public int Rating { get; set; }
        public decimal PeopleAffected { get; set; }
        public decimal LocalEconomyAffected { get; set; }
        public PotentialToProperSolution PotentialToProperSolution { get; set; }
        public decimal PriorityPoint { get; set; }

    }
}
