﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class FoodProduction :Entity
    {
        public long? LocalEconomyId { get; set; }
        [ForeignKey("LocalEconomyId"), InverseProperty("FoodProductions")]
        public virtual LocalEconomyVulnerability LocalEconomy { get; set; }
        public long? LocationId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public FoodProductionType FoodProductionType { get; set; }
        public Boolean IsSelfSufficient { get; set; }
        public decimal IsExportable { get; set; }
        public decimal IsProductionDeficiency { get; set; }
        public string Vulnerabilities { get; set; }
    }
}
