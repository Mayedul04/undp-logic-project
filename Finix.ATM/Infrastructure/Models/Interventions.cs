﻿using Finix.ATM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class Interventions:Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public long? InterventionCategoryId { get; set; }
        [ForeignKey("InterventionCategoryId")]
        public virtual SchemeCategory InterventionCategory { get; set; }
        public string InterventionTitle { get; set; }
        public string ProposalRefNo { get; set; }
        public string HHHeadName { get; set; }
        public string ShortDetails { get; set; }
        public bool IsCapable { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsSupportNeeded { get; set; }
        public ICollection<InterventionSupport> SupportTypes { get; set; }
        public ICollection<InterventionHazard> InterventionHazards { get; set; }
        public ICollection<InterventionBudget> InterventionBudgets { get; set; }
        public ICollection<InterventionExpectedbenfits> InterventionExpectedbenfits { get; set; }
        public double CRFGrant { get; set; }
        public double EstimatedBudget { get; set; }

    }
    public class InterventionSupport : Entity
    {
        public long? InterventionId { get; set; }
        [ForeignKey("InterventionId")]
        public virtual Interventions Itervention { get; set; }
        public long? SupportTypeId { get; set; }
        [ForeignKey("SupportTypeId")]
        public virtual SupportType SupportType { get; set; }
    }

    public class InterventionHazard : Entity
    {
        public long? InterventionId { get; set; }
        [ForeignKey("InterventionId")]
        public virtual Interventions Itervention { get; set; }
        public long? HazardId { get; set; }
        [ForeignKey("HazardId")]
        public virtual HouseHoldHazards Hazards { get; set; }
    }

    public class InterventionExpectedbenfits : Entity
    {
        public long? InterventionId { get; set; }
        [ForeignKey("InterventionId")]
        public virtual Interventions Itervention { get; set; }
        public long? ElementTypeId { get; set; }
        [ForeignKey("ElementTypeId")]
        public virtual ElementType ElementType { get; set; }
        public string Expectation { get; set; }
        public string Benefit { get; set; }
    }
}
