﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class SensitivityAnalysisDetails : Entity
    {
        public long? SensitivityAnalysisId { get; set; }
        [ForeignKey("SensitivityAnalysisId"), InverseProperty("SensitivityAnalysisDetail")]
        public virtual SensitivityAnalysis SensitivityAnalysis { get; set; }
        public string MainElement { get; set; }
        public string Sensitivity1 { get; set; }
        public string Sensitivity2 { get; set; }
        public string Sensitivity3 { get; set; }

    }
}
