﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
  public  class IndicatorLog :Entity
    {
        public long? IndicatorId { get; set; }
        [ForeignKey("IndicatorId")]
        public virtual Indicators Indicator { get; set; }
        public decimal OldValue { get; set; }
        public decimal NewValue { get; set; }
    }
}
