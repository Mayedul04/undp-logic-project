﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class SchemeInformation :Entity
    {
        public long? LocationId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public string SchemeTitle { get; set; }
        public string Description { get; set; }
        public string SchemeNo { get; set; }
        public long SchemeNatureId { get; set; }
        public virtual SchemeNature SchemeNature { get; set; }
        public long SchemeCategoryId { get; set; }
        public virtual SchemeCategory SchemeCategory { get; set; }
        public long? RiskId { get; set; }
        [ForeignKey("RiskId")]
        public virtual RiskStatement RiskStatement { get; set; }
        public string ProposalRefNo { get; set; }
        public string  ImplementedBy { get; set; }
        public string Coverage { get; set; }
        public double EstimatedBudget { get; set; }
        public string GainedBenefits { get; set; }
        public string ExpectedBenefits { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ICollection<SchemeFunds> Funds { get; set; }
        public ICollection<SchemeHazard> SchemeHazards { get; set; }
        public ICollection<Expenditures> Expenses { get; set; }
        public ICollection<SchemeBeneficiaries> Beneficiaries { get; set; }
        public ICollection<SchemeEmployment> Employements { get; set; }
    }
    public class SchemeFunds : Entity
    {
        public long? SchemeId { get; set; }
        [ForeignKey("SchemeId")]
        public virtual SchemeInformation Scheme { get; set; }
        public long? SourceId { get; set; }
        [ForeignKey("SourceId")]
        public virtual SourceOfFund SourceOfFund { get; set; }
        public double Amount { get; set; }
    }

    public class SchemeHazard : Entity
    {
        public long? SchemeId { get; set; }
        [ForeignKey("SchemeId")]
        public virtual SchemeInformation Scheme { get; set; }
        public long? HazardId { get; set; }
        [ForeignKey("HazardId")]
        public virtual Hazard Hazards { get; set; }
    }
    public class Expenditures : Entity
    {
        public long? SchemeId { get; set; }
        [ForeignKey("SchemeId")]
        public virtual SchemeInformation Scheme { get; set; }
        public long? SourceId { get; set; }
        [ForeignKey("SourceId")]
        public virtual SourceOfFund SourceOfFund { get; set; }
        public double CostAmount { get; set; }
        public DateTime? CostDate { get; set; }
    }
    public class SchemeBeneficiaries : Entity
    {
        public long? SchemeId { get; set; }
        [ForeignKey("SchemeId")]
        public virtual SchemeInformation Scheme { get; set; }
        public string Beneficiary { get; set; }
        public int BeneficiaryCount { get; set; }
    }
    public class SchemeEmployment : Entity
    {
        public long? SchemeId { get; set; }
        [ForeignKey("SchemeId")]
        public virtual SchemeInformation Scheme { get; set; }
        public string PeopleType { get; set; }
        public int EmploymentCount { get; set; }
    }
}
