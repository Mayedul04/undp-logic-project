﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class IndicatorType :Entity
    {
        public IndicatorPrimaryGroup TypeGroup { get; set; }
        public string TypeName { get; set; }
        public decimal Weight { get; set; }
    }
}
