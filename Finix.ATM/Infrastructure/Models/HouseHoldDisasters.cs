﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldDisasters :Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public bool IsLatestAfftected { get; set; }
        public string LatestDisasterYear { get; set; }
        public bool HasOccupationAffected { get; set; }
        public bool AnybodyDied { get; set; }
        public bool IsClimateChanged { get; set; }
        public string Changes { get; set; }
        public string CauseofChange { get; set; }
        public string Damage { get; set; }
        public long? LatestHazardId { get; set; }
        [ForeignKey("LatestHazardId")]
        public virtual Hazard Hazard { get; set; }
    }
    
}
