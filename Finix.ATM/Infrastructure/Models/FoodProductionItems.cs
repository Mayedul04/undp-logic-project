﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class FoodProductionItems :Entity
    {
        public string ItemName { get; set; }
        public FoodProductionType Type { get; set; }
    }
}
