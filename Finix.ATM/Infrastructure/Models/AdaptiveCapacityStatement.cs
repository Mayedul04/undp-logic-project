﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class AdaptiveCapacityStatement:Entity
    {
        public long OptionID { get; set; }
        public virtual RiskReductionOption Option { get; set; }
        public bool ExistingKnowledge { get; set; }
        public bool ExistingSkill { get; set; }
        public bool ExistingTechnicalKnowledge { get; set; }
        public bool ExistingFacility { get; set; }
        public bool ExistingHumanResource { get; set; }
        public bool ExistingAttitute { get; set; }
        public bool ExistingInstitution { get; set; }
        public bool ExistingFinacialCapacity { get; set; }
        public bool ExistingFinancialInstrument { get; set; }
        public bool NewKnowledge { get; set; }
        public bool NewSkill { get; set; }
        public bool NewTechnicalKnowledge { get; set; }
        public bool NewFacility { get; set; }
        public bool NewHumanResource { get; set; }
        public bool NewAttitute { get; set; }
        public bool NewInstitution { get; set; }
        public bool NewFinacialCapacity { get; set; }
        public bool NewFinancialInstrument { get; set; }

    }
}
