﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class Indicators :Entity
    {
        public string IndicatorName { get; set; }
        public long Type { get; set; }
        [ForeignKey("Type")]
        public virtual IndicatorType IndicatorType { get; set; }
        public float Weight { get; set; }
        public decimal TargetYear1 { get; set; }
        public decimal TargetYear2 { get; set; }
        public decimal TargetYear3 { get; set; }
        public decimal TargetYear4 { get; set; }
    }
}
