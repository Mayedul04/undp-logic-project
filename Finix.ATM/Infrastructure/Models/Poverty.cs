﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
  public  class Poverty :Entity
    {
        public long? LocalEconomyId { get; set; }
        [ForeignKey("LocalEconomyId"), InverseProperty("Poverties")]
        public virtual LocalEconomyVulnerability LocalEconomy { get; set; }
        public long? LocationId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public PovertySituation PovertySituation { get; set; }
        public decimal PopulationPercentage { get; set; }
        
    }
}
