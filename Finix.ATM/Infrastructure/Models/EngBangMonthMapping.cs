﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class EngBangMonthMapping:Entity
    {
        public string EnglishName { get; set; }
        public string BanglaName { get; set; }
    }
}
