﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldLandAsset :Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public string HouseHoldNo { get; set; }
        public decimal CropLandSize { get; set; }
        public double CropLandValue { get; set; }
        public decimal WaterBodySize { get; set; }
        public double WaterBodyValue { get; set; }
        public ICollection<HouseHoldAgriTools> AgriculturalTools { get; set; }
        public double AgriToolsValue { get; set; }
        public int PoultryCount { get; set; }
        public int GoatCowCount { get; set; }
        public int OthersCount { get; set; }
    }

    public class HouseHoldAgriTools : Entity
    {
        public long? AssetId { get; set; }
        [ForeignKey("AssetId")]
        public virtual HouseHoldLandAsset HouseHoldAsset { get; set; }
        public long? ToolId { get; set; }
        [ForeignKey("ToolId")]
        public virtual AgriculturalTools AgriculturalTools { get; set; }
    }
}
