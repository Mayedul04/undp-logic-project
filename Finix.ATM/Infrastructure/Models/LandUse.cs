﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class LandUse:Entity
    {
        public string LandName { get; set; }
        public long? LocationId { get; set; }
        public virtual Location Location { get; set; }
        public long LandTypeID { get; set; }
        public virtual LandType LandType { get; set; }
        public Boolean IsAnyChange { get; set; }
        public string Changes { get; set; }
        public Boolean AnyHazardCreatedBy { get; set; }
        public long? CreatedHazardId { get; set; }
        public virtual Hazard CreatedHazard { get; set; }
        public Boolean IsNavigable { get; set; }
        public Boolean PreviouslyAffectedbyDisaster { get; set; }
        public long? DisasterId { get; set; }
        public virtual Hazard Disaster { get; set; }
        public Boolean IsTackenByThirdParty { get; set; }
        public string PresentUse { get; set; }
        public Boolean IsWaterAvailabeThroughOutYear { get; set; }
    }
}
