﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class HouseHoldBuilding:Entity
    {
        public long? HHId { get; set; }
        [ForeignKey("HHId")]
        public virtual HouseHoldInformation HouseHold { get; set; }
        public HouseRenatalType RentalType { get; set; }
        public decimal? LandAmount { get; set; }
        public double? LandValue { get; set; }
        public int AgeofHouse { get; set; }
        public HouseType? HouseType { get; set; }
        public int DistancetoUP { get; set; }
        public int DistancetoMarket { get; set; }
        public int? DistancetoShelter { get; set; }
        public bool? ProximitytoRiver { get; set; }
        public bool? IsInsideEmbankment { get; set; }
        public long? OutsideWallMadeof { get; set; }
        public long? FondationMadeof { get; set; }
        public long? RoofMadeof { get; set; }
        public int RoomCount { get; set; }
        public double MainBuildingSize { get; set; }
        public double MainBuildingValue { get; set; }
        public BuildingCondition BuildingCondition { get; set; }
        public BuildingCondition RoofCondition { get; set; }
        public ICollection<HouseHoldHazards> PronetoHazards { get; set; }
        public ICollection<HouseHoldOtherBuildings> OtherBuildings{ get; set; }
    }

    public class HouseHoldOtherBuildings : Entity
    {
        public long? HouseHoldBuildingId { get; set; }
        [ForeignKey("HouseHoldBuildingId"), InverseProperty("OtherBuildings")]
        public virtual HouseHoldBuilding HouseHoldBuilding { get; set; }
        public long? BuildingId { get; set; }
        [ForeignKey("BuildingId")]
        public virtual OtherBuilding OtherBuilding { get; set; }
    }

    public class HouseHoldHazards : Entity
    {
        public long? HouseHoldBuildingId { get; set; }
        [ForeignKey("HouseHoldBuildingId"), InverseProperty("PronetoHazards")]
        public virtual HouseHoldBuilding HouseHoldBuilding { get; set; }
        public long?  HazardId { get; set; }
        public virtual Hazard Hazard { get; set; }
    }
}
