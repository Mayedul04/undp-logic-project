﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
  public  class Element:Entity
    {
        public long ElementTypeId { get; set; }
        public virtual ElementType ElementType { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public long LocationId { get; set; }
        public virtual Location Location { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool IsAffectedBefore { get; set; }
        public long Year { get; set; }
        public long HazardId { get; set; }
        public virtual Hazard Hazard { get; set; }
        public decimal DamagePercentage { get; set; }
        public string PreviousPosition { get; set; }
        public string CurrentPosition { get; set; }
        public string PreviousType { get; set; }
        public string CurrentType { get; set; }
    }
}
