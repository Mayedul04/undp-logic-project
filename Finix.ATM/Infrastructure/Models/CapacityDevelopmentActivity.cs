﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class CapacityDevelopmentActivity :Entity
    {
        public string EventName { get; set; }
        public long? EventTypeId { get; set; }
        [ForeignKey("EventTypeId")]
        public virtual EventType EventType { get; set; }
        public double ActualCost { get; set; }
        public int Duration { get; set; }
        public string Venue { get; set; }
        public long? LocationId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public string ExecutedBy  { get; set; }
        public string OrganizedBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Renarks { get; set; }
        public ICollection<Participants> Participants { get; set; }
    }
    public class Participants : Entity
    {
        public long? CapacityDevelopmentActivityId { get; set; }
        [ForeignKey("CapacityDevelopmentActivityId")]
        public virtual CapacityDevelopmentActivity CapacityDevelopmentActivity { get; set; }
        public string PeopleType { get; set; }
        public int Count { get; set; }
    }
}
