﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
   public class CropCalender :Entity
    {
        public long CropId { get; set; }
        public virtual Crops Crop { get; set; }
        public long LocationId { get; set; }
        public virtual Location Location { get; set; }
        public long StartMonthId { get; set; }
        [ForeignKey("StartMonthId")]
        public virtual EngBangMonthMapping StartMonth { get; set; }
        public long EndMonthId { get; set; }
        [ForeignKey("EndMonthId")]
        public virtual EngBangMonthMapping EndMonth { get; set; }

    }
}
