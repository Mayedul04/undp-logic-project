﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.ATM.Infrastructure.Models
{
  public  class LandType : Entity
    {
        public string TypeName { get; set; }
        public Boolean IsWaterBody { get; set; }
    }
}
