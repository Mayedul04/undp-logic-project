﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });
   
    function RiskReductionVM() {

        var self = this;

        self.Id = ko.observable();
        self.RiskStatementId = ko.observable().extend({ required: true });
        self.RiskStatementName = ko.observable();

        self.UPId = ko.observable(); 
        self.UPName = ko.observable();
        self.OptionName = ko.observable('').extend({ required: true });
        self.IsSensitive = ko.observable(false);
        self.IsExposure = ko.observable(false);

        
        self.Statements = ko.observableArray([]);
        self.UPs = ko.observableArray([]);
        self.Options = ko.observableArray([]);

        self.Reset = function () {
            self.OptionName('');
            self.IsSensitive(false);
            self.IsExposure(false);
        }

        self.SaveData = function () {

            if (self.IsValid()) {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Risk/SaveRiskReductionOption',
                    data: ko.toJSON(this),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.getAllData();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getRiskStatements = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/GetTopStatements?upid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Statements(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getAllData = function () {
            if (self.UPId()>0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/Risk/getAllOptions?upid=' + self.UPId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        self.Options(data); //Put the response in ObservableArray
                    },
                    error: function (error) {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text('Please Select UP!');
            }
        }



        self.getLocations = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.UPs(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        

        self.UPId.subscribe(function () {
            if (self.UPId() > 0) {
                self.getRiskStatements();
                self.getAllData();
            }


        });


        self.EditData = function (data) {
            
            $.when(self.getLocations(4)).done(function () {
                self.UPId(data.LocationId);
                self.RiskStatementId(data.RiskStatementId);
            });

            self.OptionName(data.OptionName);
            self.IsExposure(data.IsExposure);
            self.IsSensitive(data.IsSensitive);
        }

        //self.PrintResult = function (item, event) {
        //    if (self.UPId() > 0)
        //        window.open('/ATM/Risk/GetRiskRatingReports?reportTypeId=PDF&upid=' + self.UPId());
        //    else {
        //        $('#successModal').modal('show');
        //        $('#successModalText').text('Please select UP!');
        //    }
        //}

        self.InitialValueLoad = function () {
            self.getLocations(4);
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });



    }

    var vm = new RiskReductionVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divRisk')[0]);



});