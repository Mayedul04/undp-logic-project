﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function RiskStatementVM() {

        var self = this;

        self.Id = ko.observable();
        self.ElementId = ko.observable().extend({ required: true });
        self.ElementName = ko.observable();
        
        self.ParentLocationId = ko.observable();
        self.ParentLocationName = ko.observable();
        self.SpecificAreaId = ko.observable();
        self.SpecificAreaName = ko.observable();
        self.AfftectedPeriod = ko.observable();
        self.Statement = ko.observable();
        self.HazardId = ko.observable();
        self.HazardName = ko.observable();
        self.Consequences = ko.observable();
        
       

        self.ParentLocations = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.Elements = ko.observableArray([]);
        self.Hazards = ko.observableArray([]);
        self.Statements = ko.observableArray([]);


        self.ReportTypeId = ko.observable();
        self.Link1 = ko.observable();
        //self.Banks = ko.observableArray([]);
        //self.Code = ko.observable();
        self.Link2 = ko.observable();
        self.Link3 = ko.observable();
        self.Title1 = ko.observable('PDF');
        self.Title2 = ko.observable('Excel');
        self.Title3 = ko.observable('Word');

        //self.reportCashBookData = ko.observableArray([]);
        

        self.Reset = function () {
                self.Id('');
                self.ElementId('');
                self.ParentLocationId('');
                self.SpecificAreaId('');
                self.HazardId('');
                self.AfftectedPeriod('');
                self.Statement('');
                self.Consequences('');
         }

        self.SaveData = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Risk/SaveRiskStatement',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                       // self.AllLocatins();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getElementsAtRisk = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/getElementsAtRisk?upid=' + self.ParentLocationId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    self.Elements(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getAllStatement = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/getAllStatements?upid=' + self.ParentLocationId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    self.Statements(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getElementInfo = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getElementById?id=' + self.ElementId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.SpecificAreaId(data.LocationId);
                    self.HazardId(data.HazardId)
                   // console.log(data);
                    //self.CropSeasons(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getHazardInfo = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getCalendarInfoByHazardId?hazardid=' + self.HazardId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.AfftectedPeriod(data.StartMonthName + ' to ' + data.EndMonthName);
                  
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getLocations = function (level,parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level==4)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.getHazards = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazards',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Hazards(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.ParentLocationId.subscribe(function () {
            if (self.ParentLocationId() > 0)
            {
                self.getElementsAtRisk();
                self.getLocations(5, self.ParentLocationId());
                self.getAllStatement();
            }
                
            
        });

        self.ElementId.subscribe(function () {
            if (self.ElementId() > 0)
                self.getElementInfo();
        });

        self.HazardId.subscribe(function () {
            if (self.HazardId() > 0)
                self.getHazardInfo();
        });

        
              
        self.EditData = function (data) {
            //console.log(data);
            self.Id(data.Id);

            $.when(self.getLocations(4)).done(function () {
                self.ParentLocationId(data.ParentLocationId);
                self.ElementId(data.ElementId);
            });
           
            self.SpecificAreaId(data.SpecificAreaId);
            self.HazardId(data.HazardId);
            self.AfftectedPeriod(data.AfftectedPeriod);
            self.Statement(data.Statement);
            self.Consequences(data.Consequences);
        }

        self.PrintResult = function (item, event) {
            if (self.ParentLocationId() > 0)
                window.open('/ATM/Risk/GetRiskStatementReports?reportTypeId=PDF&upid=' + self.ParentLocationId());
            else 
            {
            $('#successModal').modal('show');
            $('#successModalText').text('Please select UP!');
            }
        }

        //self.GetReport = function () {
        //    return "/ATM/Risk/GetRiskStatementReports?reportTypeId=PDF";
        //}
        

        //self.LoadData = function () {
        //    if (self.Id() > 0) {
        //        return $.ajax({
        //            type: "GET",
        //            url: '/ATM/AgriculturalInfo/getAgriculturalLandInfoById?id=' + self.Id(),
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            success: function (data) {
        //                console.log("Data :" + ko.toJSON(data));
                        
        //                $.when(self.getCropSeasons()).done(function () {
        //                    self.CropSeasonId(data.CropSeasonId);
        //                });
        //                $.when(self.getCrops()).done(function () {
        //                    self.CropId(data.CropId);
        //                });
        //                $.when(self.getLocations(4)).done(function () {
        //                    self.ParentLocationId(data.ParentLocationId);
        //                });
        //                $.when(self.getLocations(5)).done(function () {
        //                    self.LocationId(data.LocationId);
        //                });
        //                self.Production(data.Production);
        //                self.AffectedBefore(data.AffectedBefore);
                        
                        
        //                $.when(self.getHazards()).done(function () {
        //                    self.HazardId(data.HazardId);
        //                });
        //                self.AffectedYear(data.AffectedYear);
        //                self.DamagePercentage(data.DamagePercentage);
                        
        //            },
        //            error: function (error) {
        //                $('#successModal').modal('show');
        //                $('#successModalText').text(error.statusText);
        //            }
        //        });

        //    }
        //};


        //self.setUrl = ko.computed(function () {
        //    self.Link1('/ATM/Risk/GetRiskStatementReports?reportTypeId=PDF&fromDate=' + self.FromDate() + '&toDate=' + self.ToDate());
        //    self.Link2('/ATM/Risk/GetRiskStatementReports?reportTypeId=Excel&fromDate=' + self.FromDate() + '&toDate=' + self.ToDate());
        //    self.Link3('/ATM/Risk/GetRiskStatementReports?reportTypeId=Word&fromDate=' + self.FromDate() + '&toDate=' + self.ToDate());
        //});
     
        self.InitialValueLoad = function () {
            
            self.getLocations(4);
            self.getHazards();
            //self.getAllStatement();
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        

    }

    var vm = new RiskStatementVM();
    vm.InitialValueLoad();
    
    ko.applyBindings(vm, $('#divRisk')[0]);



});