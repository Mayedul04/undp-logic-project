﻿$(document).ready(function () {
    function VulnerableHHListVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);


        self.Edit = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '141_' + data.Id,
                Menu: 'Capacity Development Activity Information',
                Url: '/ATM/CapacityDevelopmentActivity/CapacityDevelopmentActivity',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }
    }

    var pavm = new VulnerableHHListVM();
    ko.applyBindings(pavm, document.getElementById("CDAList"));
});