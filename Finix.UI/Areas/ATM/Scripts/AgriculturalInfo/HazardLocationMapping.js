﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function HazardLocationVM() {

        var self = this;

        self.Id = ko.observable();
        self.HazardId = ko.observable('').extend({ required: true });
        self.HazardName = ko.observable();

        self.ParentLocationId = ko.observable();
        self.ParentLocationName = ko.observable();
        self.LocationId = ko.observable().extend({ required: true });
        self.LocationName = ko.observable();
        self.Consequence = ko.observable();
        
        self.StartMonthId = ko.observable();
        self.EndMonthId = ko.observable();
        self.StartMonthName = ko.observable();
        self.EndMonthName = ko.observable();
        
        self.Hazards = ko.observableArray([]);
        self.Months = ko.observableArray([]);
        self.ParentLocations = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.Mappings = ko.observableArray([]);
        

        self.Reset = function () {
                self.Id('');
                self.HazardId('');
                self.ParentLocationId('');
                self.LocationId('');
                self.StartMonthId('');
                self.EndMonthId('');
        }

        self.SaveData = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/AgriculturalInfo/SaveHazardCalendar',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.AllMappings();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getLocations = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level == 3)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Location Found");
                }
            });
        }

        self.getMonths = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getMonths',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Months(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Months Found");
                }
            });
        }

        self.ParentLocationId.subscribe(function () {
            if (self.ParentLocationId() > 0)
                self.getLocations(4, self.ParentLocationId());
            else
                self.Locations({});
        });

        self.LocationId.subscribe(function () {
            if (self.LocationId() > 0)
                self.AllMappings();
            else
                self.AllMappings();
        });

        self.getHazards = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazards',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Hazards(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Hazards Found");
                }
            });
        }
        
        self.AllMappings = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getHazardCalendar?locid=' + self.LocationId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Mappings(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Data Found");
                }
            });
        }
               

       
        self.editData = function (data) {
            self.Id(data.Id);
            $.when(self.getLocations(3)).done(function () {
                self.ParentLocationId(data.ParentLocationId);
            });
            $.when(self.getLocations(4),self.ParentLocationId()).done(function () {
                self.LocationId(data.LocationId);
            });
            
            $.when(self.getHazards()).done(function () {
                self.HazardId(data.HazardId);
            });
            $.when(self.getMonths()).done(function () {
                self.StartMonthId(data.StartMonthId);
                self.EndMonthId(data.EndMonthId);
            });
            self.Consequence(data.Consequence);
        }

       
        

        self.InitialValueLoad = function () {
            self.getLocations(3);
            self.getHazards();
            self.getMonths();
            self.AllMappings();
            //self.errors.showAllMessages(true);
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

    }

    var vm = new HazardLocationVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divHazard')[0]);



});