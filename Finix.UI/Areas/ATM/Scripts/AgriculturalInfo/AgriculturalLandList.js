﻿$(document).ready(function () {
    function AgrLandListVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        self.Wards = ko.observableArray(wards);
        self.LocationID = ko.observable(locationid);
        
        self.AddNew = function (data) {
            
            var menuInfo = {
                Id: '114_0' ,
                Menu: 'New Agricultural Land',
                Url: '/ATM/AgriculturalInfo/AgriculturalLandInformation'
                
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

       
        self.Edit = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '114_' + data.Id,
                Menu: 'Agricultural Land Information',
                Url: '/ATM/AgriculturalInfo/AgriculturalLandInformation',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }


    }

    var pavm = new AgrLandListVM();
    ko.applyBindings(pavm, document.getElementById("activeList"));
});