﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function HouseholdInfoVM() {

        var self = this;

        self.Id = ko.observable();
       

        self.RaisedAboveGround = ko.observable(false);
        

        

        

      

       


       

        self.InitialValueLoad = function () {
           
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });



    }

    var vm = new HouseholdInfoVM();
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divHouseholdInfo')[0]);



});