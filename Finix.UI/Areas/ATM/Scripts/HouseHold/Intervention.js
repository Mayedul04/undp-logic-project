﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });
    function InterventionExpectedbenfits() {
        var self = this;
        self.Id = ko.observable();
        self.InterventionId = ko.observable();
        self.ElementTypeId = ko.observable();
        self.ElementTypeName = ko.observable();
        self.Benefit = ko.observable();
        self.Expectation = ko.observable();

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.InterventionId(data.InterventionId);
            self.ElementTypeId(data.ElementTypeId);
            self.ElementTypeName(data.ElementTypeName);
            self.Benefit(data.Benefit);
            self.Expectation(data.Expectation);
        }


    }
    function InterventionImplementation() {
        var self = this;
        self.Id = ko.observable();
        self.InterventionId = ko.observable();
        self.CRFGrant = ko.observable();
        self.ActualCost = ko.observable(0);
        self.OwnContribution = ko.observable(0);
        self.ImplementedBy = ko.observable();
        self.StartDate = ko.observable(moment());
        self.EndDate = ko.observable(moment());

        self.Expenses = ko.observableArray([]);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.InterventionId(data.InterventionId);
            self.CRFGrant(data.CRFGrant);
            self.ActualCost(data.ActualCost);
            self.OwnContribution(data.OwnContribution);
            self.ImplementedBy(data.ImplementedBy);
            self.StartDate(moment(data.StartDate));
            self.EndDate(moment(data.EndDate));

        }

        function Expenditures() {
            var self = this;
            self.Id = ko.observable();
            self.ImplementationId = ko.observable();
            self.SourceId = ko.observable('');
            self.CostAmount = ko.observable(0);
            self.CostDate = ko.observable(moment());

            self.LoadData = function (data) {
                self.Id(data.Id);
                self.ImplementationId(data.ImplementationId);
                self.SourceId(data.SourceId);
                self.CostAmount(data.CostAmount);
                self.CostDate(data.CostDate);

            }


        }
    }
    function InterventionSupport() {
        var self = this;
        self.Id = ko.observable();
        self.InterventionId = ko.observable();
        self.SupportTypeId = ko.observable();
        self.Status = ko.observable(0);
               
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.InterventionId(data.InterventionId);
            self.SupportTypeId(data.SupportTypeId);
            self.Status(0);
       }

        
    }
    function InterventionHazard() {
        var self = this;
        self.Id = ko.observable();
        self.InterventionId = ko.observable();
        self.HazardId = ko.observable();
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.InterventionId(data.InterventionId);
            self.HazardId(data.HazardId);
            self.Status(0);
        }


    }
    function InterventionBudget() {
            var self = this;
            self.Id = ko.observable();
            self.InterventionId = ko.observable();
            self.InterventionTitle = ko.observable('');
            self.Details = ko.observable();
            self.FundSourceId = ko.observable();
            self.SourceOfFundName = ko.observable('');
            self.FundAmount = ko.observable();
               
            self.LoadData = function (data) {
                self.Id(data.Id);
                self.InterventionId(data.InterventionId);
                self.InterventionTitle(data.InterventionTitle);
                self.FundSourceId(data.FundSourceId);
                self.Details(data.Details);
                self.SourceOfFundName(data.SourceOfFundName);
                self.FundAmount(data.FundAmount);
           }

    }
    function AdaptionCapacityDevelopment() {
        var self = this;
        self.Id = ko.observable();
        self.SchemeId = ko.observable();
        self.GotAnyTraining = ko.observable(false);

        self.Trainings = ko.observableArray([]);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.SchemeId(data.SchemeId);
            self.GotAnyTraining(data.GotAnyTraining);
        }
    }
    function InterventionExpenditures() {
        var self = this;
        self.Id = ko.observable();
        self.ImplementationId = ko.observable();
        self.SourceId = ko.observable('');
        self.CostAmount = ko.observable(0);
        self.CostDate = ko.observable(moment());

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.ImplementationId(data.ImplementationId);
            self.SourceId(data.SourceId);
            self.CostAmount(data.CostAmount);
            self.CostDate(data.CostDate);

        }


    }
    function Trainings() {
        var self = this;
        self.Id = ko.observable();
        self.AdaptionCapacityId = ko.observable();
        self.TrainingName = ko.observable('');
        self.ProvidedBy = ko.observable('');
        self.Duration = ko.observable(0);
        self.StartDate = ko.observable(moment());
        self.EndDate = ko.observable(moment());

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.AdaptionCapacityId(data.AdaptionCapacityId);
            self.TrainingName(data.TrainingName);
            self.ProvidedBy(data.ProvidedBy);
            self.Duration(data.Duration);
            self.StartDate(moment(data.StartDate));
            self.EndDate(moment(data.StartDate));
        }


    }
    function InterventionVM() {

        var self = this;

        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable();
        self.InterventionTitle=ko.observable('').extend({ required: true, maxLength: "100" });

        self.UpName = ko.observable();
        self.WardName = ko.observable();
        self.LocationName = ko.observable();

        self.HHHeadName = ko.observable();
        self.HHDetails = ko.observable();
        self.InterventionCategoryId = ko.observable('').extend({ required: true });
        self.SchemeCategoryName = ko.observable();
        self.ProposalRefNo = ko.observable();
        self.ShortDetails = ko.observable();
        self.StartDate = ko.observable();
        self.EndDate = ko.observable();
        self.IsCapable = ko.observable(true);
        self.EstimatedBudget = ko.observable();
        self.IsSupportNeeded = ko.observable(false);
        self.CRFGrant = ko.observable();

        self.InterventionBudget = new InterventionBudget();
        self.InterventionImplementation = new InterventionImplementation();
        self.AdaptionCapacityDevelopment = new AdaptionCapacityDevelopment();

        self.InterventionBudgets = ko.observableArray([]);
        self.SourceOfFunds = ko.observableArray([]);
        self.SchemeCategories = ko.observableArray([]);
        self.ElementTypes=ko.observableArray([]);
        self.InterventionBenefits = ko.observableArray([]);
        self.InterventionExpenses = ko.observableArray([]);
        self.InterventionTrainings = ko.observableArray([]);
        self.InterventionExpectedbenfits = ko.observableArray([]);

        self.SupportTypeList = ko.observableArray([]);
        self.SelectedSupportTypes = ko.observableArray([]);
        self.SupportTypes = ko.observableArray([]);

        self.HazardList = ko.observableArray([]);
        self.SelectedHazards = ko.observableArray([]);
        self.InterventionHazards = ko.observableArray([]);

        

        self.addSource = function () {
            var budget = new InterventionBudget();
            
            self.InterventionBudgets.push(budget);
        }
        self.removedSources = ko.observableArray([]);
        self.removebudget = function (line) {
            if (line.Id() > 0)
                self.removedSources.push(line.Id());
            self.InterventionBudgets.remove(line);
        }

        self.addExpense = function () {
            var expense = new InterventionExpenditures();
            self.InterventionExpenses.push(expense);
        }
        self.removedExpenses = ko.observableArray([]);
        self.removeExpense = function (line) {
            if (line.Id() > 0)
                self.removedExpenses.push(line.Id());
            self.InterventionExpenses.remove(line);
        }

        self.addTraining = function () {
            var training = new Trainings();
            self.InterventionTrainings.push(training);
        }
        self.removedTrainings = ko.observableArray([]);
        self.removeTraining = function (line) {
            if (line.Id() > 0)
                self.removedTrainings.push(line.Id());
            self.InterventionTrainings.remove(line);
        }

        self.LoadElementypes = function () {
            
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/getSelectedElementTypes?hhid=' + self.HHId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.ElementTypes(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Element type found");
                }
            });
        }

        self.LoadBenefits = ko.computed(function () {
            if (self.InterventionBenefits().length == 0) {
                //self.InterventionBenefits.removeAll();
                for (var i = 0; i < self.ElementTypes().length ; i++) {
                    var item = new InterventionExpectedbenfits();
                    var temp = self.ElementTypes()[i];
                    item.ElementTypeId(temp.Id);
                    item.ElementTypeName(temp.TypeName);
                    self.InterventionBenefits.push(item);
                }
            }
        });

        self.LoadSourceOfFunds = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAllSourceOfFund',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.SourceOfFunds(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadSchemeCategory = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Scheme/getSchemeCategories',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.SchemeCategories(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadSupportTypes = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAllSupportType',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.SupportTypeList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadHazards = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/getHouseHoldHazardList?hhid=' + self.HHId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                   // console.log(ko.toJSON(data));
                    self.HazardList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadHouseHoldData = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/GetHouseHoldBasicData?hhid='+ self.HHId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.HouseHoldNo(data.HouseHoldNo);
                    //self.LocationId(data.WardId);
                    self.LocationName(data.LocationName);
                    self.HHDetails(data.Details);
                    $.each(data.Members, function (index, value) {
                        if (value.IsHeadPerson)
                            self.HHHeadName(value.MemberName);
                        return false;
                    });
                    //self.HHHeadName();
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.saveHouseHoldIntervention = function () {
            
            for (var i = 0; i < self.SelectedSupportTypes().length ; i++) {
                var item = new InterventionSupport();
                var temp = self.SelectedSupportTypes()[i];
                item.SupportTypeId(temp);
                item.Status(1);
                if (self.SelectedSupportTypes().length < 1)  //First Time
                    self.SupportTypes.push(item);
                else {
                    $.each(self.SupportTypes(), function (index, value) {
                        if (value.SupportTypeId() == temp) {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                     });
                    if (temp != 0)
                        self.SupportTypes.push(item);
                }
            }

            for (var i = 0; i < self.SelectedHazards().length ; i++) {
                var item = new InterventionHazard();
                var temp = self.SelectedHazards()[i];
                
                item.HazardId(temp);
                item.Status(1);
                if (self.SelectedHazards().length < 1)  //First Time
                    self.InterventionHazards.push(item);
                else {
                    $.each(self.InterventionHazards(), function (index, value) {
                        if (value.HazardId() == temp) {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                    });
                    if (temp != 0)
                        self.InterventionHazards.push(item);
                }
            }
            //debugger;
            var basic = this;
            if (self.SelectedSupportTypes().length > 1)
                 basic.SupportTypes(self.SupportTypes());
            if (self.SelectedHazards().length > 1)
                basic.InterventionHazards(self.InterventionHazards());
            if (self.InterventionBudgets().length > 1)
                basic.InterventionBudgets(self.InterventionBudgets());
           // console.log('Benfits:' + ko.toJSON(self.InterventionBenefits()));
            //if (self.InterventionBenefits().length > 1)
                basic.InterventionExpectedbenfits(self.InterventionBenefits());
           // basic.InterventionHazards(self.InterventionHazards());
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveIntervention',
                data: ko.toJSON(basic),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };
        self.saveImplementation = function () {
            
            var implementation = self.InterventionImplementation;
            implementation.InterventionId(self.Id());
            //if (self.InterventionExpenses().length > 1)
                implementation.Expenses(self.InterventionExpenses());
            
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveImplementation',
                data: ko.toJSON(implementation),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };
        self.saveACD = function () {

            var acd = self.AdaptionCapacityDevelopment;
            acd.InterventionId(self.Id());
            //if (self.InterventionTrainings().length > 1)
            acd.Trainings(self.InterventionTrainings());

            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveACD',
                data: ko.toJSON(acd),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };
        self.LoadInterventionData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetInterventionData?id=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            
                            self.HHId(data.HHId);
                            self.HouseHoldNo(data.HouseHoldNo);
                            self.UpName(data.UpName);
                            self.WardName(data.WardName);
                            self.LocationName(data.LocationName);
                            self.HHDetails(data.HHDetails);
                            self.HHHeadName(data.HHHeadName);
                            $.when(self.LoadSchemeCategory()).done( function(){
                                self.InterventionCategoryId(data.InterventionCategoryId);
                            });
                            self.SchemeCategoryName(data.SchemeCategoryName);
                            self.InterventionTitle(data.InterventionTitle);
                            self.IsCapable(data.IsCapable);
                            self.ProposalRefNo(data.ProposalRefNo);
                            self.ShortDetails(data.ShortDetails);
                            self.StartDate(moment(data.StartDate));
                            self.EndDate(moment(data.EndDate));
                            self.IsSupportNeeded(data.IsSupportNeeded);
                            self.CRFGrant(data.CRFGrant);
                            self.EstimatedBudget(data.EstimatedBudget);
                            self.InterventionImplementation.CRFGrant(data.CRFGrant);
                            
                            $.each(data.InterventionBudgets, function (index, value) {
                                var aDetail = new InterventionBudget();
                                if (typeof (value) != 'undefined') {
                                    aDetail.LoadData(value);
                                    //console.log("aDetail : " + ko.toJSON(value));
                                    self.InterventionBudgets.push(aDetail);
                                }
                            });
                           
                            $.when(self.LoadSupportTypes()).done(function () {
                                if (data.SupportTypes.length > 0) {
                                    $.each(data.SupportTypes, function (index, value) {
                                        if (typeof (value) != 'undefined') {
                                            var aDetail = new InterventionSupport();
                                            aDetail.LoadData(value);
                                            self.SupportTypes.push(aDetail);
                                            self.SelectedSupportTypes.push(value.SupportTypeId);
                                        }
                                    });
                                }
                            });
                            
                            $.when(self.LoadHazards()).done(function () {
                                if (data.InterventionHazards.length > 0) {
                                    $.each(data.InterventionHazards, function (index, value) {
                                        if (typeof (value) != 'undefined') {
                                            var aDetail = new InterventionHazard();
                                            //console.log("aDetail : " + ko.toJSON(value));
                                            aDetail.LoadData(value);
                                            self.InterventionHazards.push(aDetail);
                                            self.SelectedHazards.push(value.HazardId);
                                        }
                                    });
                                }
                            });
                            //console.log('HHID:' + self.HHId());
                            self.LoadElementypes();

                            $.each(data.InterventionExpectedbenfits, function (index, value) {
                                var aDetail = new InterventionExpectedbenfits();
                                if (typeof (value) != 'undefined') {
                                    aDetail.LoadData(value);
                                    //console.log("aDetail : " + ko.toJSON(value));
                                    self.InterventionBenefits.push(aDetail);
                                }
                            });
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }

        self.LoadImplementationData = function () {
           return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetImplementationData?iid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            self.InterventionImplementation.LoadData(data);
                            //self.CRFGrant(data.CRFGrant);
                            //self.EstimatedBudget(data.EstimatedBudget);
                            //self.InterventionImplementation.CRFGrant(data.CRFGrant);
                            //self.InterventionImplementation.ActualCost(0);
                            $.when(self.LoadSourceOfFunds()).done(function () {
                                $.each(data.Expenses, function (index, value) {
                                    var aDetail = new InterventionExpenditures();
                                    if (typeof (value) != 'undefined') {
                                        aDetail.LoadData(value);
                                        self.InterventionExpenses.push(aDetail);
                                    }
                                });
                            });
                            
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            
        }

        self.LoadADCData = function () {
            return $.ajax({
                type: "GET",
                url: "/ATM/Household/GetADCData?iid=" + self.Id(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log("Data : " + ko.toJSON(data));
                    if (data != null) {
                        self.AdaptionCapacityDevelopment.LoadData(data);
                        $.each(data.Trainings, function (index, value) {
                            var aDetail = new Trainings();
                            if (typeof (value) != 'undefined') {
                                aDetail.LoadData(value);
                                //console.log("aDetail : " + ko.toJSON(value));
                                self.InterventionTrainings.push(aDetail);
                            }
                        });
                    }
                },
                error: function (error) {

                    $('#successModal').modal('show');
                    $('#successModalText').text("There is no details data for this Survey!");
                }
            });

        }
        self.InitialValueLoad = function () {

            self.LoadSupportTypes();
            self.LoadSchemeCategory();
            self.LoadSourceOfFunds();
            
            if (self.Id() > 0)
            { 
                self.LoadInterventionData();
                self.LoadImplementationData();
                self.LoadADCData();
            }
            else  {
                self.LoadHouseHoldData();
                self.LoadHazards();
            }

        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });


        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new InterventionVM();

    vm.Id(vm.queryString("Id"));
    vm.HHId(vm.queryString("HHId"));
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divIntervention')[0]);



});