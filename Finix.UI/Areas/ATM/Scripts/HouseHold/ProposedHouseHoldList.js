﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    function HouseHoldListVM() {

        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        self.Wards = ko.observableArray(wards);
        self.LocationID = ko.observable(locationid);
        self.HouseHoldNo = ko.observable(searchString);
        self.Address = ko.observable();

        self.RRAP = function (data) {
            var parameters = [{
                Name: 'HHId',
                Value: data.HHId
            }];
            var menuInfo = {
                Id: '155_' + data.HHId,
                Menu: 'House Hold RRAP',
                Url: '/ATM/Household/RiskReductionActionPlan',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.Proposal = function (data) {
            var parameters = [{
                Name: 'HHId',
                Value: data.HHId
            }];
            var menuInfo = {
                Id: '151_' + data.HHId,
                Menu: 'House Hold Proposal',
                Url: '/ATM/Household/HouseholdProposal',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.Intervention = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.InterventionId
            }];
            var menuInfo = {
                Id: '152_' + data.InterventionId,
                Menu: 'Intervention',
                Url: '/ATM/Household/HouseholdProposal',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.Disburse = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.InterventionId
            }];
            var menuInfo = {
                Id: '153_' + data.InterventionId,
                Menu: 'House Hold Disbursement',
                Url: '/ATM/Household/HouseholdDisburse',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.Scheme = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.InterventionId
            }];
            var menuInfo = {
                Id: '154_' + data.InterventionId,
                Menu: 'HH Scheme Implementation',
                Url: '/ATM/Scheme/SchemeImplementationCRF',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

       
    }

    var vm = new HouseHoldListVM();
    ko.applyBindings(vm, $('#divHH')[0]);



});