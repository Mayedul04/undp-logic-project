﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function Member() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.MemberName = ko.observable('');
        self.Gender = ko.observable();
        self.Age = ko.observable();
        self.OccupationId = ko.observable();
        self.Education = ko.observable();
        self.MaritalStatus = ko.observable();
        self.PrimaryIncome = ko.observable();
        self.SecondaryOccupation = ko.observable();
        self.SecondaryIncome = ko.observable();
        self.RelationtoHH = ko.observable();
        self.NID = ko.observable();
        self.IsHeadPerson = ko.observable(false);
        self.IsSafetyNet = ko.observable(false);
        self.IsDisable = ko.observable(false);

        //self.Genders = ko.observableArray([]);
       
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.MemberName(data.MemberName);
            self.Gender(data.Gender);
            self.Age(data.Age);
            self.OccupationId(data.OccupationId);
            self.Education(data.Education);
            self.MaritalStatus(data.MaritalStatus);
            self.PrimaryIncome(data.PrimaryIncome);
            self.SecondaryOccupation(data.SecondaryOccupation);
            self.SecondaryIncome(data.SecondaryIncome);
            self.RelationtoHH(data.RelationtoHH);
            self.NID(data.NID);
            self.IsSafetyNet(data.IsSafetyNet);
            self.IsHeadPerson(data.IsHeadPerson);
            self.IsDisable(data.IsDisable);
        }

        
    }
    function SurveyAdministration() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.Admininstrator = ko.observable();
        self.InformationProvider = ko.observable();
        self.SurveyConductor = ko.observable();
        self.SurveyDate = ko.observable(moment());
        self.SurveyDateTxt = ko.observable();
        
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.Admininstrator(data.Admininstrator);
            self.InformationProvider(data.InformationProvider);
            self.SurveyConductor(data.SurveyConductor);
            self.SurveyDate(moment(data.SurveyDate));
            self.SurveyDateTxt(data.SurveyDateTxt);
            
        }
 
    }
    function HouseHoldHazards() {
        var self = this;
        self.Id = ko.observable();
        self.HouseHoldBuildingId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.HazardId = ko.observable();
        self.HazardName = ko.observable('');
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HouseHoldBuildingId(data.HouseHoldBuildingId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.HazardId(data.HazardId);
            self.HazardName(data.HazardName);
            self.Status(0);
            //if (data.Status == 0)
            //    self.Status(false);
            //else
            //    self.Status(true);
        }

    }
    function HouseHoldOtherBuildings() {
        var self = this;
        self.Id = ko.observable();
        self.HouseHoldBuildingId = ko.observable();
        self.BuildingId = ko.observable();
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HouseHoldBuildingId(data.HouseHoldBuildingId);
            self.BuildingId(data.BuildingId);
            
            self.Status(0);
        }

    }
    function HouseHoldBuilding() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.RentalType = ko.observable();
        self.LandAmount = ko.observable();
        self.LandValue = ko.observable();
        self.AgeofHouse = ko.observable();
        self.RoomCount = ko.observable();
        self.HouseType = ko.observable();
        self.DistancetoUP = ko.observable();
        self.DistancetoMarket = ko.observable();
        self.DistancetoShelter = ko.observable();
        self.ProximitytoRiver = ko.observable();
        self.IsInsideEmbankment = ko.observable();
        self.OutsideWallMadeof = ko.observable();
        self.FondationMadeof = ko.observable();
        self.RoofMadeof = ko.observable();
        self.MainBuildingSize = ko.observable();
        self.MainBuildingValue = ko.observable();
        self.BuildingCondition = ko.observable();
        self.IsPronetoHazard = ko.observable(false);
        self.RoofCondition = ko.observable();
        self.PronetoHazards = ko.observableArray([]);
        self.OtherBuildings = ko.observableArray([]);
        
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.RentalType(data.RentalType);
            self.HouseType(data.HouseType);
            self.LandAmount(data.LandAmount);
            self.LandValue(data.LandValue);
            self.AgeofHouse(data.AgeofHouse);
            self.RoomCount(data.RoomCount);
            self.DistancetoUP(data.DistancetoUP);
            self.DistancetoMarket(data.DistancetoMarket);
            self.DistancetoShelter(data.DistancetoShelter);
            self.ProximitytoRiver(data.ProximitytoRiver);
            self.IsInsideEmbankment(data.IsInsideEmbankment);
            self.OutsideWallMadeof(data.OutsideWallMadeof);
            self.FondationMadeof(data.FondationMadeof);
            self.RoofMadeof(data.RoofMadeof);
            self.MainBuildingSize(data.MainBuildingSize);
            self.MainBuildingValue(data.MainBuildingValue);
            self.BuildingCondition(data.BuildingCondition);
            self.RoofCondition(data.RoofCondition);
            if (data.PronetoHazards.length > 0)
                self.IsPronetoHazard(true);
            else
                self.IsPronetoHazard(false);
            
        }

    }
    function HouseHoldMainFoods() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.FoodName = ko.observable();
        self.IsGrownFood = ko.observable(true);
        self.IsPurchasedFood = ko.observable(false);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.FoodName(data.FoodName);
            self.IsGrownFood(data.IsGrownFood);
            self.IsPurchasedFood(data.IsPurchasedFood);
        }

        self.IsGrownFood.subscribe(function () {
            self.IsPurchasedFood(!self.IsGrownFood());
        });

        self.IsPurchasedFood.subscribe(function () {
            self.IsGrownFood(!self.IsPurchasedFood());
        });
    }
    function HouseHoldFoodShortage() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable();
        self.ShortageName = ko.observable();
        self.CausedBy = ko.observable();
        self.ShortageLength = ko.observable();
        self.FoodType = ko.observable();
        self.ShortageDate = ko.observable();
        self.ActionTaken = ko.observable();

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.ShortageName(data.ShortageName);
            self.ShortageLength(data.ShortageLength);
            self.CausedBy(data.CausedBy);
            self.ShortageDate(moment(data.ShortageDate));
            self.FoodType(data.FoodType);
            self.ActionTaken(data.ActionTaken);

        }

    }
    function HouseHoldFoods() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.HadShortage = ko.observable(false);
        self.PurchasedFood = ko.observable();
        self.IsStocked = ko.observable(false);
        self.GrownFood = ko.observable();
        self.StockDuration = ko.observable();
        self.Firewood = ko.observable();
        self.Electricity = ko.observable();
        self.Gas = ko.observable();
        self.Others = ko.observable();
        
        self.MainFoods = ko.observableArray([]);
        self.FoodShortages = ko.observableArray([]);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.HadShortage(data.HadShortage);
            self.PurchasedFood(data.PurchasedFood);
            self.IsStocked(data.IsStocked);
            self.GrownFood(data.GrownFood);
            self.StockDuration(data.StockDuration);
            self.Firewood(data.Firewood);
            self.Electricity(data.Electricity);
            self.Gas(data.Gas);
            self.Others(data.Others);

        }

    }
    function HouseHoldWaterSources() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.WaterSourceName = ko.observable();
        self.SourceId = ko.observable();
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.WaterSourceName(data.WaterSourceName);
            self.SourceId(data.SourceId);
            self.Status = ko.observable(0);
        }

    }
    function HouseHoldHealth() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.AnnualHealthExpenditure = ko.observable();
        self.SufferedLastThreeMonth = ko.observable(false);
        self.DiseaseName = ko.observable();
        self.IsWaterSafe = ko.observable(true);
        self.HasLatrine = ko.observable(true);
        self.LatrineType = ko.observable();
        self.MainWaterSources = ko.observableArray([]);
        

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.AnnualHealthExpenditure(data.AnnualHealthExpenditure);
            self.SufferedLastThreeMonth(data.SufferedLastThreeMonth);
            self.DiseaseName(data.DiseaseName);
            self.IsWaterSafe(data.IsWaterSafe);
            self.HasLatrine(data.HasLatrine);
            self.LatrineType(data.LatrineType);
        }

    }
    function HouseHoldEnergySource() {
        var self = this;
        self.Id = ko.observable();
        self.HouseHoldEnergyId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.EnergySourceName = ko.observable();
        self.SourceId = ko.observable();
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HouseHoldEnergyId(data.HouseHoldEnergyId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.EnergySourceName(data.EnergySourceName);
            self.SourceId(data.SourceId);
            self.Status = ko.observable(0);
        }

    }
    function HouseHoldCrops() {
        var self = this;
        self.Id = ko.observable();
        self.HouseHoldEnergyId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.CropsName = ko.observable();
        self.CropId = ko.observable();
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HouseHoldEnergyId(data.HouseHoldEnergyId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.CropsName(data.CropsName);
            self.CropId(data.CropId);
            self.Status = ko.observable(0);
        }

    }
    function HouseHoldEnergy() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.SubsistenceDomesticTypePercentage = ko.observable();
        self.CommercialTypePercentage = ko.observable();
        self.OthersTypePercentage = ko.observable();

        self.HouseHoldEnergySources = ko.observableArray([]);
        
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.SubsistenceDomesticTypePercentage(data.SubsistenceDomesticTypePercentage);
            self.CommercialTypePercentage(data.CommercialTypePercentage);
            self.OthersTypePercentage(data.OthersTypePercentage);
        }

    }
    function HouseHoldAgriTools() {
        var self = this;
        self.Id = ko.observable();
        self.AssetId = ko.observable();
        self.ToolId = ko.observable();
        self.AgriculturalToolName = ko.observable();
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.AssetId(data.AssetId);
            self.ToolId(data.ToolId);
            self.AgriculturalToolName(data.AgriculturalToolName);
            self.Status = ko.observable(0);
        }

    }
    function HouseHoldLandAsset() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.CropLandSize = ko.observable();
        self.CropLandValue = ko.observable();
        self.WaterBodySize = ko.observable();
        self.WaterBodyValue = ko.observable();
        self.AgriToolsValue = ko.observable();
        self.PoultryCount = ko.observable();
        self.GoatCowCount = ko.observable();
        self.OthersCount = ko.observable();

        self.AgriculturalTools = ko.observableArray([]);
        
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.CropLandSize(data.CropLandSize);
            self.CropLandValue(data.CropLandValue);
            self.WaterBodySize(data.WaterBodySize);
            self.WaterBodyValue(data.WaterBodyValue);
            self.AgriToolsValue(data.AgriToolsValue);
            self.PoultryCount(data.WaterBodySize);
            self.GoatCowCount(data.GoatCowCount);
            self.OthersCount(data.OthersCount);
        }

    }
    function HouseHoldDisasters() {
        var self = this;
        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable('');
        self.IsLatestAfftected = ko.observable(false);
        self.LatestDisasterYear = ko.observable();
        self.HasOccupationAffected = ko.observable(false);
        self.AnybodyDied = ko.observable();
        self.IsClimateChanged = ko.observable(true);
        self.Changes = ko.observable();
        self.CauseofChange = ko.observable();
        self.Damage = ko.observable();
        self.LatestHazardId = ko.observable();

        self.LoadData = function (data) {
            
            self.Id(data.Id);
            self.HHId(data.HHId);
            self.HouseHoldNo(data.HouseHoldNo);
            self.IsLatestAfftected(data.IsLatestAfftected);
            self.LatestDisasterYear(data.LatestDisasterYear);
            self.HasOccupationAffected(data.HasOccupationAffected);
            self.AnybodyDied(data.AnybodyDied);
            self.IsClimateChanged(data.IsClimateChanged);
            self.Changes(data.Changes);
            self.CauseofChange(data.CauseofChange);
            self.Damage(data.Damage);
            self.LatestHazardId(data.LatestHazardId);
        }

    }
    function HouseHoldSurveyVM() {

        var self = this;

        self.Id = ko.observable();
        self.HouseHoldNo = ko.observable('').extend({ required: true, maxLength: "100" });

        self.UpId = ko.observable();
        self.UpName = ko.observable();
        self.ParentLocationId = ko.observable();
        self.ParentLocationName = ko.observable();
        self.VillageId = ko.observable();
        self.LocationName = ko.observable();

        self.Address = ko.observable();
        self.OccupantNumber = ko.observable();
        self.MonthlyAvgIncome = ko.observable();
        self.LivingPeriod = ko.observable();
        self.IsLivingLifeTime = ko.observable();
        self.HHeadReligion=ko.observable();

        self.HasBankAccount = ko.observable(false);
        self.BankName = ko.observable();
        self.AccountNumber = ko.observable();
        self.HasMobileNumber = ko.observable(true);
        self.HHeadMobileNo = ko.observable();

        self.HasSavings = ko.observable(false);
        self.SavingAmount = ko.observable();
        self.IsNGOMember = ko.observable(false);
        self.IsEthnicMinority = ko.observable(false);
        self.EthnicMinorityId = ko.observable();
        self.HasAdolsentMother = ko.observable(false);
        self.WentforWork = ko.observable(true);
        self.OutsideHomeDistrict = ko.observable(false);

        self.SurveyAdministration = new SurveyAdministration();
        self.HouseHoldBuilding = new HouseHoldBuilding();
        self.HouseHoldFood = new HouseHoldFoods();
        self.HouseHoldHealth = new HouseHoldHealth();
        self.HouseHoldEnergy = new HouseHoldEnergy();
        self.HouseHoldAsset = new HouseHoldLandAsset();
        self.Member = new Member();
        self.HouseHoldDisaster = new HouseHoldDisasters();

        self.Genders = ko.observableArray([]);
        self.Minorities = ko.observableArray([]);
        self.Occupations = ko.observableArray([]);
        self.MaritalStatusList = ko.observableArray([]);
        self.Relations = ko.observableArray([]);
        self.Religions = ko.observableArray([]);
        self.Members = ko.observableArray([]);
        self.Materials = ko.observableArray([]);
        self.HouseTypes = ko.observableArray([]);
        self.LatrineTypes = ko.observableArray([]);

        self.BuildingList = ko.observableArray([]);
        self.SelectedBuildings = ko.observableArray([]);
        self.OtherBuildings = ko.observableArray([]);

        self.HazardList = ko.observableArray([]);
        self.PronetoHazards = ko.observableArray([]);
        self.Hazards = ko.observableArray([]);

        self.BuildingParts = ko.observableArray([]);
        self.HouseHoldMadeInfo = ko.observableArray([]);
        self.ParentLocations = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.UPs = ko.observableArray([]);

        self.MainFoods = ko.observableArray([]);
        ///self.FoodShortages = ko.observableArray([]);
        self.HHFoodShortages = ko.observableArray([]);

        self.WaterSourceList = ko.observableArray([]);
        self.SelectedWaterSources = ko.observableArray([]);
        self.HHWaterSources = ko.observableArray([]);

        self.AgriToolList = ko.observableArray([]);
        self.SelectedTools = ko.observableArray([]);
        self.HHAgriTools = ko.observableArray([]);

        self.EnegrySourceList = ko.observableArray([]);
        self.SelectedEnergySources = ko.observableArray([]);
        self.HHEnergySources = ko.observableArray([]);

        //self.CropList = ko.observableArray([]);
        //self.SelectedCrops = ko.observableArray([]);
        //self.HHCrops = ko.observableArray([]);

        self.UpId.subscribe(function () {
            if (self.UpId() > 0)
                self.getLocations(5, self.UpId());
            else
                self.ParentLocations({});
        });

        self.ParentLocationId.subscribe(function () {
            if (self.ParentLocationId() > 0)
                self.getLocations(6, self.ParentLocationId());
            else
                self.Locations({});
        });

        self.Reset = function () {
            self.Id('');
            self.HouseHoldNo('');
            self.WardId('');
            self.LocationName('');
            self.Address('');
            self.OccupantNumber('');
            self.MonthlyAvgIncome('');
            self.LivingPeriod('');
            self.IsLivingLifeTime(false);
        }

        

        self.addFood = function () {
            var food = new HouseHoldMainFoods();
            food.HHId(self.Id());
            food.HouseHoldNo(self.HouseHoldNo());
            self.MainFoods.push(food);
        }
        self.removedFoods = ko.observableArray([]);
        self.removeFood = function (line) {
            if (line.Id() > 0)
                self.removedFoods.push(line.Id());
            self.MainFoods.remove(line);
        }

        self.addFoodShortage = function () {
            var shortage = new HouseHoldFoodShortage();
            shortage.HHId(self.Id());
            shortage.HouseHoldNo(self.HouseHoldNo());
            var name = "Shortage " + (self.HHFoodShortages().length + 1);
            shortage.ShortageName(name);
            self.HHFoodShortages.push(shortage);
        }
        self.removedShorteages = ko.observableArray([]);
        self.removeFoodShortage = function (line) {
            if (line.Id() > 0)
                self.removedFoods.push(line.Id());
            self.HHFoodShortages.remove(line);
        }

        self.LoadOccupations = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Vulnerability/getEmployments',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Occupations(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadGenders = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getGenderList',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Genders(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadRelations = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getRelations',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Relations(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadReligions = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getReligions',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Religions(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadMaritalStatusList = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getMaritalStatusList',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.MaritalStatusList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadEthnicMionorities = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAllEthnicMinority',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Minorities(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadMaterials = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getBuilgingMaterials',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Materials(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadHouseTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getHouseTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.HouseTypes(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadHazards = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazards',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.HazardList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadBuildings = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getOtherBuildings',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.BuildingList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadAgriTools = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAgriTools',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.AgriToolList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadLatrines = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getLatrineTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.LatrineTypes(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadWaterSources = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAllWaterSource',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.WaterSourceList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadEnergySources = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Household/getEnergySources',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.EnegrySourceList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        //self.LoadCrops = function () {
        //    return $.ajax({
        //        type: "GET",
        //        url: '/ATM/AgriculturalInfo/getCrops',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (data) {
        //            self.CropList(data); //Put the response in ObservableArray
        //        },
        //        error: function (error) {
        //            alert(error.status + "<--and--> " + error.statusText);
        //        }
        //    });
        //}

        self.getBuildingParts = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/getBuildingParts',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.BuildingParts(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadBuildingPartsInfo = ko.computed(function () {
            if (self.HouseHoldMadeInfo().Count==0)
            {
                self.HouseHoldMadeInfo.removeAll();
                for (var i = 0; i < self.BuildingParts().length ; i++) {
                    var item = new HouseHoldBuildingMade();
                    var temp = self.BuildingParts()[i];

                    item.BuildingPartId(temp.Id);
                    item.BuildingPartName(temp.PartName);
                    // item.HHId(self.Id);
                    item.HouseHoldNo(self.HouseHoldNo);
                    self.HouseHoldMadeInfo.push(item);

                }
            }
            
        });

        //self.LoadFoodShortages = ko.computed(function () {
        //    //if (self.FoodShortages().Count > 0) {
        //    for (var i = 0; i < 3 ; i++) {
        //        var item = new HouseHoldFoodShortage();
        //        var name = "Shortage " + (i + 1);
        //        item.ShortageName(name);
        //        item.HHId(self.Id);
        //        item.HouseHoldNo(self.HouseHoldNo);
        //        self.FoodShortages.push(item);
        //        }
        //   // }
        //});

        self.getLocations = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level == 4)
                        self.UPs(data);
                    else if (level == 5)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            }); 
        }

        self.saveHouseHoldBasicInfo = function () {
            
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHBasicInfo',
                data: ko.toJSON(this),
                contentType: "application/json",
                success: function (data) {

                    console.log(data.Id);
                    self.Id(data.Id);
                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);

                    self.GetAllDiv();
                    // self.CodeVisible(true);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };
        self.saveHouseHoldMemberInfo = function () {
            
            var member = self.Member;
            member.HHId = self.Id();
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHMember',
                data: ko.toJSON(member),
                contentType: "application/json",
                success: function (data) {
                                  
                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                    self.LoadAllmember();
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.saveHouseHoldSurveyInfo = function () {
            var survey = self.SurveyAdministration;
            survey.HHId(self.Id());
            survey.HouseHoldNo(self.HouseHoldNo());
            
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHSurveyInfo',
                data: ko.toJSON(survey),
                contentType: "application/json",
                success: function (data) {
                    
                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);

                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.saveHouseHoldBuildingInfo = function () {
            
            for (var i = 0; i < self.Hazards().length ; i++) {
                var item = new HouseHoldHazards();
                
                var temp = self.Hazards()[i];
                item.HazardId(temp);
                item.HouseHoldNo(self.HouseHoldNo);
                item.Status(1);
                if (self.Hazards().length < 1)  //First Time
                    self.PronetoHazards.push(item);
                else
                {
                    $.each(self.PronetoHazards(), function (index, value) {
                       if (value.HazardId() == temp)
                        {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                    });
                    if (temp != 0)
                       self.PronetoHazards.push(item);
                }
            }
            
            for (var i = 0; i < self.SelectedBuildings().length ; i++) {
               
                var item = new HouseHoldOtherBuildings();
                var temp = self.SelectedBuildings()[i];
                item.BuildingId(temp);
                item.Status(1);
                if (self.SelectedBuildings().length < 1)  //First Time
                    self.OtherBuildings.push(item);
                else {
                    $.each(self.OtherBuildings(), function (index, value) {
                        if (value.BuildingId() == temp) {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                    });
                    if (temp != 0)
                        self.OtherBuildings.push(item);
                }
            }

            var building = self.HouseHoldBuilding;
            building.HHId(self.Id());
            building.HouseHoldNo(self.HouseHoldNo());
            building.OtherBuildings(self.OtherBuildings());
            building.PronetoHazards(self.PronetoHazards());
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHBuildingInfo',
                data: ko.toJSON(building),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.saveHouseHoldFoodInfo = function () {
            
            var food = self.HouseHoldFood;
            food.HHId(self.Id());
            food.HouseHoldNo(self.HouseHoldNo());
            food.MainFoods(self.MainFoods());
            food.FoodShortages(self.HHFoodShortages());
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHFoodInfo',
                data: ko.toJSON(food),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.saveHouseHoldLandAssets = function () {

            for (var i = 0; i < self.SelectedTools().length ; i++) {
                var item = new HouseHoldAgriTools();
                var temp = self.SelectedTools()[i];
                item.ToolId(temp);
                item.Status(1);
                if (self.SelectedTools().length < 1)  //First Time
                    self.HHAgriTools.push(item);
                else {
                    $.each(self.HHAgriTools(), function (index, value) {
                        if (value.ToolId() == temp) {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                    });
                    if (temp != 0)
                        self.HHAgriTools.push(item);
                }

            }

            var assets = self.HouseHoldAsset;
            assets.HHId(self.Id());
            assets.HouseHoldNo(self.HouseHoldNo());
            assets.AgriculturalTools(self.HHAgriTools());


            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHLandAsset',
                data: ko.toJSON(assets),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });
        };

        self.saveHouseHoldHealthInfo = function () {

            for (var i = 0; i < self.SelectedWaterSources().length ; i++) {
                var item = new HouseHoldWaterSources();
                var temp = self.SelectedWaterSources()[i];
                item.SourceId(temp);
                item.Status(1);
                if (self.SelectedWaterSources().length < 1)  //First Time
                    self.HHWaterSources.push(item);
                else {
                    $.each(self.HHWaterSources(), function (index, value) {
                         if (value.SourceId() == temp) {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                     });
                    if (temp != 0)
                        self.HHWaterSources.push(item);
                }

            }
            

            var health = self.HouseHoldHealth;
            health.HHId(self.Id());
            health.HouseHoldNo(self.HouseHoldNo());
            health.MainWaterSources(self.HHWaterSources());
            
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHHealthInfo',
                data: ko.toJSON(health),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.saveHouseHoldDisasterInfo = function () {

            var disaster = self.HouseHoldDisaster;
            disaster.HHId = self.Id();
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHDisaster',
                data: ko.toJSON(disaster),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                    
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.saveHouseHoldEnergyInfo = function () {

            for (var i = 0; i < self.SelectedEnergySources().length ; i++) {
                var item = new HouseHoldEnergySource();
                var temp = self.SelectedEnergySources()[i];
                item.SourceId(temp);
                item.Status(1);
                if (self.SelectedEnergySources().length < 1)  //First Time
                    self.HHEnergySources.push(item);
                else {
                    $.each(self.HHEnergySources(), function (index, value) {
                        if (value.SourceId() == temp) {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                    });
                    if (temp != 0)
                        self.HHEnergySources.push(item);
                }

            }

            var energy = self.HouseHoldEnergy;
            energy.HHId(self.Id());
            energy.HouseHoldNo(self.HouseHoldNo());
            energy.HouseHoldEnergySources(self.HHEnergySources());
            

            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveHHEnergyInfo',
                data: ko.toJSON(energy),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });
        };
        
        self.LoadBasicData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldBasicData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            self.Id(data.Id);
                            $.when(self.getLocations(4)).done(function () {
                                self.UpId(data.UpId);
                            });
                            $.when(self.getLocations(5)).done(function () {
                                self.ParentLocationId(data.ParentLocationId);
                            });
                            $.when(self.getLocations(6, self.ParentLocationId())).done(function () {
                                self.VillageId(data.VillageId);
                            });
                            
                            self.HouseHoldNo(data.HouseHoldNo);
                            self.Address(data.Address);
                            self.MonthlyAvgIncome(data.MonthlyAvgIncome);
                            self.OccupantNumber(data.OccupantNumber);
                            self.LivingPeriod(data.LivingPeriod);
                            self.IsLivingLifeTime(data.IsLivingLifeTime);
                            self.BankName(data.BankName);
                            self.AccountNumber(data.AccountNumber);
                            self.HasBankAccount(data.HasBankAccount);
                            self.HHeadMobileNo(data.HHeadMobileNo);
                            self.HasMobileNumber(data.HasMobileNumber);
                            self.IsLivingLifeTime(data.IsLivingLifeTime);
                            
                            self.HasSavings(data.HasSavings);
                            self.SavingAmount(data.SavingAmount);
                            self.IsNGOMember(data.IsNGOMember);
                            self.IsEthnicMinority(data.IsEthnicMinority);
                            $.when(self.LoadEthnicMionorities()).done(function () {
                                self.EthnicMinorityId(data.EthnicMinorityId);
                            });
                            $.when(self.LoadReligions()).done(function () {
                                self.HHeadReligion(data.HHeadReligion);
                            });
                            self.HasAdolsentMother(data.HasAdolsentMother);
                            self.WentforWork(data.WentforWork);
                            self.OutsideHomeDistrict(data.OutsideHomeDistrict);
                            
                            
                        }
                    },
                    error: function (error) {
                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Local!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a Location!");
            }

        }

        self.GetAllDiv = function () {
            if (self.Id() > 0) {
                $("#Administration :input").attr("disabled", false);
                $("#BuildingInfo :input").attr("disabled", false);
                $("#Food :input").attr("disabled", false);
                $("#Health :input").attr("disabled", false);
                $("#EnergyAndFarming :input").attr("disabled", false);

            }
            else {
                $("#Administration :input").attr("disabled", true);
                $("#BuildingInfo :input").attr("disabled", true);
                $("#Food :input").attr("disabled", true);
                $("#Health :input").attr("disabled", true);
                $("#EnergyAndFarming :input").attr("disabled", true);

            }


        }

        self.LoadAllmember = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldmemberData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            
                            self.Members(data);
                        }
                    },
                    error: function (error) {
                        
                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no Members!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }
        self.LoadSurveyData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldSurveyData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            //var survey = new SurveyAdministration();
                            //survey.LoadData(data);
                            self.SurveyAdministration.LoadData(data);
                        }
                    },
                    error: function (error) {
                        var survey = new SurveyAdministration();
                        survey.HouseHoldNo(self.HouseHoldNo);
                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }
        self.LoadBuildingData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldBuildingData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            self.LoadMaterials();
                            self.HouseHoldBuilding.LoadData(data);
                            $.when(self.LoadBuildings()).done(function () {
                                if (data.OtherBuildings.length > 0) {
                                    $.each(data.OtherBuildings, function (index, value) {
                                        if (typeof (value) != 'undefined') {
                                            var aDetail = new HouseHoldOtherBuildings();
                                            aDetail.LoadData(value);
                                            self.OtherBuildings.push(aDetail);
                                            self.SelectedBuildings.push(value.BuildingId);
                                        }
                                    });
                                }
                            });
                            $.when(self.LoadHazards()).done( function(){
                            if (data.PronetoHazards.length > 0) {
                                $.each(data.PronetoHazards, function (index, value) {
                                    if (typeof (value) != 'undefined') {
                                        var aDetail = new HouseHoldHazards();
                                        aDetail.LoadData(value);
                                        self.PronetoHazards.push(aDetail);
                                        self.Hazards.push(value.HazardId);
                                    }
                                });
                            }
                            });
                         }
                    },
                    error: function (error) {
                        
                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }
        self.LoadFoodData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldFoodData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            self.HouseHoldFood.LoadData(data);
                            if (data.MainFoods.length > 0) {
                                self.MainFoods.removeAll();
                                $.each(data.MainFoods, function (index, value) {
                                    var aDetail = new HouseHoldMainFoods();
                                    if (typeof (value) != 'undefined') {
                                        aDetail.LoadData(value);
                                        //console.log("aDetail : " + ko.toJSON(value));
                                        self.MainFoods.push(aDetail);
                                    }
                                });
                            }
                            
                            if (data.FoodShortages.length > 0) {
                                self.HHFoodShortages.removeAll();
                                $.each(data.FoodShortages, function (index, value) {
                                    var aDetail = new HouseHoldFoodShortage();
                                    if (typeof (value) != 'undefined') {
                                        value.ShortageName = 'Shortage ' + (index+1);
                                        aDetail.LoadData(value);
                                        self.HHFoodShortages.push(aDetail);
                                    }
                                });
                            }

                        }
                    },
                    error: function (error) {
                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Local!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a Location!");
            }

        }
        self.LoadHealthData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldHealthData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data != null) {
                            self.HouseHoldHealth.LoadData(data);
                            
                            $.when(self.LoadWaterSources()).done(function () {
                                if (data.MainWaterSources.length > 0) {
                                    $.each(data.MainWaterSources, function (index, value) {
                                        if (typeof (value) != 'undefined') {
                                            var aDetail = new HouseHoldWaterSources();
                                            aDetail.LoadData(value);
                                            self.HHWaterSources.push(aDetail);
                                            self.SelectedWaterSources.push(value.SourceId);
                                        }
                                    });
                                }
                            });
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }
        self.LoadLandAssetData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldLandAssetData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data != null) {
                            self.HouseHoldAsset.LoadData(data);

                            $.when(self.LoadAgriTools()).done(function () {
                                if (data.AgriculturalTools.length > 0) {
                                    $.each(data.AgriculturalTools, function (index, value) {
                                        if (typeof (value) != 'undefined') {
                                            var aDetail = new HouseHoldLandAsset();
                                            aDetail.LoadData(value);
                                            self.HHAgriTools.push(aDetail);
                                            self.SelectedTools.push(value.ToolId);
                                        }
                                    });
                                }
                            });
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }
        self.LoadDisasterData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldDisaterData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        ///console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            self.HouseHoldDisaster.LoadData(data);
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no Information!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }
        self.LoadEnergyData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetHouseHoldEnergyData?hhid=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data != null) {
                            self.HouseHoldEnergy.LoadData(data);

                            $.when(self.LoadEnergySources()).done(function () {
                                if (data.HouseHoldEnergySources.length > 0) {
                                    $.each(data.HouseHoldEnergySources, function (index, value) {
                                        if (typeof (value) != 'undefined') {
                                            var aDetail = new HouseHoldEnergySource();
                                            aDetail.LoadData(value);
                                            self.HHEnergySources.push(aDetail);
                                            self.SelectedEnergySources.push(value.SourceId);
                                        }
                                    });
                                }
                            });
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }

       
        self.EditMember = function (data) {
            self.Member.Id(data.Id);
            self.Member.HHId(data.HHId);
            self.Member.HouseHoldNo(data.HouseHoldNo);
            self.Member.MemberName(data.MemberName);
            self.Member.Gender(data.Gender);
            self.Member.Age(data.Age);
            self.Member.OccupationId(data.OccupationId);
            self.Member.Education(data.Education);
            self.Member.MaritalStatus(data.MaritalStatus);
            self.Member.PrimaryIncome(data.PrimaryIncome);
            self.Member.SecondaryOccupation(data.SecondaryOccupation);
            self.Member.SecondaryIncome(data.SecondaryIncome);
            self.Member.RelationtoHH(data.RelationtoHH);
            self.Member.NID(data.NID);
            self.Member.IsSafetyNet(data.IsSafetyNet);
            self.Member.IsHeadPerson(data.IsHeadPerson);
            self.Member.IsDisable(data.IsDisable);

        }
        self.InitialValueLoad = function () {
            
            self.getLocations(4);
            self.LoadGenders();
            self.getBuildingParts();
            self.LoadHazards();
            self.LoadWaterSources();
            self.LoadEnergySources();
            //self.LoadCrops();
            self.LoadOccupations();
            self.LoadEthnicMionorities();
            self.LoadMaritalStatusList();
            self.LoadRelations();
            self.LoadReligions();
            self.LoadMaterials();
            self.LoadHouseTypes();
            self.LoadBuildings();
            self.LoadLatrines();
            
            self.LoadAgriTools();
            self.GetAllDiv();
            if (self.Id() > 0)
            {
                self.LoadBasicData();
                self.LoadAllmember();
                self.LoadSurveyData();
                self.LoadBuildingData();
                self.LoadFoodData();
                self.LoadHealthData();
                self.LoadEnergyData();
                self.LoadDisasterData();
                self.LoadLandAssetData();
                
            }
            
         }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });


        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new HouseHoldSurveyVM();
    
    vm.Id(vm.queryString("Id"));
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divScheme')[0]);



});