﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="~/Content/customassets/js/jquery-3.1.1.min.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />
///<reference path="~/Content/customassets/js/highcharts.js" />
///<reference path="~/Content/customassets/js/exporting.js" />
///<reference path="~/Content/customassets/js/export-data.js" />


$(document).ready(function () {

    function DashboardVM() {

        var self = this;

        self.ThanaId = ko.observable();
        self.ThanaName = ko.observable();

        self.UPId = ko.observable();
        self.UPName = ko.observable();

        self.HHCount = ko.observable();
        self.SelectedHHCount = ko.observable();
        self.ProposedInterventionCount = ko.observable();
        self.TotalDisbursement = ko.observable();

       // self.InterventionCategory = ko.observableArray([]);
        self.DisbursementCategoryCount = ko.observableArray([]);
        self.DisbursementCategoryAmount = ko.observableArray([]);

        self.Thanas = ko.observableArray([]);
        self.UPs = ko.observableArray([]);

        
        self.getDisburementData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/GetDisbursementDataUPWise?locid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.DisbursementCategoryCount([]);
                    self.DisbursementCategoryAmount([]);
                    $.each(data, function (index, value) {
                        self.DisbursementCategoryCount.push({ name: value.SchemeCategoryName, y: value.InterventionCount });
                        self.DisbursementCategoryAmount.push({ name: value.SchemeCategoryName, y: value.DisbursedAmount });
                    });
                    
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getHouseHoldDashboardData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/GetHouseHoldDashboardDataUPWise?locid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.HHCount(data.HHCount + ' HHs');
                    self.SelectedHHCount(data.SelectedHHCount + ' HHs');
                    self.ProposedInterventionCount(data.ProposedInterventionCount);
                    self.TotalDisbursement(data.TotalDisbursement + ' (BDT)');
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.DrawDisbursementCountChart = function () {
            $('#SectorWiseDisbursement').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Sector Wise Disbursement'
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true

                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Scheme Category',
                    colorByPoint: true,
                    data: self.DisbursementCategoryCount()
                }]
            });
        }

        self.DrawDisbursementAmountChart = function () {
            $('#AmountDisbursed').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Amount Disbursed'
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} BDT</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true

                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Category and DisbursedAmount',
                    colorByPoint: true,
                    data: self.DisbursementCategoryAmount()
                }]
            });
        }

        self.getLocations = function (level,parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    if(level == 3)
                        self.Thanas(data);
                    if (level == 4)
                        self.UPs(data);
                    
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.ThanaId.subscribe(function () {
            if (self.ThanaId() > 0) {
                self.getLocations(4, self.ThanaId());
            }
        });

        self.UPId.subscribe(function () {
            if (self.UPId() > 0)
                {
                self.getHouseHoldDashboardData();
                $.when(self.getDisburementData()).done(function () {
                    self.DrawDisbursementCountChart();
                    self.DrawDisbursementAmountChart();
                });
            }
            
            
        });

        self.InitialValueLoad = function () {
            self.getLocations(3);

        }
   }

    var vm = new DashboardVM();
    vm.InitialValueLoad();
    
    ko.applyBindings(vm, $('#divDash')[0]);



});