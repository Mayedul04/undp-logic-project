﻿$(document).ready(function () {
    function VulnerableHHListVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        
    }

    var pavm = new VulnerableHHListVM();
    ko.applyBindings(pavm, document.getElementById("householdAnalysisList"));
});