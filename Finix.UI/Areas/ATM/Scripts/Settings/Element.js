﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function ElementVM() {

        var self = this;

        self.Id = ko.observable();
        self.ElementTypeId = ko.observable();
        self.ElementTypeName = ko.observable();
        self.Name = ko.observable('').extend({ required: true, pattern: { message: 'Only alphabetical values required.', params: "[^\p{L}\d\s_]", maxLength: "100" } });
        self.LocationId = ko.observable().extend({ required: true });
        self.LocationName = ko.observable();

        self.ParentLocationId = ko.observable();
        self.ParentLocationName = ko.observable();
        
        self.Details = ko.observable();
        self.Latitude = ko.observable('').extend({ required: true });
        self.Longitude = ko.observable('').extend({ required: true });
        self.Year = ko.observable();
        self.HazardId = ko.observable();
        self.HazardName = ko.observable();
        self.DamagePercentage = ko.observable();
        self.PreviousPosition = ko.observable();
        self.CurrentPosition = ko.observable();
        self.PreviousType = ko.observable();
        self.CurrentType = ko.observable();
        self.IsAffectedBefore = ko.observable(false);
        
        self.Locations = ko.observableArray([]);
        self.ElementTypes = ko.observableArray([]);
        self.ParentLocations = ko.observableArray([]);
        self.Elements = ko.observableArray([]);
        self.Hazards = ko.observableArray([]);

        self.IsOrganization = ko.observable("false");

        self.Reset = function () {
                self.Id('');
                self.Name('');
                self.LocationId('');
                self.Details('');
                self.Latitude('');
                self.Latitude('');
                self.DamagePercentage('');
         }

        self.SaveElement = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Settings/SaveElement',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                       // self.AllLocatins();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getElementTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getElementTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.ElementTypes(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.getLocations = function (level) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level==4)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.ParentLocationId.subscribe(function () {
            if (self.ParentLocationId() > 0)
                self.getLocations(5, self.ParentLocationId());
            else
                self.Locations({});
        });

        self.getHazards = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazards',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Hazards(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }



        self.LoadData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/Settings/getElementById?id=' + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data :" + ko.toJSON(data));
                        self.Name(data.Name);
                        $.when(self.getElementTypes()).done(function () {
                            self.ElementTypeId(data.ElementTypeId);
                        });
                        $.when(self.getLocations(4)).done(function () {
                            self.ParentLocationId(data.ParentLocationId);
                        });
                        $.when(self.getLocations(5)).done(function () {
                            self.LocationId(data.LocationId);
                        });
                        self.Details(data.Details);
                        self.Latitude(data.Latitude);
                        self.Longitude(data.Longitude);
                        self.IsAffectedBefore(data.IsAffectedBefore);
                        self.Year(data.Year);
                        $.when(self.getHazards()).done(function () {
                            self.HazardId(data.HazardId);
                        });
                        
                        self.DamagePercentage(data.DamagePercentage);
                        self.PreviousPosition(data.PreviousPosition);
                        self.CurrentPosition(data.CurrentPosition);
                        self.PreviousType(data.PreviousType);
                        self.CurrentType(data.CurrentType);
                    },
                    error: function (error) {
                        $('#successModal').modal('show');
                        $('#successModalText').text(error.statusText);
                    }
                });

            }
        };

     
        self.InitialValueLoad = function () {
            self.getElementTypes();
            self.getLocations(4);
            self.getHazards();
            //self.getParents();
            //self.errors.showAllMessages(true);
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new ElementVM();
    vm.InitialValueLoad();
    vm.Id(vm.queryString("Id"));
    vm.LoadData();
    ko.applyBindings(vm, $('#divElement')[0]);



});