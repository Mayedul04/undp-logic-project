﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function CultivationSeasonVM() {

        var self = this;

        self.Id = ko.observable();
        self.SeasonName = ko.observable('').extend({ required: true, pattern: { message: 'Only alphabetical values required.', params: "[^\p{L}\d\s_]", maxLength: "100" } });
        self.CultivationSeasons = ko.observableArray([]);
        self.Details = ko.observable();

        self.StartMonthId = ko.observable('').extend({required: true});
        self.EndMonthId = ko.observable('').extend({required:true});
        self.StartMonthName = ko.observable();
        self.EndMonthName = ko.observable();

        self.Months = ko.observableArray([]);

        self.Reset = function () {
            self.Id('');
            self.SeasonName('');
            self.Details('');
            self.StartMonthName('');
            self.EndMonthName('');
        }

        self.SaveCultivationSeason = function () {
            if (self.IsValid()) {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Settings/SaveCultivationSeason',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.AllCultivationSeason();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }


        self.AllCultivationSeason = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getCultivationSeason',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data)
                        self.CultivationSeasons(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getMonths = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getMonths',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Months(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Months Found");
                }
            });
        }

        self.editCultivationSeason = function (data) {
            self.Id(data.Id);
            self.SeasonName(data.SeasonName);
            self.Details(data.Details);
            self.StartMonthName(data.StartMonthName);
            self.EndMonthName(data.EndMonthName);

        }

        self.InitialValueLoad = function () {
            self.getMonths();
            self.AllCultivationSeason();

        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

    }

    var vm = new CultivationSeasonVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divCultivationSeason')[0]);



});