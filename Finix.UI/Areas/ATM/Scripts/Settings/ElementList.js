﻿$(document).ready(function () {
    function ElementsVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        self.Wards = ko.observableArray(wards);
        self.LocationID = ko.observable(locationid);
        
        self.AddNew = function (data) {
            
            var menuInfo = {
                Id: '112_0' ,
                Menu: 'New Element',
                Url: '/ATM/Settings/ElementSetup'
                
            }
            window.parent.AddTabFromExternal(menuInfo);
        }
       
        self.Edit = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '112_' + data.Id,
                Menu: 'Element Information',
                Url: '/ATM/Settings/ElementSetup',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }


    }

    var pavm = new ElementsVM();
    ko.applyBindings(pavm, document.getElementById("activeList"));
});