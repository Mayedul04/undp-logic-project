﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function Indicator() {
        
        var self = this;
        self.Id = ko.observable();
        self.IndicatorName = ko.observable('').extend({required:true});
        self.Type = ko.observable();
        self.IndicatorTypeName = ko.observable();
        self.Weight = ko.observable('').extend({ required: true });
        self.OldWeight = ko.observable(0.0);
        self.TargetYear1 = ko.observable();
        self.TargetYear2 = ko.observable();
        self.TargetYear3 = ko.observable();
        self.TargetYear4 = ko.computed(function () {
            if (self.TargetYear1() > 0 && self.TargetYear2() > 0 && self.TargetYear3() > 0) {
                return (100 - (parseInt(self.TargetYear1()) + parseInt(self.TargetYear2()) + parseInt(self.TargetYear3())))
            }
            else
                return 0;
        });
        self.errors = ko.validation.group(self);
        self.errors.showAllMessages(true);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.IndicatorName(data.IndicatorName);
            self.Type(data.Type);
            self.IndicatorTypeName(data.IndicatorTypeName);
            self.Weight(data.Weight);
            self.OldWeight(data.Weight);
            self.TargetYear1(data.TargetYear1);
            self.TargetYear2(data.TargetYear2);
            self.TargetYear3(data.TargetYear3);
            //self.TargetYear4(data.TargetYear4);
            
        }

    }
    function IndicatorVM() {

        var self = this;

        self.Type = ko.observable('').extend({required:true});
        self.IndicatorTypeName = ko.observable('');

        //self.variable = new Indicator();

        self.Types = ko.observableArray([]);
        self.AddedIndicators = ko.observableArray([]);
        self.Indicators = ko.observableArray([]);
 
        

        self.addIndicator = function () {
            var indicator = new Indicator();
            indicator.Type(self.Type());
            //ko.validatedObservable(indicator);
            self.AddedIndicators.push(indicator);
        }
        self.removedIndicators = ko.observableArray([]);
        self.removeIndicator = function (line) {
            if (line.Id() == null)
            {
                self.removedIndicators.push(line.Id());
                self.AddedIndicators.remove(line);
            }
                
        }

        self.Type.subscribe(function () {
            if (self.Type() > 0)
                self.LoadExistingData();
            else
                self.AddedIndicators([]);
        });
 
        self.LoadExistingData = function () {
            return $.ajax({
                    type: "GET",
                    url: "/ATM/Settings/GetIndicatorsByType?typeid=" + self.Type(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data != null) {
                            self.AddedIndicators([]);
                            $.each(data, function (index, value) {
                                if (typeof (value) != 'undefined') {
                                    var aDetail = new Indicator();
                                    aDetail.LoadData(value);
                                    self.AddedIndicators.push(aDetail);
                                 }
                            });
                        }
                    },
                    error: function (error) {
                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Type!");
                    }
                });
            }

        self.SaveIndicator = function () {
            var submitdata = self.AddedIndicators();
            var total = 0;
            $.each(self.AddedIndicators(), function (i, item) {
                total = parseFloat(total) + parseFloat(item.Weight());
                
            });
            
            if (total==1)
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Settings/SaveIndicator',
                    data: ko.toJSON(submitdata),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        //self.AllIndicators();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text('The Sum of Weights must be 1');
            }
        }

        self.getTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getIndicatorTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Types(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

       

        self.InitialValueLoad = function () {
            self.getTypes();
            //self.AllIndicators();
            
        }

        self.errors = ko.validation.group(self);
        
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            var errinternal =
            function () {
                var total = 0;
                $.each(self.AddedIndicators(), function (index, value) {
                    total += value.errors().length;
                })
                return total;
            }
            err += errinternal();
            
            if (err == 0  && self.AddedIndicators().length > 0)
                return true;
            return false;
        });

    }

    var vm = new IndicatorVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divIndicator')[0]);



});