﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function LocationVM() {

        var self = this;

        self.Id = ko.observable();
        self.LocationName = ko.observable('').extend({ required: true, pattern: { message: 'Only alphabetical values required.', params: "[^\p{L}\d\s_]", maxLength: "100" } });
        self.Code = ko.observable('').extend({ required: true });
        self.Level = ko.observable().extend({ required: true });
        //self.Level = ko.observable();
        self.ParentID = ko.observable();
        self.ParentName = ko.observable();
        self.Details = ko.observable();
        self.Latitude = ko.observable('').extend({ required: true });
        self.Longitude = ko.observable('').extend({ required: true });
        
        self.Locations = ko.observableArray([]);
        self.Locationlevels = ko.observableArray([]);
        self.Parents = ko.observableArray([]);

        self.Reset = function () {
                self.Id('');
                self.LocationName('');
                self.ParentID('');
                self.Level('');
                self.Details('');
               
        }

        self.SaveLocation = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Settings/SaveLocation',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.AllLocatins();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getLocationLevels = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getLocationLevel',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Locationlevels(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.getParents = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getParents?level=' + self.Level(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data)
                        self.Parents(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        
        self.Level.subscribe(function () {
            if (self.Level() > 0)
                self.getParents();
            
        });

        
          self.LoadData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/Settings/getLocationById?id=' + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data :" + ko.toJSON(data));
                        self.Id(data.Id);
                        self.LocationName(data.LocationName);
                        self.Level(data.Level);
                        $.when(self.getParents()).done(function () {
                            self.ParentID(data.ParentID);
                            self.ParentName(data.ParentName);
                        });
                        self.Code(data.Code);
                        self.Details(data.Details);
                        self.Latitude(data.Latitude);
                        self.Longitude(data.Longitude);
                    },
                    error: function (error) {
                        $('#successModal').modal('show');
                        $('#successModalText').text(error.statusText);
                    }
                });

            }
        };
       
        

        self.InitialValueLoad = function () {
            //self.AllLocatins();
            self.getLocationLevels();
          }

        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

    }

    var vm = new LocationVM();
    vm.InitialValueLoad();
    vm.Id(vm.queryString("Id"));
    vm.LoadData();
    ko.applyBindings(vm, $('#divLocation')[0]);



});