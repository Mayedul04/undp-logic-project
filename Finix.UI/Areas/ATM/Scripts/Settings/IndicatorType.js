﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function IndicatorTypeVM() {

        var self = this;

        self.Id = ko.observable();
        self.TypeName = ko.observable('').extend({ required: true, pattern: { message: 'Only alphabetical values required.', params: "[^\p{L}\d\s_]", maxLength: "100" } });
        self.Weight = ko.observable(0.0);
        self.TypeGroup = ko.observable();
        self.GroupName = ko.observable();
         
        self.Types = ko.observableArray([]);
        self.Groups = ko.observableArray([]);
 
        self.Reset = function () {
                self.Id('');
                self.TypeName('');
                self.Weight('');
                self.TypeGroup('');
         }

        self.SaveIndicatorType = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Settings/SaveIndicatorType',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.AllTypes();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }
        self.getGroups = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getIndicatorPrimaryGroups',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Groups(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        
        self.AllTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getIndicatorTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data)
                        self.Types(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
               

       
        self.editType = function (data) {
            self.Id(data.Id);
            $.when(self.getGroups()).done( function(){
                self.TypeGroup(data.TypeGroup);
                self.GroupName(data.GroupName);
            });
            self.TypeName(data.TypeName);
            self.Weight(data.Weight);

        }

       
        

        self.InitialValueLoad = function () {
            self.getGroups();
            self.AllTypes();
            
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

    }

    var vm = new IndicatorTypeVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divIndicator')[0]);



});