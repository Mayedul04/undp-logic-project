﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function AgriculturalToolsVM() {

        var self = this;

        self.Id = ko.observable();
        self.ToolName = ko.observable('').extend({ required: true, pattern: { message: 'Only alphabetical values required.', params: "[^\p{L}\d\s_]", maxLength: "100" } });
        self.AgriculturalTools = ko.observableArray([]);



        self.Reset = function () {
            self.Id('');
            self.ToolName('');        

        }

        self.SaveAgriculturalTools = function () {
            if (self.IsValid()) {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Settings/SaveAgriculturalTools',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.AllAgriculturalTools();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }


        self.AllAgriculturalTools = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAgriculturalTools',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data)
                        self.AgriculturalTools(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.editAgriculturalTools = function (data) {
            self.Id(data.Id);
            self.ToolName(data.ToolName);


        }

        self.InitialValueLoad = function () {
             self.AllAgriculturalTools();

        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

    }

    var vm = new AgriculturalToolsVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divAgriculturalTools')[0]);



});