﻿$(document).ready(function () {
    function SchemeImplementationVM() {
        var self = this;

        self.FromDate = ko.observable(fromDate ? moment(fromDate, "DD/MM/YYYY") : moment());
        self.ToDate = ko.observable(toDate ? moment(toDate, "DD/MM/YYYY") : moment());

        self.FromDate.subscribe(function () {
            $('#fromDate').val(moment(self.FromDate()).format("DD/MM/YYYY"));
        });
        self.ToDate.subscribe(function () {
            $('#toDate').val(moment(self.ToDate()).format("DD/MM/YYYY"));
        });


    }

    var pavm = new SchemeImplementationVM();
    ko.applyBindings(pavm, document.getElementById("SchemeImplementation"));
});