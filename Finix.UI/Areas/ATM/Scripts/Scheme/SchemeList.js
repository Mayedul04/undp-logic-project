﻿$(document).ready(function () {
    function SchemeListVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        self.Thanas = ko.observableArray(thanas);
        self.Thana = ko.observable(thana);
        self.SchemeTitle = ko.observable(searchString);
        
        

        self.Edit = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '131_' + data.Id,
                Menu: 'Scheme Implementation Information',
                Url: '/ATM/Scheme/SchemeImplementation',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        

    }

    var pavm = new SchemeListVM();
    ko.applyBindings(pavm, document.getElementById("#divSchemeList"));
});