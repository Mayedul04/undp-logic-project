﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function SchemeFund() {
        var self = this;
        self.Id = ko.observable();
        self.SchemeId = ko.observable();
        self.SchemeTitle = ko.observable('');
        self.SourceId = ko.observable('').extend({ required: true });
        self.SourceOfFundName = ko.observable();
        self.Amount = ko.observable(0).extend({ required: true });
               
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.SchemeId(data.SchemeId);
            self.SchemeTitle(data.SchemeTitle);
            self.SourceId(data.SourceId);
            self.SourceOfFundName(data.SourceOfFundName);
            self.Amount(data.Amount);
            
        }

        
    }
    function SchemeBeneficiaries() {
        var self = this;
        self.Id = ko.observable();
        self.SchemeId = ko.observable();
        self.Beneficiary = ko.observable('');
        self.BeneficiaryCount = ko.observable();
        
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.SchemeId(data.SchemeId);
            self.Beneficiary(data.Beneficiary);
            self.BeneficiaryCount(data.BeneficiaryCount);
         }

    }
    function SchemeHazard() {
        var self = this;
        self.Id = ko.observable();
        self.SchemeId = ko.observable();
        self.HazardId = ko.observable();
        self.Status = ko.observable(0);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.SchemeId(data.SchemeId);
            self.HazardId(data.HazardId);
            self.Status(0);
        }


    }
    function SchemeEmployment() {
        var self = this;
        self.Id = ko.observable();
        self.SchemeId = ko.observable();
        self.PeopleType = ko.observable('');
        self.EmploymentCount = ko.observable();

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.SchemeId(data.SchemeId);
            self.PeopleType(data.PeopleType);
            self.EmploymentCount(data.EmploymentCount);
        }

    }
    function Expenditures() {
        var self = this;
        self.Id = ko.observable();
        self.ImplementationId = ko.observable();
        self.SourceId = ko.observable('');
        self.CostAmount = ko.observable(0);
        self.CostDate = ko.observable(moment());

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.ImplementationId(data.ImplementationId);
            self.SourceId(data.SourceId);
            self.CostAmount(data.CostAmount);
            self.CostDate(data.CostDate);

        }


    }
    
    function SchemeVM() {

        var self = this;

        self.Id = ko.observable();
        self.UpId = ko.observable();
        self.LocationId = ko.observable();
        self.SchemeTitle = ko.observable('').extend({ required: true, maxLength: "100" });
        self.Description = ko.observable();
        self.SchemeNo = ko.observable('').extend({ required: true, maxLength: "50" });

        self.SchemeNatureId = ko.observable('').extend({ required: true });
        self.SchemeNatureName = ko.observable();
        self.SchemeCategoryId = ko.observable('').extend({ required: true });
        self.SchemeCategoryName = ko.observable();
        self.Coverage = ko.observable();
        self.ProposalRefNo = ko.observable();
        self.ImplementedBy = ko.observable();
        self.EstimatedBudget = ko.observable();
        self.RiskId = ko.observable('').extend({ required: true });
        self.GainedBenefits = ko.observable();
        self.ExpectedBenefits = ko.observable();
        self.StartDate = ko.observable(moment());
        self.EndDate = ko.observable(moment());

        self.HazardList = ko.observableArray([]);
        self.SelectedHazards = ko.observableArray([]);
        

        self.RiskStatements = ko.observableArray([]);
        self.SchemeCategories = ko.observableArray([]);
        self.SchemeNatures = ko.observableArray([]);
        self.SourceList = ko.observableArray([]);

        self.SchemeHazards = ko.observableArray([]);
        self.Funds = ko.observableArray([]);
        self.Expenses = ko.observableArray([]);
        self.Beneficiaries=ko.observableArray([]);
        self.SchemeEmployments = ko.observableArray([]);

        self.Elements = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.UPs = ko.observableArray([]);
        
        self.UpId.subscribe(function () {
            if (self.UpId() > 0)
            {
                self.getLocations(5, self.UpId());
                self.LoadRisks();
            }
                
            else
                self.Locations({});
        });

        self.addFund = function () {
            var fund = new SchemeFund();
            self.Funds.push(fund);
        }
        self.removedFunds = ko.observableArray([]);
        self.removeFund = function (line) {
            if (line.Id() > 0)
                self.removedFunds.push(line.Id());
            self.Funds.remove(line);
        }

        self.addExpense = function () {
            var expense = new Expenditures();
            self.Expenses.push(expense);
        }
        self.removedExpenses = ko.observableArray([]);
        self.removeExpense = function (line) {
            if (line.Id() > 0)
                self.removedExpenses.push(line.Id());
            self.Expenses.remove(line);
        }

        self.addBeneficiary = function () {
            var benf = new SchemeBeneficiaries();
            self.Beneficiaries.push(benf);
        }
        self.removedBeneficiaries = ko.observableArray([]);
        self.removeBeneficiary = function (line) {
            if (line.Id() > 0)
                self.removedBeneficiaries.push(line.Id());
            self.Beneficiaries.remove(line);
        }

        self.addEmployment = function () {
            var emp = new SchemeEmployment();
            self.SchemeEmployments.push(emp);
        }
        self.removedEmployments = ko.observableArray([]);
        self.removeEmployment = function (line) {
            if (line.Id() > 0)
                self.removedEmployments.push(line.Id());
            self.SchemeEmployments.remove(line);
        }
        
        self.getLocations = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level == 4)
                        self.UPs(data);
                    else 
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadHazards = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazards',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // console.log(ko.toJSON(data));
                    self.HazardList(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadSourceOfFunds = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAllSourceOfFund',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.SourceList(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadRisks = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/getAllStatements?upid=' + self.UpId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.RiskStatements(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Element type found");
                }
            });
        }

        self.LoadSchemeNatures = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Scheme/getSchemeNatures',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.SchemeNatures(data); 
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadSchemeCategories = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/Scheme/getSchemeCategories',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.SchemeCategories(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        
        self.saveSchemeInfo = function () {
            for (var i = 0; i < self.SelectedHazards().length ; i++) {
                var item = new SchemeHazard();
                var temp = self.SelectedHazards()[i];

                item.HazardId(temp);
                item.Status(1);
                if (self.SelectedHazards().length < 1)  //First Time
                    self.SchemeHazards.push(item);
                else {
                    $.each(self.SchemeHazards(), function (index, value) {
                        if (value.HazardId() == temp) {
                            value.Status(1);
                            temp = 0;
                            return false;
                        }
                    });
                    if (temp != 0)
                        self.SchemeHazards.push(item);
                }
            }
            
            //self.SchemeHazards(self.SchemeHazards());
           
            $.ajax({
                type: "POST",
                url: '/ATM/Scheme/SaveSchemeImplementation',
                data: ko.toJSON(this),
                contentType: "application/json",
                success: function (data) {
                    self.Id(data.Id);
                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);

                   // self.GetAllDiv();
                    // self.CodeVisible(true);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.LoadSchemeData = function () {
            
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Scheme/GetSchemeData?id=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {

                            self.Id(data.Id);
                            self.LocationId(data.LocationId);
                            self.SchemeTitle(data.SchemeTitle);
                            self.Description(data.Description);
                            self.SchemeNo(data.SchemeNo);
                            //self.LocationName(data.LocationName);
                            $.when(self.LoadSchemeNatures()).done(function () {
                                self.SchemeNatureId(data.SchemeNatureId);
                                self.SchemeNatureName(data.SchemeNatureName);
                            });
                            
                            $.when(self.LoadSchemeCategories()).done(function () {
                                self.SchemeCategoryId(data.SchemeCategoryId);
                                self.SchemeCategoryName(data.SchemeCategoryName);
                            });
                            
                            self.Coverage(data.Coverage);
                            self.ImplementedBy(data.ImplementedBy);
                            self.ProposalRefNo(data.ProposalRefNo);

                            $.when(self.LoadRisks()).done(function () {
                                self.RiskId(data.RiskId);
                                self.SchemeCategoryName(data.SchemeCategoryName);
                            });

                            self.EstimatedBudget(data.EstimatedBudget);
                            self.GainedBenefits(data.GainedBenefits);
                            self.ExpectedBenefits(data.ExpectedBenefits);
                            
                           
                            $.each(data.Beneficiaries, function (index, value) {
                                var aDetail = new SchemeBeneficiaries();
                                if (typeof (value) != 'undefined') {
                                    aDetail.LoadData(value);
                                    //console.log("aDetail : " + ko.toJSON(value));
                                    self.Beneficiaries.push(aDetail);
                                }
                            });

                            $.each(data.SchemeEmployments, function (index, value) {
                                var aDetail = new SchemeEmployment();
                                if (typeof (value) != 'undefined') {
                                    aDetail.LoadData(value);
                                    //console.log("aDetail : " + ko.toJSON(value));
                                    self.SchemeEmployments.push(aDetail);
                                }
                            });
                            
                            $.when(self.LoadHazards()).done(function () {
                                if (data.SchemeHazards.length > 0) {
                                    $.each(data.SchemeHazards, function (index, value) {
                                        if (typeof (value) != 'undefined') {
                                            var aDetail = new SchemeHazard();
                                            //console.log("aDetail : " + ko.toJSON(value));
                                            aDetail.LoadData(value);
                                            self.SchemeHazards.push(aDetail);
                                            self.SelectedHazards.push(value.HazardId);
                                        }
                                    });
                                }
                            });
                            
                            $.when(self.LoadSourceOfFunds()).done(function () {
                                $.each(data.Funds, function (index, value) {
                                    var aDetail = new SchemeFund();
                                    if (typeof (value) != 'undefined') {
                                        aDetail.LoadData(value);
                                        //console.log("aDetail : " + ko.toJSON(value));
                                        self.Funds.push(aDetail);
                                    }
                                });
                                $.each(data.Expenses, function (index, value) {
                                    var aDetail = new Expenditures();
                                    if (typeof (value) != 'undefined') {
                                        aDetail.LoadData(value);
                                        //console.log("aDetail : " + ko.toJSON(value));
                                        self.Expenses.push(aDetail);
                                    }
                                });
                            });
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }

        self.InitialValueLoad = function () {
            self.getLocations(4, null);
            self.LoadSchemeCategories();
            self.LoadSchemeNatures();
            self.LoadSourceOfFunds();
            self.LoadHazards();
            // self.GetAllDiv();
            if (self.Id() > 0) {
                self.LoadSchemeData();
                //self.LoadAllmember();
            }

        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });


        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }
    var vm = new SchemeVM();
    vm.Id(vm.queryString("Id"));
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divScheme')[0]);

});