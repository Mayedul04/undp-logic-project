﻿using Finix.ATM.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.ATM.Controllers
{
    public class DashboardController : Controller
    {
        private readonly VulnerabilityFacade _localeconomyfacade = new VulnerabilityFacade();
        private readonly LandUseFacade _hazardfacade = new LandUseFacade();
        // GET: ATM/Dashboard
        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult GetEmployementDataUPWise(long locid)
        {
            var data = _localeconomyfacade.LoadEmploymentInfoByLocation(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLandTypeInfoUPWise(long locid)
        {
            var data = _localeconomyfacade.LoadLandTypeInfoByLocation(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPovertySituationUPWise(long locid)
        {
            var data = _localeconomyfacade.LoadPovertySituationByLocation(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHazardVennDiagramUPWise(long locid)
        {
            var data = _hazardfacade.GetHazardVennByLocation(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}