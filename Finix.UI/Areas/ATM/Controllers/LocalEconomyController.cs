﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.ATM.Controllers
{
    public class LocalEconomyController : Controller
    {
        // GET: ATM/LocalEconomy
        public ActionResult LocalEconomyType()
        {
            return View();
        }

        public ActionResult LocalEconomyVulnerability()
        {
            return View();
        }
    }
}