﻿using Finix.ATM.DTO;
using Finix.ATM.Facade;
using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.ATM.Controllers
{
    public class SettingsController : Controller
    {
        private readonly LocationFacade _facadelocation = new LocationFacade();
        private readonly EnumFacade _enums = new EnumFacade();
        private readonly HouseHoldFacade _facade = new HouseHoldFacade();
        // GET: ATM/Settings


        #region Hazard Setup
        public ActionResult HazardSetup()
        {
            return View();
        }
        public ActionResult SaveHazard(HazardDto dto)
        {
            var result = _facadelocation.SaveHazards(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AllHazards()
        {
            var data = _facadelocation.GetHazards();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AllHazardsByLocation(long? locationid)
        {
            var data = _facadelocation.AllHazardByLocation(locationid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Elements
        public ActionResult ElementSetup()
        {
            return View();
        }

        public ActionResult AllElements(string sortOrder, string searchString, long? up, long? locationid, int page = 1, int pageSize = 20)
        {
           
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (locationid != null)
                ViewBag.LocationId = (long)locationid;
            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs =   _facadelocation.GetLocationsByLevel(LocationLevel.UP,null);
            ViewBag.Wards = _facadelocation.GetLocationsByLevel(LocationLevel.Ward,up);


            var data = _facadelocation.ElementList(pageSize, page, searchString, locationid);
            return View(data);
        }

        public ActionResult getElementById(long id)
        {
            var data = _facadelocation.GetElementById(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveElement(ElementDto dto)
        {
            var result = _facadelocation.SaveElements(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ElementType()
        {
            return View();
        }
        public ActionResult SaveElementType(ElementTypeDto dto)
        {
            var result = _facadelocation.SaveElementType(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getElementTypes()
        {
            var data = _facadelocation.GetElementTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getLocationsByLevel(LocationLevel level, long? parent)
        {
            var data = _facadelocation.GetLocationsByLevel(level,parent);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        
        #endregion
        
        #region Indicators

        public ActionResult IndicatorType()
        {
            return View();
        }
        public ActionResult SaveIndicatorType(IndicatorTypeDto dto)
        {
            var result = _facadelocation.SaveIndicatorType(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getIndicatorPrimaryGroups()
        {
            var data = _enums.GetIndicatorPrimaryGroups();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getIndicatorTypes()
        {
            var data = _facadelocation.GetAllIndicatorTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIndicatorsByType(long typeid)
        {
            var data = _facadelocation.GetAllIndicatorsByType(typeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Indicators()
        {
            return View();
        }
        public ActionResult SaveIndicator(List<IndicatorsDto> dto)
        {
            var result = _facadelocation.SaveIndicators(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getIndicators()
        {
            var data = _facadelocation.GetAllIndicators();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult Sensitivity()
        {
            return View();
        }

        public ActionResult Exposure()
        {
            return View();
        }

       
        public ActionResult IndictorEvaluations()
        {
            return View();
        }
        

        #region Locations

        public ActionResult AllLocations(string sortOrder, string currentFilter, string searchString, long? up, long? locationid, int page = 1, int pageSize = 20)
        {

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (locationid != null)
                ViewBag.LocationId = (long)locationid;
            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, null);
            ViewBag.Wards = _facadelocation.GetLocationsByLevel(LocationLevel.Ward, up);


            var data = _facadelocation.LocationList(pageSize, page, searchString, up, locationid);
            return View(data);
        }
        public ActionResult LocationSetup()
        {
            return View();
        }
        public ActionResult SaveLocation(LocationDto dto)
        {
            var result = _facadelocation.SaveLocations(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getLocationLevel()
        {
            var data = _enums.GetLocationLevels();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
       
        public ActionResult getParents(LocationLevel level)
        {
            var data = _facadelocation.GetParents(level);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getLocationById(long id)
        {
            var data = _facadelocation.GetLocationById(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Element At Risk
        public ActionResult ElementAtRisk()
        {
            return View();
        }
        public ActionResult SaveElementAtRisk(ElementAtRiskDto dto)
        {
            var result = _facadelocation.SaveElemntAtRisk(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getElementsByLocation(long? locationid)
        {
            var data = _facadelocation.GetElements(locationid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllElementsAtRisk()
        {
            var data = _facadelocation.GetAllElementsAtRisk();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Water Sources
        public ActionResult getAllWaterSource()
        {
            var data = _facadelocation.GetAllWaterSource();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Source Of Fund
        public ActionResult getAllSourceOfFund()
        {
            var data = _facadelocation.GetAllFundSource();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Support Type
        public ActionResult getAllSupportType()
        {
            var data = _facadelocation.GetAllSupportTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Ethnic Minorty
        public ActionResult getAllEthnicMinority()
        {
            var data = _facadelocation.GetAllEthnicMinority();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult Diagram()
        {
            return View();
        }


        public ActionResult DisastersAndClimateChange()
        {
            return View();
        }

        public ActionResult getGenderList()
        {
            var data = _enums.GetGenders();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region Crops
        public ActionResult Crops()
        {
            return View();
        }

        public ActionResult SaveCrop(CropsDto dto)
        {
            var result = _facade.SaveCrop(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getCrops()
        {
            var data = _facade.GetCrops();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CultivationSeason
        public ActionResult CultivationSeason()
        {
            return View();
        }

        public ActionResult SaveCultivationSeason(CropSeasonDto dto)
        {
            var result = _facade.SaveCultivationSeason(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getMonths()
        {
            var data = _facade.GetMonths();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getCultivationSeason()
        {
            var data = _facade.GetCultivationSeason();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Source of fund
        public ActionResult SourceOfFund()
        {
            return View();
        }

        public ActionResult SaveSourceOfFund(SourceOfFundDto dto)
        {
            var result = _facade.SaveSourceOfFund(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSourceOfFund()
        {
            var data = _facade.GetSourceOfFund();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region SupportType
        public ActionResult SupportType()
        {
            return View();
        }

        public ActionResult SaveSupportType(SupportTypeDto dto)
        {
            var result = _facade.SaveSupportType(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSupportType()
        {
            var data = _facade.GetSupportType();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region WaterSource
        public ActionResult WaterSource()
        {
            return View();
        }

        public ActionResult SaveWaterSource(WaterSourceDto dto)
        {
            var result = _facade.SaveWaterSource(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getWaterSource()
        {
            var data = _facade.GetWaterSource();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region LandType
        public ActionResult LandType()
        {
            return View();
        }

        public ActionResult SaveLandType(LandTypeDto dto)
        {
            var result = _facade.SaveLandType(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getLandType()
        {
            var data = _facade.GetLandType();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion
        public ActionResult getMaritalStatusList()
        {
            var data = _enums.GetMaritalStatus();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getRelations()
        {
            var data = _enums.GetRelations();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getReligions()
        {
            var data = _enums.GetReligions();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getRentalTypes()
        {
            var data = _enums.GetRentalTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getHouseTypes()
        {
            var data = _enums.GetHouseTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getOtherBuildings()
        {
            var data = _facade.GetOtherBuildings();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getBuilgingMaterials()
        {
            var data = _facade.GetMaterials();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getLatrineTypes()
        {
            var data = _enums.GetLatrineTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAgriTools()
        {
            var data = _facade.GetAgriTools();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region AgriculturalTools
        public ActionResult AgriculturalTools()
        {
            return View();
        }

        public ActionResult SaveAgriculturalTools(AgriculturalToolsDto dto)
        {
            var result = _facade.SaveAgriculturalTools(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAgriculturalTools()
        {
            var data = _facade.GetAgriculturalTools();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region EthnicMinority
        public ActionResult EthnicMinority()
        {
            return View();
        }

        public ActionResult SaveEthnicMinority(EthnicMinorityDto dto)
        {
            var result = _facade.SaveEthnicMinority(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getEthnicMinority()
        {
            var data = _facade.GetEthnicMinority();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        
    }
}