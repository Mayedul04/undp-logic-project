﻿using Finix.ATM.DTO;
using Finix.ATM.Facade;
using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.ATM.Controllers
{
    
    public class AgriculturalInfoController : Controller
    {
        private readonly LandUseFacade _facade = new LandUseFacade();
        private readonly LocationFacade _facadelocation = new LocationFacade();
        // GET: ATM/AgriculturalInfo
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult getMonths()
        {
            var data = _facade.GetMonths();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getCropSeasons()
        {
            var data = _facade.GetCropSeasons();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #region Crop Calendar
        public ActionResult CropCalendar()
        {
            return View();
        }
        public ActionResult SaveCropCalendar(CropCalendarDto dto)
        {
            var result = _facade.SaveCropCalendar(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getCrops()
        {
            var data = _facade.GetCrops();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getCropCalendar(long? locid)
        {
            var data = _facade.GetCropCalendar(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getCropCalendarById(long id)
        {
            var data = _facade.GetCropCalendarById(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        } 
        #endregion

        #region Use of Waterbodies
        public ActionResult LandUse()
        {
            return View();
        }

        public ActionResult getLandTypes()
        {
            var data = _facade.GetLandTypes(true);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveLandUse(LandUseDto dto)
        {
            var result = _facade.SaveLandInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getLandInfoById(long id)
        {
            var data = _facade.GetLandInfoById(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getLandList()
        {
            var data = _facade.GetLandList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Hazard Calendar
        public ActionResult HazardCalendar()
        {
            return View();
        }
        public ActionResult SaveHazardCalendar(HazardLocationMappingDto dto)
        {
            var result = _facade.SaveHazardCalendar(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getHazardCalendar(long? locid)
        {
            var data = _facade.GetHazardCalendar(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getCalendarInfoByHazardId(long hazardid)
        {
            var data = _facade.GetCalendarByHazardId(hazardid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Agricultural Land Information
        public ActionResult AgriculturalLandInformation()
        {
            return View();
        }

        public ActionResult SaveAgriculturalLandInfo(AgriculturalLandInfoDto dto)
        {
            var result = _facade.SaveAgriculrulLandInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AllAgriculturalLands(string sortOrder, long? up, long? locationid, int page = 1, int pageSize = 20)
        {
            ViewBag.CurrentSort = sortOrder;
            if (locationid != null)
                ViewBag.LocationId = (long)locationid;
            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, null);
            ViewBag.Wards = _facadelocation.GetLocationsByLevel(LocationLevel.Ward, up);

            var data = _facade.AgriculturalLandList(pageSize, page, locationid);
            return View(data);
        }

        public ActionResult getAgriculturalLandInfoById(long id)
        {
            var data = _facade.getAgriculturalLandInfoById(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        } 
        #endregion
    }
}