﻿using Finix.ATM.DTO;
using Finix.ATM.Facade;
using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;
using System.Web.Mvc;
using System.IO;

namespace Finix.UI.Areas.ATM.Controllers
{
    public class HouseholdController : Controller
    {
        private readonly HouseHoldFacade _facade = new HouseHoldFacade();
        private readonly LocationFacade _facadelocation = new LocationFacade();
        private readonly EnumFacade _enums = new EnumFacade();
        // GET: ATM/Household
        #region Energy Sources
        public ActionResult EnergySource()
        {
            return View();
        }
        public ActionResult SaveEnergySource(EnergySourceDto dto)
        {
            var result = _facade.SaveEnergySource(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getEnergySources()
        {
            var data = _facade.GetEnergySources();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Materails
        public ActionResult Material()
        {
            return View();
        }
        public ActionResult SaveMaterial(MaterialDto dto)
        {
            var result = _facade.SaveMaterial(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getMaterials()
        {
            var data = _facade.GetMaterials();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Building Part
        //public ActionResult Material()
        //{
        //    return View();
        //}
        //public ActionResult SaveMaterial(MaterialDto dto)
        //{
        //    var result = _facade.SaveMaterial(dto, SessionHelper.UserProfile.UserId);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult getBuildingParts()
        {
            var data = _facade.GetBuildingParts();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult HouseholdActivityTracking()
        {
            return View();
        }

        public ActionResult QuestionAboutHousehold()
        {
            return View();
        }

        public ActionResult AboutBuilding()
        {
            return View();
        }

        public ActionResult QuestionAboutFood()
        {
            return View();
        }

        public ActionResult QuestionAboutHealth()
        {
            return View();
        }

        public ActionResult EnergyAndFarming()
        {
            return View();
        }

        #region House Hold Informations
        public ActionResult HouseholdInformation()
        {
            return View();
        }
        public ActionResult SaveHHBasicInfo(HouseHoldInformationDto dto)
        {
            var result = _facade.SaveHHBasicInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveHHSurveyInfo(HouseHoldSurveyAdministrationDto dto)
        {
            var result = _facade.SaveHHSurveyInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveHHBuildingInfo(HouseHoldBuildingDto dto)
        {
            var result = _facade.SaveHHBuildingInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveHHFoodInfo(HouseHoldFoodDto dto)
        {
            var result = _facade.SaveHHFoodInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveHHHealthInfo(HouseHoldHealthDto dto)
        {
            var result = _facade.SaveHHHealthInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveHHLandAsset(HouseHoldLandAssetDto dto)
        {
            var result = _facade.SaveHHLandAsset(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveHHEnergyInfo(HouseHoldEnergyDto dto)
        {
            var result = _facade.SaveHHEnergyInfo(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HouseholdList(string sortOrder, string currentFilter, string searchString, long? ownership, long? occupation, long? distid, long? upazilaid, long? up, long? locationid, bool? savings, bool? womenheaded, bool? proximity, bool? ethnicminority, bool? adolsentmother, bool? disable, bool? migrated, bool? embankment, double? lowerlimit, double? upperlimit, int? lowerage, int? upperage, int page = 1, int pageSize = 20)
        {
            ViewBag.SearchString = searchString; 
            ViewBag.CurrentSort = sortOrder;

            if (distid != null)
                ViewBag.DistId = (long)distid;
            if (upazilaid != null)
                ViewBag.UpazilaId = (long)upazilaid;
            if (ownership != null)
                ViewBag.UP = (long)ownership;
            if (locationid != null)
                ViewBag.LocationId = (long)locationid;

            if (ownership != null)
                ViewBag.Ownership = (long)ownership;
            if (occupation != null)
                ViewBag.Occupation = (long)occupation;

            if (lowerlimit != null)
                ViewBag.LowerLimit = (long)lowerlimit;
            if (upperlimit != null)
                ViewBag.UpperLimit = (long)upperlimit;
            if (lowerage != null)
                ViewBag.LowerAge = (long)lowerage;
            if (upperage != null)
                ViewBag.UpperAge = (long)upperage;


            if (savings != null)
                ViewBag.Savings = (bool)savings;
            if (womenheaded != null)
                ViewBag.WomenHeaded = (bool)womenheaded;
            if (proximity != null)
                ViewBag.Proximity = (bool)proximity;
            if (ethnicminority != null)
                ViewBag.EthnicMinority = (bool)ethnicminority;
            if (adolsentmother != null)
                ViewBag.AdolsentMother = (bool)adolsentmother;
            if (disable != null)
                ViewBag.Disable = (bool)disable;
            if (migrated != null)
                ViewBag.Migrated = (bool)migrated;
            if (embankment != null)
                ViewBag.Embankment = (bool)embankment;



            ViewBag.Districts = _facadelocation.GetLocationsByLevel(LocationLevel.District, null);
            ViewBag.Upazilas = _facadelocation.GetLocationsByLevel(LocationLevel.Upazila, distid);
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, upazilaid);
            ViewBag.Wards = _facadelocation.GetLocationsByLevel(LocationLevel.Ward, up);

            var data = _facade.householdList(pageSize, page, searchString, ownership, occupation,  distid, upazilaid, up,  locationid, savings, womenheaded, proximity, ethnicminority,adolsentmother, disable,  migrated, embankment,  lowerlimit, upperlimit, lowerage, upperage);
            return View(data);
        }

        public ActionResult HouseholdWaitingList(string sortOrder, string currentFilter, string searchString, long? up, long? locationid, int page = 1, int pageSize = 20)
        {
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (locationid != null)
                ViewBag.LocationId = (long)locationid;
            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, null);
            ViewBag.Wards = _facadelocation.GetLocationsByLevel(LocationLevel.Ward, up);


            var data = _facade.householdWaitingList(pageSize, page, searchString, up, locationid);
            return View(data);
        }

        public ActionResult GetHouseHoldBasicData(long hhid)
        {
            var data = _facade.LoadHouseHoldBasicInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveHHMember(HouseHoldMembersDto dto)
        {
            var result = _facade.SaveHHmember(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveHHDisaster(HouseHoldDisastersDto dto)
        {
            var result = _facade.SaveHHDisasters(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetHouseHoldmemberData(long hhid)
        {
            var data = _facade.LoadHouseHoldMemberInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHouseHoldDisaterData(long hhid)
        {
            var data = _facade.LoadHouseHoldDisasterInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberDataById(long memid)
        {
            var data = _facade.getMemberInfo(memid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHouseHoldSurveyData(long hhid)
        {
            var data = _facade.LoadHouseHoldSurveyInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetHouseHoldBuildingData(long hhid)
        {
            var data = _facade.LoadHouseHoldBuildingInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHouseHoldFoodData(long hhid)
        {
            var data = _facade.LoadHouseHoldFoodInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHouseHoldHealthData(long hhid)
        {
            var data = _facade.LoadHouseHoldHealthInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHouseHoldLandAssetData(long hhid)
        {
            var data = _facade.LoadHouseHoldLandAsset(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetHouseHoldEnergyData(long hhid)
        {
            var data = _facade.LoadHouseHoldEnergyInfo(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getHouseHoldList(long? upid)
        {
            var data = _facade.GetHouseHolds(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getHouseHoldHazardList(long hhid)
        {
            var data = _facade.LoadHouseHoldHazardList(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getWaitingHouseHoldList(long? upid)
        {
            var data = _facade.GetWaitingHouseHolds(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSelectedHouseHoldList(long? upid)
        {
            var data = _facade.GetSelectedHouseHolds(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult ProposaledHHList(string sortOrder, string currentFilter, string searchString, long? up, long? locationid, int page = 1, int pageSize = 20)
        {
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (locationid != null)
                ViewBag.LocationId = (long)locationid;
            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, null);
            ViewBag.Wards = _facadelocation.GetLocationsByLevel(LocationLevel.Ward, up);

            var data = _facade.SelectedHHList(pageSize, page, searchString, up, locationid);
            return View(data);
        }
        #region House Hold Proposals
        public ActionResult HouseholdProposal()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveHouseHoldsForProposal(List<long> selectedIds)
        {
            var result = _facade.SaveHHforProposal(selectedIds, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveIntervention(InterventionsDto dto)
        {
            var result = _facade.SaveInterventions(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveImplementation(InterventionImplementationDto dto)
        {
            var result = _facade.SaveImplementation(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveACD(AdaptionCapacityDevelopmentDto dto)
        {
            var result = _facade.SaveAdaptionCapacityDevelopment(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInterventionData(long id)
        {
            var data = _facade.LoadIntervention(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetImplementationData(long iid)
        {
            var data = _facade.LoadInterventionImplementation(iid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetADCData(long iid)
        {
            var data = _facade.LoadADCData(iid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region HouseHold Disbursements
        [HttpPost]
        public ActionResult SaveDisbursement(DisbursementHistoryDto dto)
        {
            var result = _facade.SaveDisbursement(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HouseholdDisburse()
        {
            return View();
        }

        public ActionResult LoadDisbursementHistory(long interventionid)
        {
            var data = _facade.LoadDisbursementHistory(interventionid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region House Hold Dashboard
        public ActionResult HouseHoldDashboard()
        {
            return View();
        }

        public ActionResult GetHouseHoldDashboardDataUPWise(long locid)
        {
            var data = _facade.LoadHouseHoldDashboardData(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDisbursementDataUPWise(long locid)
        {
            var data = _facade.LoadDisbursementData(locid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region HouseHold Vulnerability List
        public ActionResult VulnerableHHList(string sortOrder, string currentFilter, string searchString, long? up, int page = 1, int pageSize = 20)
        {

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;

            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, null);


            var data = _facade.VulnerableHHList(pageSize, page, searchString, up);
            return View(data);
        }
        #endregion

        #region House Hold Exposure/Sensitivity Analysis
        public ActionResult HouseholdAnalysis(string sortOrder, string currentFilter, string searchString, long? up, long? locationid, int page = 1, int pageSize = 20)
        {

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (locationid != null)
                ViewBag.LocationId = (long)locationid;
            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, null);
            ViewBag.Wards = _facadelocation.GetLocationsByLevel(LocationLevel.Ward, up);


            var data = _facade.householdAnalysisList(pageSize, page, searchString, up, locationid);
            return View(data);
        }


        public ActionResult getHouseholdAnalysisInfoById(long id)
        {
            var data = _facade.getHouseholdAnalysisInfoById(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HHExposure()
        {
            return View();
        }
        public ActionResult SaveExposureSensitivity(HouseHoldElementsAnalysisDto dto)
        {
            var data = _facade.SaveAnalysisData(dto, SessionHelper.UserProfile.UserId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getHazardPercentageById(long hazardid, long locationid)
        {
            var data = _facade.getHazardPercentageById(hazardid, locationid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getWeightByTypeId(long typeid)
        {
            var data = _facade.getTypeWeightById(typeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllAnalysisByHHId(long hhid)
        {
            var data = _facade.LoadAnalysisData(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region House Hold Potentail impact
        public ActionResult HHPotentialImpact()
        {
            return View();
        }

        public ActionResult getSelectedElementTypes(long hhid)
        {
            var data = _facade.GetElementTypesByRating(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SavePontentialImpact(HouseHoldPotentialImpactDto dto)
        {
            var data = _facade.SavePI(dto, SessionHelper.UserProfile.UserId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllPotentailImpactbyHHId(long hhid)
        {
            var data = _facade.LoadPIData(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult AdaptiveCapacity()
        {
            return View();
        }

        public ActionResult GetHHRiskData(long piid)
        {
            var data = _facade.LoadPotentialImpactInfo(piid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveAdaptiveCapacityCalculations(HouseHoldPotentialImpactDto dto)
        {
            var data = _facade.SaveADC(dto, SessionHelper.UserProfile.UserId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region RiskReductionActionPlan
        public ActionResult RiskReductionActionPlan()
        {
            return View();
        }

        public ActionResult SaveRiskReductionActionPlan(HouseHoldRRAPDto dto)
        {
            var result = _facade.SaveRiskReductionActionPlan(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getRRAPData(long hhid)
        {
            var data = _facade.LoadRRAPData(hhid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Household Report
        public ActionResult HouseholdReports()
        {
            return View();
        }

        public ActionResult GetListOfHouseholdReports(string reportTypeId, long upid)
        {
            // var RiskStatement = new List<RiskStatementDto>();
            List<HouseHoldInformationDto> householdList = new List<HouseHoldInformationDto>();
            var ListOfHousehold = _facade.GetListOfHousehold(upid);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/ATM/Reports"), "rptListOfHouseholds.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            ReportDataSource rd = new ReportDataSource("ListOfHouseholds", ListOfHousehold);
            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.6in</PageWidth>" +
                "  <PageHeight>8.2in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }



        #endregion
    }
}