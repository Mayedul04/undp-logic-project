﻿using Finix.ATM.DTO;
using Finix.ATM.Facade;
using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.ATM.Controllers
{
    public class CapacityDevelopmentActivityController : Controller
    {
        private readonly CapacityDevelopmentActivityFacade _facade = new CapacityDevelopmentActivityFacade();
        private readonly LocationFacade _facadelocation = new LocationFacade();
        // GET: ATM/CapacityDevelopmentActivity
        #region Event Types
        public ActionResult EventType()
        {
            return View();
        }
        public ActionResult SaveEventType(EventTypeDto dto)
        {
            var result = _facade.SaveEventType(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getEventTypes()
        {
            var data = _facade.GetEventTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Capacity Development Activity
        public ActionResult CapacityDevelopmentActivity()
        {
            return View();
        }
        public ActionResult CapacityDevelopmentActivityList(string sortOrder, string currentFilter, string searchString, long? up, int page = 1, int pageSize = 20)
        {
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;

            if (up != null)
                ViewBag.UP = (long)up;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, null);
            
            var data = _facade.CDAList(pageSize, page, searchString, up);
            return View(data);
        }
        public ActionResult SaveapacityDevelopmentActivity(CapacityDevelopmentActivityDto dto)
        {
            var data = _facade.SaveApacityDevelopmentActivity(dto, SessionHelper.UserProfile.UserId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getDevelopmentActivityList()
        {
            var data = _facade.GetDevelopmentActivityList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadDevelopmentActivityData(long id)
        {
            var data = _facade.LoadDevelopmentActivityData(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        } 
        #endregion
    }
}