﻿using Finix.ATM.DTO;
using Finix.ATM.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using System.IO;

namespace Finix.UI.Areas.ATM.Controllers
{
    public class RiskController : Controller
    {
        private readonly RiskSatementFacade _facade = new RiskSatementFacade();
        private readonly EnumFacade _enumfacade = new EnumFacade();
        // GET: ATM/Risk
        #region Risk Statement
        public ActionResult RiskStatement()
        {
            return View();
        }

        public ActionResult SaveRiskStatement(RiskStatementDto dto)
        {
            var result = _facade.SaveRiskStatement(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllStatements(long? upid)
        {
            var data = _facade.GetStatements(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getElementsAtRisk(long? upid)
        {
            var data = _facade.getElementsAtRisk(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Risk Statement Report
        public ActionResult GetRiskStatementReports(string reportTypeId, long upid)
        {
            // var RiskStatement = new List<RiskStatementDto>();

            //ReportViewer.PDFFonts = new Dictionary<string, Stream>(StringComparer.OrdinalIgnoreCase);
            //ReportViewer.PDFFonts.Add("MS Mincho", assembly.GetManifestResourceStream("WinRTSampleApplication.Assets.msmincho.ttf"));

            List<RiskStatementDto> memberList = new List<RiskStatementDto>();
            var RiskStatement = _facade.GetStatements(upid);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/ATM/Reports"), "rptRiskStatement.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            ReportDataSource rd = new ReportDataSource("RiskStatement", RiskStatement);
            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }
        #endregion

        #region Risk Rating and Prioritization
        public ActionResult RiskRating()
        {
            return View();
        }
        public ActionResult getPotentialstoSolution()
        {
            var data = _enumfacade.GetPotentialtoSolutions();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveRiskPriority(RiskPriorityDto dto)
        {
            var result = _facade.SaveRiskPriority(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllPriorityData(long? upid)
        {
            var data = _facade.GetRatings(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Risk Rating report
        public ActionResult GetRiskRatingReports(string reportTypeId, long upid)
        {
            // var RiskStatement = new List<RiskStatementDto>();
            List<RiskPriorityDto> memberList = new List<RiskPriorityDto>();
            var RiskRating = _facade.GetRatings(upid);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/ATM/Reports"), "rptRiskRating.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            ReportDataSource rd = new ReportDataSource("RiskRating", RiskRating);
            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.6in</PageWidth>" +
                "  <PageHeight>8.2in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }

        #endregion

        #region Element At Risk report
        public ActionResult GetElementAtRiskReports(string reportTypeId, long upid)
        {
            // var RiskStatement = new List<RiskStatementDto>();
            List<ElementAtRiskDto> memberList = new List<ElementAtRiskDto>();
            var ElementAtRisk = _facade.getElementsAtRisk(upid);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/ATM/Reports"), "rptElementAtRisk.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            ReportDataSource rd = new ReportDataSource("ElementAtRisk", ElementAtRisk);
            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }

        #endregion

        #region Risk Reduction Option
        public ActionResult RiskReductionOption()
        {
            return View();
        }

        public ActionResult SaveRiskReductionOption(RiskReductionOptionDto dto)
        {
            var result = _facade.SaveRiskReductionOption(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllOptions(long upid)
        {
            var data = _facade.GetReductionOptions(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTopStatements(long? upid)
        {
            var data = _facade.GetTopStatements(upid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Risk Reduction Option Action Plan
        public ActionResult RiskReductionActionPlan()
        {
            return View();
        }
        public ActionResult SaveOptionActionPlan(List<OptionActionPlanDto> dto)
        {
            var data = _facade.SaveOptionActionPlan(dto, SessionHelper.UserProfile.UserId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllOptionsByStatement(long statementid)
        {
            var data = _facade.GetReductionOptionsByStatement(statementid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActionPlanStatementWise(long statementid)
        {
            var data = _facade.GetActionPlansByStatement(statementid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getOptionsDonthaveActionplan(long statementid)
        {
            var data = _facade.GetOptions(statementid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}