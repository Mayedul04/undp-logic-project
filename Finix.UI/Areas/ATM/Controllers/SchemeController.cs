﻿using Finix.ATM.DTO;
using Finix.ATM.Facade;
using Finix.ATM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.ATM.Controllers
{
    public class SchemeController : Controller
    {
        private readonly SchemeFacade _facade = new SchemeFacade();
        private readonly LocationFacade _facadelocation = new LocationFacade();
        private readonly EnumFacade _enums = new EnumFacade();
        // GET: ATM/Scheme
        #region SchemeCategory
        public ActionResult SchemeCategory()
        {
            return View();
        }
        public ActionResult SaveSchemeCategory(SchemeCategoryDto dto)
        {
            var result = _facade.SaveSchemeCategory(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getSchemeCategories()
        {
            var data = _facade.GetSchemeCategories();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Scheme Nature
        public ActionResult SchemeNature()
        {
            return View();
        }
        public ActionResult SaveSchemeNature(SchemeNatureDto dto)
        {
            var result = _facade.SaveSchemeNature(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getSchemeNatures()
        {
            var data = _facade.GetSchemeNatures();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion


        public ActionResult ProposedHouseHoldList()
        {
            return View();
        }
        public ActionResult SchemeImplementation()
        {
            return View();
        }

        public ActionResult SchemeImplementationCRF()
        {
            return View();
        }

        public ActionResult SaveSchemeImplementation(SchemeInformationDto dto)
        {
            var result = _facade.SaveSchemePBCRG(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSchemeData(long id)
        {
            var data = _facade.LoadSchemeInfo(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CRFDashboard()
        {
            return View();
        }

        #region Scheme List
        //public ActionResult SchemeList()
        //{
        //    return View();
        //}

        public ActionResult SchemeList(string sortOrder, string currentFilter, string searchString, long? thana, long? up, int page = 1, int pageSize = 20)
        {

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (up != null)
                ViewBag.UP = (long)up;
            if (thana != null)
                ViewBag.Thana = (long)thana;
            ViewBag.UPs = _facadelocation.GetLocationsByLevel(LocationLevel.UP, thana);
            ViewBag.Thanas = _facadelocation.GetLocationsByLevel(LocationLevel.Upazila, null);


            var data = _facade.SchemeList(pageSize, page, searchString, thana, up);
            return View(data);
        }

        //public ActionResult getHouseholdAnalysisInfoById(long id)
        //{
        //    var data = _facade.getHouseholdAnalysisInfoById(id);
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}

        #endregion

    }
}