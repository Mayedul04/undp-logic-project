﻿using Finix.Auth.DTO;
using Finix.Auth.Facade;
using Finix.UI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.Auth.Dto;

namespace Finix.UI.Areas.Auth.Controllers
{
    public class UserController : BaseController
    {
        private UserFacade _user;
        public ActionResult UserEntry()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }
        public UserController(UserFacade userFacade)
        {
            this._user = userFacade;
        }
        //
        // GET: /Auth/User/
        [HttpPost]
        public JsonResult UserRegistration(UserDto userDto)
        {
            if (userDto.UserId != null)
            {
                userDto.Id = userDto.UserId;
            }
            var success = _user.UserRegistration(userDto);
            if ((bool)success.Success)
                return Json("User info saved successfully.");
            else
                return Json("Data not saved.");
        }

        [HttpGet]
        public JsonResult GetUsersByCompanyId(long CompanyProfileId)
        {
            var users = _user.GetUsersByCompanyId(CompanyProfileId);
            var data = users.Select(u => new { Id = u.Id, Name = u.UserName });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserOfficeApplicationEntry()
        {
            //ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;getUserOfficeApplications
            return View();
        }
        public JsonResult GetAllApplications()
        {
            var cp = _user.GetAllApplications();
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllOfficeProfiles()
        {
            var cp = _user.GetAllOfficeProfiles();
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllUsers()
        {
            var cp = _user.GetAllUsers();
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getUserOfficeApplications(long? appId, long? officeId, long? userId)
        {
            var user = _user.getUserOfficeApplications(appId, officeId, userId);
            return Json(user, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveUserOfficeApplication(UserOfficeApplicationDto dto)
        {
            try
            {

                var result = _user.SaveUserOfficeApplication(dto);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult UpdateUserOfficeApplications(long id)
        {
            try
            {

                var result = _user.UpdateUserOfficeApplications(id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }
        #region User Management
        public ActionResult UserDetails()
        {
            return View();
        }
        public ActionResult UserList(string sortOrder, string searchString, int page = 1)
        {
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            var model = _user.UserList(10, page, searchString);
            return View(model);
        }
        //LoadUserById ChangeUserStatus
        public JsonResult LoadUserById(long id)
        {
            var cp = _user.LoadUserById(id);
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ChangeUserStatus(long id, bool status)
        {
            var cp = _user.ChangeUserStatus(id, status);
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveUserDetails(List<UserOfficeApplicationDto> UserCompanyApplications, List<long> RemovedUserCompanyApplications)
        {
            try
            {
                var result = _user.SaveUserDetails(UserCompanyApplications, RemovedUserCompanyApplications, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}