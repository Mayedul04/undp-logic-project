﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });
    function ActionPlan() {
        var self = this;
        self.Id = ko.observable();
        self.UPId = ko.observable();
        self.RiskStatementId = ko.observable();
        self.OptionId = ko.observable();
        self.OptionName = ko.observable('');
        self.WhoWilldo = ko.observable();
        self.When = ko.observable(moment());
        self.How = ko.observable();
        self.Where = ko.observable();
        self.Consideration = ko.observable();
        self.LoadData = function (data) {
            self.Id(data.Id);
            self.UPId(data.UPId);
            self.RiskStatementId(data.RiskStatementId);
            self.OptionId(data.OptionId);
            self.OptionName(data.OptionName);
            self.WhoWilldo(data.WhoWilldo);
            self.When(data.When);
            self.How(data.How);
            self.Where(data.Where);
            self.Consideration(data.Consideration);
        }
    }
    function OptionActionPlanVM() {

        var self = this;

        self.Id = ko.observable();
        self.RiskStatementId = ko.observable().extend({ required: true });
        self.RiskStatementName = ko.observable();

        self.UPId = ko.observable();
        self.UPName = ko.observable();
       
        self.Statements = ko.observableArray([]);
        self.UPs = ko.observableArray([]);
        self.Options = ko.observableArray([]);
        self.DatatobeSaved = ko.observableArray([]);
        
        self.LoadOptions = ko.computed(function () {
            if (self.Options().length > 0)
            {
                self.DatatobeSaved.removeAll();
                for (var i = 0; i < self.Options().length ; i++) {
                    var item = new ActionPlan();
                    var temp = self.Options()[i];
                    item.UPId(self.UPId);
                    item.RiskStatementId(self.RiskStatementId);
                    item.OptionId(temp.Id);
                    item.OptionName(temp.OptionName);
                    self.DatatobeSaved.push(item);
                }
            }
        });

        self.SaveInfo = function () {
            var submitdata = ko.observableArray([]);
            $.each(self.DatatobeSaved(),
            function (index, value) {
                submitdata.push({
                    Id: value.Id(),
                    UPId: self.UPId(),
                    RiskStatementId: self.RiskStatementId(),
                    OptionId: value.OptionId(),
                    WhoWilldo: value.WhoWilldo(),
                    When: value.When(),
                    How: value.How(),
                    Where:value.Where(),
                    Consideration:value.Consideration()

                });
            });
 
            if (self.IsValid()) {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Risk/SaveOptionActionPlan',
                    data: ko.toJSON(submitdata),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        //self.getAllData();
                        //self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }
        

        self.getRiskStatements = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/GetTopStatements?upid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Statements(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getOptions = function () {
            if (self.RiskStatementId() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/Risk/getOptionsDonthaveActionplan?statementid=' + self.RiskStatementId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        self.Options(data); //Put the response in ObservableArray
                    },
                    error: function (error) {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text('Please Select UP!');
            }
        }


        self.getLocations = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.UPs(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }


        self.UPId.subscribe(function () {
            if (self.UPId() > 0) {
                self.getRiskStatements();
                //self.getOptions();
            }
        });

        self.RiskStatementId.subscribe(function () {
            if (self.RiskStatementId() > 0) {
                self.getOptions();
            }
        });

        self.InitialValueLoad = function () {
            self.getLocations(4);
        }

        self.loadActionPlans = function () {
            if (self.RiskStatementId() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/Risk/GetActionPlanStatementWise?statementid=' + self.RiskStatementId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            if (data.length > 0) {
                                self.DatatobeSaved.removeAll();
                                $.each(data, function (index, value) {
                                    var aDetail = new ActionPlan();
                                    if (typeof (value) != 'undefined') {
                                        aDetail.LoadData(value);
                                        //console.log("aDetail : " + ko.toJSON(value));
                                        self.DatatobeSaved.push(aDetail);
                                    }
                                });
                            }
                            
                        }
                    },
                    error: function (error) {
                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no Action Plan for this Statement!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a Statement!");
            }
            
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.DatatobeSaved().length;
            if (err == 0)
                return false;
            return true;
        });



    }

    var vm = new OptionActionPlanVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divRisk')[0]);



});