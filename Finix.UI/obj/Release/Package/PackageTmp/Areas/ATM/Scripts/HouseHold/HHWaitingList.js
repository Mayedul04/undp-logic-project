﻿$(document).ready(function () {
    function HouseholdAnalysisVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        self.Wards = ko.observableArray(wards);
        self.LocationID = ko.observable(locationid);
        self.HouseHoldNo = ko.observable(searchString);
        self.Address = ko.observable();

        self.WaitingHouseHoldList = ko.observableArray(pageData);
        self.SelectedIds = ko.observableArray([]);


        self.Selection = function () {
            self.SelectedIds([]);
            $.each(self.WaitingHouseHoldList(), function (index, value) {
                if (value.IsSelected)
                    self.SelectedIds.push(value.HHId);
            });
            if (self.SelectedIds().length > 0) {
                $.ajax({
                    url: '/ATM/HouseHold/SaveHouseHoldsForProposal',
                    type: 'POST',
                    contentType: 'application/json',
                    data: ko.toJSON(self.SelectedIds()),
                    success: function (data) {
                        self.SelectedIds([]);
                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);
                        
                    },
                    error: function (error) {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text('There is no House Hold Selected!');
            }
        }
    }

    

    var pavm = new HouseholdAnalysisVM();
    ko.applyBindings(pavm, document.getElementById("householdAnalysisList"));
});