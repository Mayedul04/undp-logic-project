﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    function HouseHoldListVM() {

        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.Districts = ko.observableArray(districts);
        self.UpaZilas = ko.observableArray(upazilas);
        self.UPs = ko.observableArray(ups);
        self.Wards = ko.observableArray(wards);
        self.Ownerships = ko.observableArray(ownerships);
        self.Occupations = ko.observableArray(occupations);

        self.DistId = ko.observableArray(distid);
        self.UpazilaId = ko.observable(upazilaid);
        self.UP = ko.observable(up);
        self.LocationID = ko.observable(locationid);
        


        self.HasSavings = ko.observable(savings);
        self.WomenHeaded = ko.observable(womenheaded);
        self.ProximitytoHaor = ko.observable(proximity);
        self.IsEthnicMinority = ko.observable(ethnicminority);
        self.HasAdolsentMother = ko.observable(adolsentmother);
        self.HaveDisable = ko.observable(disable);
        self.WentforWork = ko.observable(migrated);
        self.InsideEmbankment = ko.observable(embankment);

        self.LowerLimit = ko.observable(lowerlimit);
        self.UpperLimit = ko.observable(upperlimit);
        self.LowerAge = ko.observable(lowerage);
        self.UpperAge = ko.observable(upperage);
        

        self.HouseHoldNo = ko.observable(searchString);
        self.Ownership = ko.observable(ownership);
        self.Occupation = ko.observable(occupation);
        

        self.Edit = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.HHId
            }];
            var menuInfo = {
                Id: '121_' + data.HHId,
                Menu: 'House Hold Information',
                Url: '/ATM/Household/HouseholdInformation',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.Addnew = function (data) {
            
            var menuInfo = {
                Id: '122_',
                Menu: 'House Hold Information',
                Url: '/ATM/Household/HouseholdInformation'
                
            }
            window.parent.AddTabFromExternal(menuInfo);
        }
    }

    var vm = new HouseHoldListVM();
    
    ko.applyBindings(vm, $('#divHH')[0]);



});