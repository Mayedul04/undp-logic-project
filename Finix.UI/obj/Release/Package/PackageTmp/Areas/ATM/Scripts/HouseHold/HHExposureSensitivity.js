﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    
    function ExposureSensitivityVM() {

        var self = this;

        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable();
        
        self.ElementTypeId = ko.observable();
        self.ElementTypeName = ko.observable()

        self.LocationId= ko.observable();
        self.LocationName = ko.observable();

        self.HazardId = ko.observable();
        self.HazardName = ko.observable();

        self.Exposed = ko.observable();
        self.Sensibility = ko.observable();
        self.ElementWeight = ko.observable(1);
        self.HazardPercentage = ko.observable();
        
        self.ElementTypes = ko.observableArray([]);
        self.Hazards = ko.observableArray([]);
        self.AnalysisData = ko.observableArray([]);

        self.ExposedValue = ko.computed(function () {
            if (self.Exposed() > 0 && self.ElementWeight() > 0 && self.HazardPercentage() > 0)
                return (self.Exposed() * self.ElementWeight() * self.HazardPercentage());
            else
                return 0;
        });

        self.SensibilityValue = ko.computed(function () {
            if (self.Sensibility() > 0 && self.ElementWeight() > 0 && self.HazardPercentage() > 0)
                return (self.Sensibility() * self.ElementWeight() * self.HazardPercentage());
            else
                return 0;
        });

        self.HazardId.subscribe(function () {
            self.getHazardPercentage();
        });

        self.ElementTypeId.subscribe(function () {
            self.getElementTypeWeight();
        });

        self.Reset = function () {
            self.Id('');
            //self.ElementName('');
          }

        
        self.getHazards = function () {
            //console.log(self.LocationId());
            if (self.LocationId() > 0)
            {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/AgriculturalInfo/getHazardCalendar?locid=' + self.LocationId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        self.Hazards(data); //Put the response in ObservableArray
                    },
                    error: function (error) {
                        alert(error.status + "<--and--> No Data Found");
                    }
                });
            }
  
        }

        self.getElementTypeWeight = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/getWeightByTypeId?typeid=' + self.ElementTypeId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.ElementWeight(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Data Found");
                }
            });
        }

        self.getHazardPercentage = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/getHazardPercentageById?hazardid=' + self.HazardId() + '&locationid=' + self.LocationId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.HazardPercentage(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Data Found");
                }
            });
        }

        self.getElementTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getElementTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.ElementTypes(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Hazards Found");
                }
            });
        }

        self.LoadHouseHoldData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/GetHouseHoldBasicData?hhid='+ self.HHId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.HouseHoldNo(data.HouseHoldNo);
                    self.LocationId(data.ParentLocationId);
                    self.LocationName(data.LocationName);
                    //console.log(self.LocationId());
                    self.getHazards();
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.saveHouseHoldAnalysisData = function () {
            
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveExposureSensitivity',
                data: ko.toJSON(this),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                    self.LoadAnalysisData();
                    self.Reset();
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.LoadAnalysisData = function () {
            if (self.HHId() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/getAllAnalysisByHHId?hhid=" + self.HHId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        self.AnalysisData(data);
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }

        self.EditData = function (data) {
            
            self.Id(data.Id);
            $.when(self.LoadHouseHoldData()).done(function () {
                self.HHId(data.HHId);
                self.HazardId(data.HazardId);
                self.HazardName(data.HazardName);
            });
            $.when(self.getElementTypes()).done(function () {
                self.ElementTypeId(data.ElementTypeId);
                self.ElementTypeName(data.ElementTypeName);
                self.getElementTypeWeight();
            });
            
            self.Exposed(data.Exposed);
            self.Sensibility(data.Sensibility);
        }

        self.InitialValueLoad = function () {
            self.getElementTypes();
            
            if (self.HHId() > 0) {
                self.LoadHouseHoldData();
                self.LoadAnalysisData();
            }
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });


        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new ExposureSensitivityVM();

    vm.HHId(vm.queryString("HHId"));
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divAnalysis')[0]);



});