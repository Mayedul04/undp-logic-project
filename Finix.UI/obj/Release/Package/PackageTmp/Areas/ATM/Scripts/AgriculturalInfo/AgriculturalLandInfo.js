﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function AgriculturalLandInfoVM() {

        var self = this;

        self.Id = ko.observable();
        self.CropId = ko.observable().extend({ required: true});
        self.CropName = ko.observable();
        self.CropSeasonId = ko.observable().extend({ required: true });
        self.CropSeasonName = ko.observable();

        self.ParentLocationId = ko.observable();
        self.ParentLocationName = ko.observable();
        self.LocationId = ko.observable().extend({ required: true });
        self.LocationName = ko.observable();

        self.Production = ko.observable();
        
        self.AffectedBefore = ko.observable(false);
        self.HazardId = ko.observable();
        self.HazardName = ko.observable();
        self.AffectedYear = ko.observable();
        self.DamagePercentage = ko.observable();
       

        self.ParentLocations = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.Crops = ko.observableArray([]);
        self.Hazards = ko.observableArray([]);
        self.CropSeasons = ko.observableArray([]);
        self.Lands = ko.observableArray([]);

        

        self.Reset = function () {
                self.Id('');
                self.CropId('');
                self.CropSeasonId('');
                self.ParentLocationId('');
                self.LocationId('');
                self.Production('');
                self.AffectedYear('');
                self.DamagePercentage('');
                
         }

        self.SaveLandInfo = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/AgriculturalInfo/SaveAgriculturalLandInfo',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                       // self.AllLocatins();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getCrops = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getCrops',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Crops(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getCropSeasons = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getCropSeasons',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.CropSeasons(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getLocations = function (level,parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level==4)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.ParentLocationId.subscribe(function () {
            if (self.ParentLocationId() > 0)
                self.getLocations(5, self.ParentLocationId());
            else
                self.Locations({});
        });

        self.getHazards = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazards',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Hazards(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
              

        

        self.LoadData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/AgriculturalInfo/getAgriculturalLandInfoById?id=' + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        console.log("Data :" + ko.toJSON(data));
                        
                        $.when(self.getCropSeasons()).done(function () {
                            self.CropSeasonId(data.CropSeasonId);
                        });
                        $.when(self.getCrops()).done(function () {
                            self.CropId(data.CropId);
                        });
                        $.when(self.getLocations(4)).done(function () {
                            self.ParentLocationId(data.ParentLocationId);
                        });
                        $.when(self.getLocations(5)).done(function () {
                            self.LocationId(data.LocationId);
                        });
                        self.Production(data.Production);
                        self.AffectedBefore(data.AffectedBefore);
                        
                        
                        $.when(self.getHazards()).done(function () {
                            self.HazardId(data.HazardId);
                        });
                        self.AffectedYear(data.AffectedYear);
                        self.DamagePercentage(data.DamagePercentage);
                        
                    },
                    error: function (error) {
                        $('#successModal').modal('show');
                        $('#successModalText').text(error.statusText);
                    }
                });

            }
        };

     
        self.InitialValueLoad = function () {
            self.getCrops();
            self.getLocations(4);
            self.getHazards();
            self.getCropSeasons();
            //self.getParents();
            //self.errors.showAllMessages(true);
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new AgriculturalLandInfoVM();
    vm.InitialValueLoad();
    vm.Id(vm.queryString("Id"));
    vm.LoadData();
    ko.applyBindings(vm, $('#divLandUse')[0]);



});