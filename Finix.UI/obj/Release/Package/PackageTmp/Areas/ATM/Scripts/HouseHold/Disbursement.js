﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });
    
    function InterventionBudget() {
            var self = this;
            self.Id = ko.observable();
            self.InterventionId = ko.observable();
            self.InterventionTitle = ko.observable('');
            self.Details = ko.observable();
            self.FundSourceId = ko.observable();
            self.SourceOfFundName = ko.observable('');
            self.FundAmount = ko.observable();
               
            self.LoadData = function (data) {
                self.Id(data.Id);
                self.InterventionId(data.InterventionId);
                self.InterventionTitle(data.InterventionTitle);
                self.FundSourceId(data.FundSourceId);
                self.Details(data.Details);
                self.SourceOfFundName(data.SourceOfFundName);
                self.FundAmount(data.FundAmount);
           }

        }
    function DisbursementVM() {

        var self = this;

        self.Id = ko.observable();
        self.InterventionId = ko.observable();
        self.InterventionTitle = ko.observable();

        self.HHHeadName = ko.observable();
        self.HouseHoldNo = ko.observable();
        self.InterventionCategoryId = ko.observable();
        self.SchemeCategoryName = ko.observable();
        self.ShortDetails = ko.observable();
        self.StartDate = ko.observable();
        self.EndDate = ko.observable();

        self.CRFGrant = ko.observable();
        self.DisburseDate = ko.observable(moment());
        self.DisbursedAmount = ko.observable(0.0).extend({ required:true }).extend({ numeric: 2 }).extend({ min: 10 });
        self.DisbursementHistories = ko.observableArray([]);

        self.AvailableAmount = ko.computed(function () {
            if (self.DisbursementHistories().length > 0) {
                var lastrow = self.DisbursementHistories()[self.DisbursementHistories().length - 1];
                // self.players()[i].name
                return lastrow.RemainingFund;
            }
            else
                return self.CRFGrant();
        });
        self.RemainingFund=ko.computed(function () {
            if (self.AvailableAmount() > 0) {
                return (self.AvailableAmount() - self.DisbursedAmount());
            }
        });
        
        
               

        self.InterventionBudgets = ko.observableArray([]);
        self.SourceOfFunds = ko.observableArray([]);
        self.SchemeCategories = ko.observableArray([]);
        

                
        self.LoadSourceOfFunds = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAllSourceOfFund',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.SourceOfFunds(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadSchemeCategory = function () {
           
            return $.ajax({
                type: "GET",
                url: '/ATM/Scheme/getSchemeCategories',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    self.SchemeCategories(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        
        self.saveDisbursement = function () {
            console.log(self.AvailableAmount());
            console.log(self.RemainingFund());
            if (self.RemainingFund() >= 0) {
                $.ajax({
                    type: "POST",
                    url: '/ATM/HouseHold/SaveDisbursement',
                    data: ko.toJSON(this),
                    contentType: "application/json",
                    success: function (data) {
                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);
                        self.LoadDisbursementHistory();
                    },
                    error: function () {
                        $('#successModal').modal('show');
                        $('#successModalText').text(error.statusText);

                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text('Availble fund is less than Disbursed Amount!');
            }

        };

        self.LoadInterventionData = function () {
            if (self.InterventionId() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/GetInterventionData?id=" + self.InterventionId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            self.HouseHoldNo(data.HouseHoldNo);
                            self.HHHeadName(data.HHHeadName);
                            $.when(self.LoadSchemeCategory()).done( function(){
                                self.InterventionCategoryId(data.InterventionCategoryId);
                            });
                            self.SchemeCategoryName(data.SchemeCategoryName);
                            self.InterventionTitle(data.InterventionTitle);
                            self.ShortDetails(data.ShortDetails);
                            self.StartDate(moment(data.StartDate));
                            self.EndDate(moment(data.EndDate));
                            
                            self.CRFGrant(data.CRFGrant);
                            $.each(data.InterventionBudgets, function (index, value) {
                                var aDetail = new InterventionBudget();
                                if (typeof (value) != 'undefined') {
                                    aDetail.LoadData(value);
                                    //console.log("aDetail : " + ko.toJSON(value));
                                    self.InterventionBudgets.push(aDetail);
                                }
                            });
                           
                            
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }
        self.LoadDisbursementHistory = function () {
            if (self.InterventionId() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/Household/LoadDisbursementHistory?interventionid=" + self.InterventionId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {
                            self.DisbursementHistories(data);
                            
                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }

        self.InitialValueLoad = function () {
            self.LoadSchemeCategory();
            self.LoadSourceOfFunds();
            if (self.InterventionId() > 0)
            {
                self.LoadInterventionData();
                self.LoadDisbursementHistory();
            }
            
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });


        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new DisbursementVM();

    vm.InterventionId(vm.queryString("Id"));
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divIntervention')[0]);



});