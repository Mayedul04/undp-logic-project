﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function RiskPriorityVM() {

        var self = this;

        self.Id = ko.observable();
        self.RiskStatementId = ko.observable().extend({ required: true });
        self.RiskStatementName = ko.observable();
        
        self.UPId = ko.observable();
        self.UPName = ko.observable();
       
        self.Likelihood = ko.observable();
        self.Consequence = ko.observable();
        
        
        self.Rating = ko.observable();
        self.PeopleAffected = ko.observable();
        self.LocalEconomyAffected = ko.observable();
        self.PotentialToProperSolution = ko.observable();
        

        self.ParentLocations = ko.observableArray([]);
        self.PotentialToProperSolutions = ko.observableArray([]);
        self.Ratings = ko.observableArray([]);
        self.Statements = ko.observableArray([]);

        self.ReportTypeId = ko.observable();
        self.Link1 = ko.observable();
        //self.Banks = ko.observableArray([]);
        //self.Code = ko.observable();
        self.Link2 = ko.observable();
        self.Link3 = ko.observable();
        self.Title1 = ko.observable('PDF');
        self.Title2 = ko.observable('Excel');
        self.Title3 = ko.observable('Word');

        

        self.Reset = function () {
                self.Id('');
                self.RiskStatementId('');
                self.UPId('');
                self.Likelihood('');
                self.RiskPercentage('');
                self.Consequence('');
                //self.RiskPercentage('');
                self.Rating('');
                self.PeopleAffected('');
                self.LocalEconomyAffected('');
                self.PotentialToProperSolution('');
                //self.PriorityPoint('');
         }

        self.RiskPercentage = ko.computed(function () {
            if (self.Likelihood() > 0 && self.Consequence() > 0)
                return (self.Likelihood() * self.Consequence() * 4);
            else
                return 0;
        });

        self.PriorityPoint = ko.computed(function () {
            if (self.RiskPercentage() > 0 && self.PeopleAffected() > 0 && self.LocalEconomyAffected() > 0 && self.PotentialToProperSolution()>0)
                return ((self.RiskPercentage() * self.PeopleAffected() * self.LocalEconomyAffected() * self.PotentialToProperSolution() * 100)/100000000);
            else
                return 0;
        });

        self.SaveData = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Risk/SaveRiskPriority',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                       // self.AllLocatins();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getRiskStatements = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/getAllStatements?upid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Statements(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getAllData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/getAllPriorityData?upid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    self.Ratings(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        

        self.getLocations = function (level,parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level==4)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.getPotentials = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Risk/getPotentialstoSolution',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.PotentialToProperSolutions(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.UPId.subscribe(function () {
            if (self.UPId() > 0)
            {
                self.getRiskStatements();
                self.getAllData();
            }
                
            
        });

              
        self.EditData = function (data) {

            console.log(data);
            self.Id(data.Id);

            $.when(self.getLocations(4)).done(function () {
                self.UPId(data.UPId);
                self.RiskStatementId(data.RiskStatementId);
            });
           
            self.Likelihood(data.Likelihood);
            self.Consequence(data.Consequence);
            self.Rating(data.Rating);
            self.PeopleAffected(data.PeopleAffected);
            self.PotentialToProperSolution(data.PotentialToProperSolution);
            self.LocalEconomyAffected(data.LocalEconomyAffected);
        }

        self.PrintResult = function (item, event) {
             if (self.UPId()>0)
                 window.open('/ATM/Risk/GetRiskRatingReports?reportTypeId=PDF&upid=' + self.UPId());
             else 
             {
                $('#successModal').modal('show');
                $('#successModalText').text('Please select UP!');
             }
        }

        self.InitialValueLoad = function () {
            self.getLocations(4);
            self.getPotentials();
            //self.getAllStatement();
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        

    }

    var vm = new RiskPriorityVM();
    vm.InitialValueLoad();
    
    ko.applyBindings(vm, $('#divRisk')[0]);



});