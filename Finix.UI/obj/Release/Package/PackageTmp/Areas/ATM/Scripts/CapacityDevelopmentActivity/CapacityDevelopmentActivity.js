﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

   
    function Participants() {
        var self = this;
        self.Id = ko.observable();
        self.CapacityDevelopmentActivityId = ko.observable();
        self.PeopleType = ko.observable('');
        self.Count = ko.observable();

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.CapacityDevelopmentActivityId(data.CapacityDevelopmentActivityId);
            self.PeopleType(data.PeopleType);
            self.Count(data.Count);
        }

    }
   
    function CapacityDevelopmentActivityVM() {

        var self = this;

        self.Id = ko.observable();
        self.LocationId = ko.observable();
        self.LocationName = ko.observable();
        self.EventName = ko.observable('').extend({ required: true, maxLength: "100" });
   
        self.EventTypeId = ko.observable('').extend({ required: true });
        self.EventTypeName = ko.observable();
        self.ActualCost = ko.observable();
        self.Duration = ko.observable();
        self.Venue = ko.observable();
        self.ExecutedBy = ko.observable();
        self.OrganizedBy = ko.observable();
         self.Renarks = ko.observable();
        self.StartDate = ko.observable(moment());
        self.EndDate = ko.observable(moment());

       
        self.EventTypes = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.Participants = ko.observableArray([]);

        self.Reset = function () {
            self.Id('');
            self.EventName('');
            self.ActualCost('');
            self.Duration('');
            self.Venue('');
            self.ExecutedBy('');
            self.OrganizedBy('');
            self.Renarks('');
            self.StartDate(moment());
            self.EndDate(moment());
            self.Participants([]);
        }

        self.addParticipant = function () {
            var emp = new Participants();
            self.Participants.push(emp);
        }
        self.removedParticipants = ko.observableArray([]);
        self.removeParticipant = function (line) {
            if (line.Id() > 0)
                self.removedParticipants.push(line.Id());
            self.Participants.remove(line);
        }

        self.getLocations = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        

        self.LoadEventTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/CapacityDevelopmentActivity/getEventTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.EventTypes(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

      

        self.saveCDAInfo = function () {
             $.ajax({
                type: "POST",
                url: '/ATM/CapacityDevelopmentActivity/SaveapacityDevelopmentActivity',
                data: ko.toJSON(this),
                contentType: "application/json",
                success: function (data) {
                    self.Id(data.Id);
                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                    self.Reset();
                    // self.GetAllDiv();
                    // self.CodeVisible(true);
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.LoadData = function () {

            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: "/ATM/CapacityDevelopmentActivity/LoadDevelopmentActivityData?id=" + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log("Data : " + ko.toJSON(data));
                        if (data != null) {

                            self.Id(data.Id);
                            
                            self.EventName(data.EventName);
                            self.ActualCost(data.ActualCost);
                            self.Duration(data.Duration);
                            self.Venue(data.Venue);
                            self.ExecutedBy(data.ExecutedBy);
                            self.OrganizedBy(data.OrganizedBy);
                            self.Renarks(data.Renarks);
                            self.StartDate(moment(data.StartDate));
                            self.EndDate(moment(data.EndDate));
                            
                            $.when(self.LoadEventTypes()).done(function () {
                                self.EventTypeId(data.EventTypeId);
                                self.EventTypeName(data.EventTypeName);
                            });

                            $.when(self.getLocations(4, null)).done(function () {
                                self.LocationId(data.LocationId);
                                self.LocationName(data.LocationName);
                            });

                          
                            $.each(data.Participants, function (index, value) {
                                var aDetail = new Participants();
                                if (typeof (value) != 'undefined') {
                                    aDetail.LoadData(value);
                                    //console.log("aDetail : " + ko.toJSON(value));
                                    self.Participants.push(aDetail);
                                }
                            });

                        }
                    },
                    error: function (error) {

                        $('#successModal').modal('show');
                        $('#successModalText').text("There is no details data for this Survey!");
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("Please select a HouseHold!");
            }

        }

        self.InitialValueLoad = function () {
            self.getLocations(4, null);
            self.LoadEventTypes();
            // self.GetAllDiv();
            
            if (self.Id() > 0) {
                self.LoadData();
                //self.LoadAllmember();
            }

        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });


        self.queryString = function getParameterByName(name) {
            
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }
    var vm = new CapacityDevelopmentActivityVM();
    vm.Id(vm.queryString("Id"));
    
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divCapacityDevelopmentActivity')[0]);

});