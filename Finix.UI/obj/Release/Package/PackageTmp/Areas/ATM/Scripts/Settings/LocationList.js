﻿$(document).ready(function () {
    function LocationsVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.LocationName = ko.observable(searchString);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        self.Wards = ko.observableArray(wards);
        self.LocationID = ko.observable(locationid);
        
        self.AddNew = function (data) {
            
            var menuInfo = {
                Id: '113_0' ,
                Menu: 'New Location',
                Url: '/ATM/Settings/LocationSetup'
                
            }
            window.parent.AddTabFromExternal(menuInfo);
        }
       
        self.Edit = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '113_' + data.Id,
                Menu: 'Location Information',
                Url: '/ATM/Settings/LocationSetup',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }


    }

    var pavm = new LocationsVM();
    ko.applyBindings(pavm, document.getElementById("activeList"));
});