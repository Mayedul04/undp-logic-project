﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function LandUseVM() {

        var self = this;

        self.Id = ko.observable();
        self.LandTypeID = ko.observable();
        self.LandTypeName = ko.observable();
        self.LandName = ko.observable('').extend({ required: true, pattern: { message: 'Only alphabetical values required.', params: "[^\p{L}\d\s_]", maxLength: "100" } });

        self.LocationId = ko.observable().extend({ required: true });
        self.LocationName = ko.observable();

        self.ParentLocationId = ko.observable();
        self.ParentLocationName = ko.observable();
        
        self.IsAnyChange = ko.observable(false);
        self.Changes = ko.observable();
        self.AnyHazardCreatedBy = ko.observable(false);
        self.CreatedHazardId = ko.observable();
        self.CreatedHazardName = ko.observable();
        self.IsNavigable = ko.observable(false);

        self.PreviouslyAffectedbyDisaster = ko.observable(false);
        self.DisasterId = ko.observable();
        self.DisasterName = ko.observable();
        self.PresentUse = ko.observable();
        self.IsWaterAvailabeThroughOutYear = ko.observable(true);
        self.IsTackenByThirdParty = ko.observable(false);

        self.ParentLocations = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.LandTypes = ko.observableArray([]);
        self.Hazards = ko.observableArray([]);
        self.Lands = ko.observableArray([]);

        self.IsRiver = ko.observable(false);
        self.LandTypeID.subscribe(function () {
            if (self.LandTypeID() == 4)
                self.IsRiver(true);
            else
                self.IsRiver(false);
        });

        self.Reset = function () {
                self.Id('');
                self.LandName('');
                self.LandTypeID('');
                self.ParentLocationId('');
                self.LocationId('');
                self.Changes('');
                self.CreatedHazard('');
                self.PresentUse('');
                
         }

        self.SaveLandUse = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/AgriculturalInfo/SaveLandUse',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                       // self.AllLocatins();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getLandTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getLandTypes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.LandTypes(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.getLocations = function (level,parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level==4)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.ParentLocationId.subscribe(function () {
            if (self.ParentLocationId() > 0)
                self.getLocations(5, self.ParentLocationId());
            else
                self.Locations({});
        });

        self.getHazards = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazards',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Hazards(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getLandList = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getLandList',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Lands(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.editData = function (data) {
            self.Id(data.Id);
            
            self.LandName(data.LandName);
            $.when(self.getLandTypes()).done(function () {
                self.LandTypeID(data.LandTypeID);
            });
            $.when(self.getLocations(4)).done(function () {
                self.ParentLocationId(data.ParentLocationId);
            });
            $.when(self.getLocations(5)).done(function () {
                self.LocationId(data.LocationId);
            });
            self.Changes(data.Changes);
            self.PresentUse(data.PresentUse);
            self.IsAnyChange(data.IsAnyChange);
            self.AnyHazardCreatedBy(data.AnyHazardCreatedBy);
            self.IsNavigable(data.IsNavigable);
            $.when(self.getHazards()).done(function () {
                self.CreatedHazardId(data.CreatedHazard);
                self.DisasterId(data.DisasterId);
            });

            self.PreviouslyAffectedbyDisaster(data.PreviouslyAffectedbyDisaster);
            self.IsWaterAvailabeThroughOutYear(data.IsWaterAvailabeThroughOutYear);
            self.IsTackenByThirdParty(data.IsTackenByThirdParty);

        }

        //self.EditData = function () {
        //    if (self.Id() > 0) {
        //        return $.ajax({
        //            type: "GET",
        //            url: '/ATM/AgriculturalInfo/getLandInfoById?id=' + self.Id(),
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            success: function (data) {
        //                console.log("Data :" + ko.toJSON(data));
        //                self.LandName(data.LandName);
        //                $.when(self.getLandTypes()).done(function () {
        //                    self.LandTypeID(data.LandTypeID);
        //                });
        //                $.when(self.getLocations(4)).done(function () {
        //                    self.ParentLocationId(data.ParentLocationId);
        //                });
        //                $.when(self.getLocations(5)).done(function () {
        //                    self.LocationId(data.LocationId);
        //                });
        //                self.Changes(data.Changes);
        //                self.PresentUse(data.PresentUse);
        //                self.IsAnyChange(data.IsAnyChange);
        //                self.AnyHazardCreatedBy(data.AnyHazardCreatedBy);
        //                self.IsNavigable(data.IsNavigable);
        //                $.when(self.getHazards()).done(function () {
        //                    self.CreatedHazard(data.CreatedHazard);
        //                });
                        
        //                self.PreviouslyAffectedbyDisaster(data.PreviouslyAffectedbyDisaster);
        //                self.IsWaterAvailabeThroughOutYear(data.IsWaterAvailabeThroughOutYear);
        //                self.IsTackenByThirdParty(data.IsTackenByThirdParty);
                        
        //            },
        //            error: function (error) {
        //                $('#successModal').modal('show');
        //                $('#successModalText').text(error.statusText);
        //            }
        //        });

        //    }
        //};

     
        self.InitialValueLoad = function () {
            self.getLandTypes();
            self.getLocations(4);
            self.getHazards();
            self.getLandList();
            //self.getParents();
            //self.errors.showAllMessages(true);
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        

    }

    var vm = new LandUseVM();
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divLandUse')[0]);



});