﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function HHIndicator() {

        var self = this;
        self.Id=ko.observable();
        self.IndicatorId = ko.observable();
        self.IndicatorTitle = ko.observable();
        self.RiskId = ko.observable();
        self.PotentialImpact = ko.observable();
        self.Score = ko.observable();
        self.IsSelected = ko.observable(false);

        self.LoadData = function (data) {
            self.Id(data.Id);
            self.IndicatorId(data.IndicatorId);
            self.IndicatorTitle(data.IndicatorTitle);
            self.RiskId(data.RiskId);
            self.PotentialImpact(data.PotentialImpact);
            self.Score(data.Score);
            self.IsSelected(data.IsSelected);
            
        }
    }

    function HHAdaptiveCapacityVM() {

        var self = this;

        self.Id = ko.observable();
        self.HouseHoldNo = ko.observable();
        self.PotentialImpact = ko.observable();
        //self.Likelihood = ko.observable();
        //self.Consequence = ko.observable();
        //self.Consequences = ko.observable();
        self.Rating = ko.observable();


        self.Indicators = ko.observableArray([]);
        self.HHIndicatorScores = ko.observableArray([]);


        
        self.LoadIndicators = function () {
            return $.ajax({
                type: "GET",
                url: "/ATM/Settings/GetIndicatorsByType?typeid=1",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // console.log(ko.toJSON(data));
                    if (data != null) {
                        self.Indicators(data);
                    }
                },
                error: function (error) {
                    $('#successModal').modal('show');
                    $('#successModalText').text("There is no details data for this Type!");
                }
            });
        }
        self.LoadRiskData = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/HouseHold/GetHHRiskData?piid=' + self.Id(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log(ko.toJSON(data));
                        self.HouseHoldNo(data.HouseHoldNo);
                        self.PotentialImpact(data.PotentialImpact);
                        self.Rating(data.Rating);
                        if (data.HHIndicatorScores.length > 0) {
                            self.HHIndicatorScores([]);
                            $.each(data.HHIndicatorScores, function (index, value) {
                                var aDetail = new HHIndicator();
                                    if (typeof (value) != 'undefined') {
                                        aDetail.LoadData(value);
                                        //console.log("aDetail : " + ko.toJSON(value));
                                        self.HHIndicatorScores.push(aDetail);
                                    }
                                });
                        }
                        else
                            self.LoadIndicators();
                    },
                    error: function (error) {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
        }

        self.saveAdaptiveCapacity = function () {
            //console.log(ko.toJSON(this));
            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SaveAdaptiveCapacityCalculations',
                data: ko.toJSON(this),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                    //self.getPotentials();
                    
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        

        self.LoadHHIndicators = ko.computed(function () {
            //self.HHIndicatorScores([]);
            if (self.Indicators().length > 0) {
                for (var i = 0; i < self.Indicators().length ; i++) {
                    var item = new HHIndicator();
                    var indicator = self.Indicators()[i];
                    item.IsSelected(false);
                    item.IndicatorId(indicator.Id);
                    item.IndicatorTitle(indicator.IndicatorName);
                    item.Score(indicator.Weight * 1);
                    self.HHIndicatorScores.push(item);
                }
            }
        });

       
        self.InitialValueLoad = function () {
            

            if (self.Id() > 0) {
                self.LoadRiskData();
                //self.getPotentials();
            }
            
        }


        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new HHAdaptiveCapacityVM();
    vm.Id(vm.queryString("PIId"));
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divCapacity')[0]);



});