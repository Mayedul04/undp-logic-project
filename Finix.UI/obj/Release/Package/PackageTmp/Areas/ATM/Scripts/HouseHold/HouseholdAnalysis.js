﻿$(document).ready(function () {
    function HouseholdAnalysisVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.UPs = ko.observableArray(ups);
        self.UP = ko.observable(up);
        self.Wards = ko.observableArray(wards);
        self.LocationID = ko.observable(locationid);
        self.HouseHoldNo = ko.observable(searchString);
        self.Address = ko.observable();

        self.Exposure = function (data) {
            var parameters = [{
                Name: 'HHId',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '101_' + data.Id,
                Menu: 'HH Exposure-Sensitivity Analysis',
                Url: '/ATM/HouseHold/HHExposure',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.PI = function (data) {
            var parameters = [{
                Name: 'HHId',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '103_'+ data.Id,
                Menu: 'HH Potential Impact',
                Url: '/ATM/HouseHold/HHPotentialImpact',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        
        

    }

    var pavm = new HouseholdAnalysisVM();
    ko.applyBindings(pavm, document.getElementById("householdAnalysisList"));
});