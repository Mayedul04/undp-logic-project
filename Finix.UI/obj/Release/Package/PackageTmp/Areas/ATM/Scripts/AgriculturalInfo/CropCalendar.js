﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function CropCaledarVM() {

        var self = this;

        self.Id = ko.observable();
        self.CropId = ko.observable().extend({ required: true });
        self.CropName = ko.observable();
        self.ParentLocationId = ko.observable();
        self.ParentLocationName = ko.observable();
        self.LocationId = ko.observable().extend({ required: true });
        self.LocationName = ko.observable();
       
        
        self.StartMonthId = ko.observable();
        self.EndMonthId = ko.observable();
        self.StartMonthName = ko.observable();
        self.EndMonthName = ko.observable();

        
        self.Crops = ko.observableArray([]);
        self.Months = ko.observableArray([]);
        self.ParentLocations = ko.observableArray([]);
        self.Locations = ko.observableArray([]);
        self.Mappings = ko.observableArray([]);

        self.getLocations = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level == 3)
                        self.ParentLocations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.ParentLocationId.subscribe(function () {
            if (self.ParentLocationId() > 0)
                self.getLocations(4, self.ParentLocationId());
            else
                self.Locations({});
        });

        self.LocationId.subscribe(function () {
            if (self.LocationId() > 0)
                self.CropCalender();
            else
                self.CropCalender();
        });


        self.Reset = function () {
                self.Id('');
                self.CropName('');
                self.CropId('');
                self.StartMonthId('');
                self.EndMonthId('');
                
                
         }

        self.SaveCropCalendar = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/AgriculturalInfo/SaveCropCalendar',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.CropCalender();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getCrops = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getCrops',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Crops(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        
        self.getMonths = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getMonths',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Months(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.CropCalender = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getCropCalendar?locid=' + self.LocationId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Mappings(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.editData = function (data) {
            self.Id(data.Id);
            $.when(self.getCrops()).done(function () {
                self.CropId(data.CropId);
            });
            $.when(self.getLocations(3)).done(function () {
                self.ParentLocationId(data.ParentLocationId);
            });
            $.when(self.getLocations(4),self.ParentLocationId()).done(function () {
                self.LocationId(data.LocationId);
            });
            $.when(self.getMonths()).done(function () {
                self.StartMonthId(data.StartMonthId);
                self.EndMonthId(data.EndMonthId);
            });
            
        }
        
        self.InitialValueLoad = function () {
            self.getCrops();
            self.getMonths();
            self.getLocations(3);
            self.CropCalender();
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        

    }

    var vm = new CropCaledarVM();
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divLandUse')[0]);



});