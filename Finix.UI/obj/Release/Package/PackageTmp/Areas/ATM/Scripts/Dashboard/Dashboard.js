﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="~/Content/customassets/js/jquery-3.1.1.min.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />
///<reference path="~/Content/customassets/js/highcharts.js" />
///<reference path="~/Content/customassets/js/exporting.js" />
///<reference path="~/Content/customassets/js/export-data.js" />


$(document).ready(function () {

    function DashboardVM() {

        var self = this;

        self.DivisionId = ko.observable(3); //Default Rangpur Division
        self.DistrictId = ko.observable();
        self.DistrictName = ko.observable();
        self.IsReady = ko.observable(false);

        self.ThanaId = ko.observable(13);
        self.ThanaName = ko.observable('Chilmari');
        
        self.UPId = ko.observable(15);
        self.UPName = ko.observable('Thanahat');
       
        self.Employmentdata = ko.observableArray([]);
        self.EmploymentCategory = ko.observableArray([]);

        self.LandTypeData = ko.observableArray([{ name: 'test', y: 50 }, {name: 'test2',y:20}]);
        self.PovertySituationData = ko.observableArray([{ name: 'test', y: 50 }, { name: 'test2', y: 20 }]);
        self.HazardVennData = ko.observableArray([]);

        self.Districts = ko.observableArray([]);
        self.Thanas = ko.observableArray([]);
        self.UPs = ko.observableArray([]);
        
        self.CropCalendar = ko.observableArray([]);
        self.HazardCalendar = ko.observableArray([]);
        
        self.getEmploymentData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Dashboard/GetEmployementDataUPWise?locid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.EmploymentCategory([]);
                    self.Employmentdata([]);
                    $.each(data, function (index, value) {
                        
                        self.EmploymentCategory.push('' + value.EmploymentName+ '');
                        self.Employmentdata.push(value.PeopleEngaged);
                        
                    });
                    
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getLandTypeData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Dashboard/GetLandTypeInfoUPWise?locid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                   
                    self.LandTypeData([]);
                    $.each(data, function (index, value) {
                        self.LandTypeData.push({ name: value.LandTypeName, y: value.Area });
                    });
                   
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getPovertySituationData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Dashboard/GetPovertySituationUPWise?locid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    self.PovertySituationData([]);
                    $.each(data, function (index, value) {
                        self.PovertySituationData.push({ name: value.PovertySituationName, y: value.PopulationPercentage });
                    });

                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getHazardVennData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Dashboard/GetHazardVennDiagramUPWise?locid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    self.HazardVennData([]);
                    $.each(data, function (index, value) {
                        //var marker = new Marker(value.radius, value.FillColor, value.LineColor);
                        self.HazardVennData.push({ color: ''+value.FillColor+'', name:''+ value.HazardName+'', data: [{ x: value.xAxis, y: value.Consequence, marker: { radius: value.radius, fillColor: ''+value.FillColor+'', lineColor: ''+value.LineColor+'', lineWidth: 1 } }] });
                    });
                    
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.DrawEmploymentChart = function () {
            $('#employmentType').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Employment Type'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: self.EmploymentCategory(),
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Employment (%)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true
                    //useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Employment Type',
                    data: self.Employmentdata()

                }]
            });
        }
        
        self.DrawLandTypeChart = function () {
            $('#landType').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Land Type'
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true

                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Land Type',
                    colorByPoint: true,
                    data: self.LandTypeData()
                }]
            });
        }

        self.DrawPovertySituationChart = function () {
            $('#povertySituation').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Poverty Situation'
                },
                tooltip: {
                    // pointFormat: '{name}: <b>{point.y:1f}%</b>',
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true

                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Poverty Situation',
                    colorByPoint: true,
                    data: self.PovertySituationData()
                }]
            });
        }

        self.DrawHazardVenn = function () {
           
            $('#hazardven').highcharts({
                chart: {
                       type: 'scatter'
                  },
                title: {
                    text: 'Hazard Venn Diagram'
                },

                credits: {
                    enabled: false
                },
                legend: { enabled: true },
                tooltip: { enabled: true },
                plotOptions: {
                    series: {
                        shadow: true,
                        borderWidth: 0,
                        marker: {
                            symbol: 'circle'
                        }
                    }
                },
                xAxis: {
                    min: 1,
                    max: 12,
                    lineWidth: 2,
                    gridLineWidth: 1,
                    gridLineColor: '#d8d8d4',
                    title: {
                        text: 'Month'
                    },
                    categories: ['','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    crosshair: true,
                    labels: { enabled: true },
                    tickLength: 0
                },
                yAxis: {
                    min: 0,
                    max: 5,
                    lineWidth: 2,
                    gridLineWidth: 1,
                    gridLineColor: '#d8d8d4',
                    title: {
                        text: 'Concequence'
                    },
                    labels: { enabled: true }
                },

                series: self.HazardVennData()

            });
        }

        self.DrawDisburseBar = function () {
            
                $('#disbursement').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Disbursement'
                },

                xAxis: {
                    categories: ['Thanahat', 'Raniganj'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Amount Disbursed ($)',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' $'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Disbursed Amount',
                    data: [

                    ['Thanahat', 20000],
                    ['Raniganj', 30000]

                    ],
                }]
            });
        }
        
        self.getCropCalender = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getCropCalendar',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.CropCalendar(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getLocations = function (level,parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level == 2)
                        self.Districts(data);
                    if(level == 3)
                        self.Thanas(data);
                    if (level == 4)
                        self.UPs(data);
                    
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getHazardCalendar = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/AgriculturalInfo/getHazardCalendar',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.HazardCalendar(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        
        self.DistrictId.subscribe(function () {
            if (self.DistrictId() > 0) {
                self.getLocations(3, self.DistrictId());
                //self.ThanaId(13);
                //self.ThanaName('Chilmari');
            }
        });

        self.ThanaId.subscribe(function () {
            if (self.ThanaId() > 0) {
                self.getLocations(4, self.ThanaId());
                //self.UPId(15);
                //self.UPName('Thanahat');
            }
        });

        self.UPId.subscribe(function () {
            if (self.UPId()== 15)
            {
                self.IsReady(true);
                self.getCropCalender();
                self.getHazardCalendar();
                self.DrawDisburseBar();
                $.when(self.getEmploymentData()).done(function () {
                    self.DrawEmploymentChart();
                });
                $.when(self.getLandTypeData()).done(function () {
                    self.DrawLandTypeChart();
                });
                $.when(self.getPovertySituationData()).done(function () {
                    self.DrawPovertySituationChart();
                });
                $.when(self.getHazardVennData()).done(function () {
                    console.log(ko.toJSON(self.HazardVennData()));
                    self.DrawHazardVenn();
                });
            }
        });

              
        

        self.InitialValueLoad = function () {
            self.getLocations(2, self.DivisionId());
            
            
        }
        
       
    }

    var vm = new DashboardVM();
    vm.InitialValueLoad();
    
    ko.applyBindings(vm, $('#divDash')[0]);



});