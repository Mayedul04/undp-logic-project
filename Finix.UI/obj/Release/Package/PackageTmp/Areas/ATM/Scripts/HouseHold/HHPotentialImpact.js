﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function HHPotentialImpactVM() {

        var self = this;

        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable();
        self.PotentialImpact = ko.observable('').extend({ required: true });
        
        self.ElementTypeId = ko.observable();
        self.ElementTypeName = ko.observable();
        self.Likelihood = ko.observable();
        self.Consequence = ko.observable();
        self.Consequences = ko.observable('').extend({ required: true });
        
        
        
        self.ElementTypes = ko.observableArray([]);
        self.PotentialToProperSolutions = ko.observableArray([]);

        self.Rating = ko.computed(function () {
            if (self.Likelihood() > 0 && self.Consequence() > 0)
                return (self.Likelihood() * self.Consequence());
            else
                return 0;
        });

        self.Reset = function () {
            self.Id('');
            self.ElementTypeId('');
            self.PotentialImpact('');
            self.Consequences('');
            self.Likelihood('');
            self.Consequence('');
        }
        
        self.getElementTypes = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/getSelectedElementTypes?hhid=' + self.HHId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.ElementTypes(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> No Element type found");
                }
            });
        }
       
        self.getPotentials = function () {
            if (self.HHId() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/HouseHold/getAllPotentailImpactbyHHId?hhid=' + self.HHId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        self.PotentialToProperSolutions(data); //Put the response in ObservableArray
                    },
                    error: function (error) {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text("There is no data!");
            }
        }

        self.LoadHouseHoldData = function () {
            if (self.HHId() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/ATM/HouseHold/GetHouseHoldBasicData?hhid=' + self.HHId(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //console.log(ko.toJSON(data));
                        self.HouseHoldNo(data.HouseHoldNo);

                    },
                    error: function (error) {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
        }
       
        self.savePI = function () {

            $.ajax({
                type: "POST",
                url: '/ATM/HouseHold/SavePontentialImpact',
                data: ko.toJSON(this),
                contentType: "application/json",
                success: function (data) {

                    $('#successModal').modal('show');
                    $('#successModalText').text(data.Message);
                    self.getPotentials();
                    self.Reset();
                },
                error: function () {
                    $('#successModal').modal('show');
                    $('#successModalText').text(error.statusText);

                }
            });

        };

        self.EditData = function (data) {
            self.Id(data.Id);
            $.when(self.LoadHouseHoldData()).done(function () {
                self.HHId(data.HHId);
            });
            $.when(self.getElementTypes()).done(function () {
                self.ElementTypeId(data.ElementTypeId);
                self.ElementTypeName(data.ElementTypeName);
            });
            self.PotentialImpact(data.PotentialImpact);
            self.Likelihood(data.Likelihood);
            self.Consequences(data.Consequences);
            self.Consequence(data.Consequence);
        }

        self.InitialValueLoad = function () {
            self.getElementTypes();

            if (self.HHId() > 0) {
                self.LoadHouseHoldData();
                self.getPotentials();
            }
        }

        self.Calculations = function (data) {
            var parameters = [{
                Name: 'PIId',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '106_' + data.Id,
                Menu: 'HH Adaptive Capacity',
                Url: '/ATM/HouseHold/AdaptiveCapacity',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };

    }

    var vm = new HHPotentialImpactVM();
    vm.HHId(vm.queryString("HHId"));
    vm.InitialValueLoad();
    
    ko.applyBindings(vm, $('#divPotentialImpact')[0]);



});