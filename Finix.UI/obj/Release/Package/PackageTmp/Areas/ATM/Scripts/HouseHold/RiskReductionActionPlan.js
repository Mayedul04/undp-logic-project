﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function RiskReductionActionPlanVM() {

        var self = this;

        self.Id = ko.observable();
        self.HHId = ko.observable();
        self.HouseHoldNo = ko.observable();
        self.IsThereAnyCommunityInitiative = ko.observable();
        self.WhatAreTheInitiatives = ko.observable();
        self.WhatAreTheBenefitsReceivingByTheHH = ko.observable();
        self.IsThereAnyThreatForTheHHFromTheCommunityInitiative = ko.observable();
        self.WhatAreTheThreats = ko.observable();
        self.ImpactsOfCommunityLevelInitiativeOnHHThreatIdentified = ko.observable();
        self.GapsForHHIntervention = ko.observable();
        self.LivelihoodRiskReductionPlan = ko.observable();
        self.LifeAndAssetLossReductionPlan = ko.observable();
        self.PlanForSupportingNeighborhoodThreats = ko.observable();

        self.Reset = function () {
            self.Id('');
            self.HouseHoldNo('');
            self.IsThereAnyCommunityInitiative('');
            self.WhatAreTheInitiatives('');
            self.WhatAreTheBenefitsReceivingByTheHH('');
            self.IsThereAnyThreatForTheHHFromTheCommunityInitiative('');
            self.WhatAreTheThreats('');
            self.ImpactsOfCommunityLevelInitiativeOnHHThreatIdentified('');
            self.GapsForHHIntervention('');
            self.LivelihoodRiskReductionPlan('');
            self.LifeAndAssetLossReductionPlan('');
            self.PlanForSupportingNeighborhoodThreats('');

        }

        self.SaveRiskReductionActionPlan = function () {
            if (self.IsValid()) {
                $.ajax({
                    type: "POST",
                    url: '/ATM/HouseHold/SaveRiskReductionActionPlan',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                       // self.AllCrops();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }
        self.LoadHouseHoldData = function () {
            //debugger;
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/GetHouseHoldBasicData?hhid=' + self.HHId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(ko.toJSON(data));
                    self.HouseHoldNo(data.HouseHoldNo);
                    
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.LoadRRAPData = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/HouseHold/getRRAPData?hhid=' + self.HHId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data)
                    {
                        self.Id(data.Id);
                        self.HHId(data.HHId);
                        //self.HouseHoldNo(data.HouseHoldNo);
                        self.IsThereAnyCommunityInitiative(data.IsThereAnyCommunityInitiative);
                        self.WhatAreTheInitiatives(data.WhatAreTheInitiatives);
                        self.WhatAreTheBenefitsReceivingByTheHH(data.WhatAreTheBenefitsReceivingByTheHH);
                        self.IsThereAnyThreatForTheHHFromTheCommunityInitiative(data.IsThereAnyThreatForTheHHFromTheCommunityInitiative);
                        self.WhatAreTheThreats(data.WhatAreTheThreats);
                        self.ImpactsOfCommunityLevelInitiativeOnHHThreatIdentified(data.ImpactsOfCommunityLevelInitiativeOnHHThreatIdentified);
                        self.GapsForHHIntervention(data.GapsForHHIntervention);
                        self.LivelihoodRiskReductionPlan(data.LivelihoodRiskReductionPlan);
                        self.LifeAndAssetLossReductionPlan(data.LifeAndAssetLossReductionPlan);
                        self.PlanForSupportingNeighborhoodThreats(data.PlanForSupportingNeighborhoodThreats);
                    }
                        
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

       

        self.InitialValueLoad = function () {
            if (self.HHId() > 0)
            {
                self.LoadHouseHoldData();
                self.LoadRRAPData();
            }
                
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });
        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };
    }

    var vm = new RiskReductionActionPlanVM();
    vm.HHId(vm.queryString("HHId"));
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divRiskReductionActionPlan')[0]);



});