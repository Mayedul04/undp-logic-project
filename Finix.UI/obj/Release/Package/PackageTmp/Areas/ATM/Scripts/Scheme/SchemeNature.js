﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function SchemeNatureVM() {

        var self = this;

        self.Id = ko.observable();
        self.NatureName = ko.observable('').extend({ required: true, pattern: { message: 'Only alphabetical values required.', params: "[^\p{L}\d\s_]", maxLength: "100" } });

        self.Natures = ko.observableArray([]);

        self.Reset = function () {
            self.Id('');
            self.NatureName('');

        }

        self.SaveNature = function () {
            if (self.IsValid()) {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Scheme/SaveSchemeNature',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.AllNatures();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }


        self.AllNatures = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Scheme/getSchemeNatures',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data)
                        self.Natures(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.editNature = function (data) {
            self.Id(data.Id);
            self.NatureName(data.NatureName);


        }

        self.InitialValueLoad = function () {
            self.AllNatures();

        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

    }

    var vm = new SchemeNatureVM();
    vm.InitialValueLoad();

    ko.applyBindings(vm, $('#divScheme')[0]);



});