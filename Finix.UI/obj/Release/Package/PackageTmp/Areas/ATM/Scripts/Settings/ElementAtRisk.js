﻿/// <reference path="../knockout-3.4.0.debug.js" />
/// <reference path="../jquery-2.1.4.js" />
/// <reference path="../finix.util.js" />
/// <reference path="~/Scripts/knockout.validation.min.js" />

$(document).ready(function () {

    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true
    });

    function ElementRiskVM() {

        var self = this;

        self.Id = ko.observable();
        self.ElementId = ko.observable().extend({ required: true});
        self.ElementName = ko.observable();
        
        self.UPId = ko.observable().extend({ required: true });
        self.UPName = ko.observable();

        self.Exposure = ko.observable();
        self.Likelihood = ko.observable();
        self.Consequences = ko.observable();
        
        self.HazardId = ko.observable();
        self.HazardName = ko.observable();
        self.ReportTypeId = ko.observable();
        
        self.Locations = ko.observableArray([]);
        self.Elements = ko.observableArray([]);
        self.Hazards = ko.observableArray([]);
        self.ElementList = ko.observableArray([]);

        self.Reset = function () {
                self.Id('');
                self.ElementId('');
                self.UPId('');
                self.HazardId('');
                self.Exposure('');
                self.Likelihood('');
                self.Consequences('');
        }
        self.UPId.subscribe(function () {
            if (self.UPId() > 0)
            {
                self.getElements();
                self.getHazards();
            }
        });

        self.SaveData = function () {
            if (self.IsValid())
            {
                $.ajax({
                    type: "POST",
                    url: '/ATM/Settings/SaveElementAtRisk',
                    data: ko.toJSON(self),
                    contentType: "application/json",
                    success: function (data) {

                        $('#successModal').modal('show');
                        $('#successModalText').text(data.Message);

                        self.getAllElementsAtRisk();
                        self.Reset();
                    },
                    error: function () {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            }
            else {
                $('#successModal').modal('show');
                $('#successModalText').text(error.status + "<--save and--> " + error.statusText);
            }
        }

        self.getElements = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getElementsByLocation?locationid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Elements(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getLocations = function (level,parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/GetLocationsByLevel?level=' + level + '&parent=' + parent,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (level==4)
                        self.Locations(data);
                    else
                        self.Locations(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        self.getAllElementsAtRisk = function (level, parent) {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/getAllElementsAtRisk',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(data);
                    self.ElementList(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        

        self.getHazards = function () {
            return $.ajax({
                type: "GET",
                url: '/ATM/Settings/AllHazardsByLocation?locationid=' + self.UPId(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Hazards(data); //Put the response in ObservableArray
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }

        

        self.LoadData = function (data) {
            console.log("Data :" + ko.toJSON(data));
            self.Id(data.Id);
                        
            $.when(self.getLocations(4)).done(function () {
                self.UPId(data.UPId);
            });
            $.when(self.getElements()).done(function () {
                self.ElementId(data.ElementId);
            });
            $.when(self.getHazards()).done(function () {
                self.HazardId(data.HazardId);
            });
            self.Exposure(data.Exposure);
            self.Likelihood(data.Likelihood);
            self.Consequences(data.Consequences);
        };

        self.PrintResult = function (item, event) {
            if(self.UPId()>0)
                window.open('/ATM/Risk/GetElementAtRiskReports?reportTypeId=PDF&upid=' + self.UPId());
            else 
            {
                $('#successModal').modal('show');
                $('#successModalText').text('Please select UP!');
            }
        }

     
        self.InitialValueLoad = function () {
            self.getLocations(4);
            //self.getElements();
            //self.getHazards();
            self.getAllElementsAtRisk();
            //self.errors.showAllMessages(true);
        }

        self.errors = ko.validation.group(self);
        self.IsValid = ko.computed(function () {
            var err = self.errors().length;
            if (err == 0)
                return true;
            return false;
        });

        

    }

    var vm = new ElementRiskVM();
    vm.InitialValueLoad();
    ko.applyBindings(vm, $('#divElement')[0]);



});