/// <autosync enabled="true" />
/// <reference path="../areas/atm/scripts/agriculturalinfo/agriculturallandinfo.js" />
/// <reference path="../areas/atm/scripts/agriculturalinfo/agriculturallandlist.js" />
/// <reference path="../areas/atm/scripts/agriculturalinfo/cropcalendar.js" />
/// <reference path="../areas/atm/scripts/agriculturalinfo/hazardlocationmapping.js" />
/// <reference path="../areas/atm/scripts/agriculturalinfo/landuse.js" />
/// <reference path="../areas/atm/scripts/capacitydevelopmentactivity/capacitydevelopmentactivity.js" />
/// <reference path="../areas/atm/scripts/capacitydevelopmentactivity/cdalist.js" />
/// <reference path="../areas/atm/scripts/capacitydevelopmentactivity/eventtype.js" />
/// <reference path="../areas/atm/scripts/dashboard/dashboard.js" />
/// <reference path="../areas/atm/scripts/household/dashboard.js" />
/// <reference path="../areas/atm/scripts/household/disbursement.js" />
/// <reference path="../areas/atm/scripts/household/energysource.js" />
/// <reference path="../areas/atm/scripts/household/hhadaptivecapacity.js" />
/// <reference path="../areas/atm/scripts/household/hhexposuresensitivity.js" />
/// <reference path="../areas/atm/scripts/household/hhpotentialimpact.js" />
/// <reference path="../areas/atm/scripts/household/hhwaitinglist.js" />
/// <reference path="../Areas/ATM/Scripts/HouseHold/HouseholdAnalysis.js" />
/// <reference path="../Areas/ATM/Scripts/HouseHold/HouseholdInformation.js" />
/// <reference path="../areas/atm/scripts/household/householdlist.js" />
/// <reference path="../areas/atm/scripts/household/householdsurvey.js" />
/// <reference path="../areas/atm/scripts/household/intervention.js" />
/// <reference path="../areas/atm/scripts/household/material.js" />
/// <reference path="../areas/atm/scripts/household/proposedhouseholdlist.js" />
/// <reference path="../areas/atm/scripts/household/riskreductionactionplan.js" />
/// <reference path="../areas/atm/scripts/household/vulnerablelist.js" />
/// <reference path="../areas/atm/scripts/riskstatement/optionactionplan.js" />
/// <reference path="../areas/atm/scripts/riskstatement/riskpriority.js" />
/// <reference path="../Areas/ATM/Scripts/RiskStatement/RiskReductionOption.js" />
/// <reference path="../areas/atm/scripts/riskstatement/riskstatement.js" />
/// <reference path="../Areas/ATM/Scripts/Scheme/CRFDashboard.js" />
/// <reference path="../Areas/ATM/Scripts/Scheme/PBCRGSchemeImplementation.js" />
/// <reference path="../areas/atm/scripts/scheme/schemecategory.js" />
/// <reference path="../Areas/ATM/Scripts/Scheme/SchemeImplementation.js" />
/// <reference path="../areas/atm/scripts/scheme/schemelist.js" />
/// <reference path="../areas/atm/scripts/scheme/schemenature.js" />
/// <reference path="../areas/atm/scripts/settings/agriculturaltools.js" />
/// <reference path="../areas/atm/scripts/settings/crops.js" />
/// <reference path="../areas/atm/scripts/settings/cultivationseason.js" />
/// <reference path="../Areas/ATM/Scripts/Settings/Diagram.js" />
/// <reference path="../areas/atm/scripts/settings/element.js" />
/// <reference path="../areas/atm/scripts/settings/elementatrisk.js" />
/// <reference path="../areas/atm/scripts/settings/elementlist.js" />
/// <reference path="../areas/atm/scripts/settings/elementtype.js" />
/// <reference path="../areas/atm/scripts/settings/ethnicminority.js" />
/// <reference path="../areas/atm/scripts/settings/hazardsetup.js" />
/// <reference path="../areas/atm/scripts/settings/indicator.js" />
/// <reference path="../areas/atm/scripts/settings/indicatortype.js" />
/// <reference path="../areas/atm/scripts/settings/landtype.js" />
/// <reference path="../areas/atm/scripts/settings/location.js" />
/// <reference path="../areas/atm/scripts/settings/locationlist.js" />
/// <reference path="../areas/atm/scripts/settings/sourceoffund.js" />
/// <reference path="../areas/atm/scripts/settings/supporttype.js" />
/// <reference path="../areas/atm/scripts/settings/watersource.js" />
/// <reference path="../areas/atm/scripts/vulnerability/localeconomyvulnerability.js" />
/// <reference path="../areas/atm/scripts/vulnerability/populationgroup.js" />
/// <reference path="../areas/atm/scripts/vulnerability/populationvulnerability.js" />
/// <reference path="../areas/atm/scripts/vulnerability/sensitivityanalysis.js" />
/// <reference path="../Areas/Auth/Scripts/applicationCreate.js" />
/// <reference path="../Areas/Auth/Scripts/companyCreate.js" />
/// <reference path="../Areas/Auth/Scripts/many2many.js" />
/// <reference path="../Areas/Auth/Scripts/menu.js" />
/// <reference path="../Areas/Auth/Scripts/module.js" />
/// <reference path="../Areas/Auth/Scripts/Role/proxy-role.js" />
/// <reference path="../Areas/Auth/Scripts/Role/role.js" />
/// <reference path="../Areas/Auth/Scripts/Role/role-permission.js" />
/// <reference path="../Areas/Auth/Scripts/submodule.js" />
/// <reference path="../Areas/Auth/Scripts/Task.js" />
/// <reference path="../Areas/Auth/Scripts/User/userDetails.js" />
/// <reference path="../Areas/Auth/Scripts/User/UserList.js" />
/// <reference path="../Areas/Auth/Scripts/user-info.js" />
/// <reference path="../Areas/Auth/Scripts/validate.js" />
/// <reference path="../Content/customassets/js/custom.js" />
/// <reference path="../content/customassets/js/export-data.js" />
/// <reference path="../content/customassets/js/exporting.js" />
/// <reference path="../content/customassets/js/highcharts.js" />
/// <reference path="../Content/customassets/js/infragistics.core.js" />
/// <reference path="../Content/customassets/js/infragistics.dv.js" />
/// <reference path="../Content/customassets/js/infragistics.lob.js" />
/// <reference path="../Content/customassets/js/infragistics.ui.combo.knockout-extensions.js" />
/// <reference path="../Content/customassets/js/infragistics.ui.datachart.knockout-extensions.js" />
/// <reference path="../Content/customassets/js/infragistics.ui.editors.knockout-extensions.js" />
/// <reference path="../Content/customassets/js/jquery.animate-colors-min.js" />
/// <reference path="../content/customassets/js/jquery-3.1.1.min.js" />
/// <reference path="../Content/customassets/js/jssor.slider.min.js" />
/// <reference path="../Content/customassets/js/jssor.slider.mini.js" />
/// <reference path="../content/customassets/js/knockout.validation.js" />
/// <reference path="../content/customassets/js/loader.js" />
/// <reference path="../Content/customassets/plugins/icheck.js" />
/// <reference path="../Content/highchecktree/js/highchecktree.js" />
/// <reference path="../Content/themes/redmond/jquery-ui.js" />
/// <reference path="../Content/tinymce/js/tinymce/jquery.tinymce.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/advlist/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/anchor/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/autolink/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/autoresize/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/autosave/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/bbcode/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/charmap/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/code/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/codesample/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/colorpicker/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/contextmenu/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/directionality/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/emoticons/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/example/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/example_dependency/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/fullpage/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/fullscreen/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/hr/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/image/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/imagetools/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/importcss/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/insertdatetime/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/legacyoutput/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/link/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/lists/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/media/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/nonbreaking/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/noneditable/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/pagebreak/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/paste/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/preview/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/print/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/save/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/searchreplace/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/spellchecker/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/tabfocus/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/table/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/template/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/textcolor/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/textpattern/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/toc/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/visualblocks/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/visualchars/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/plugins/wordcount/plugin.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/themes/inlite/theme.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/themes/modern/theme.min.js" />
/// <reference path="../Content/tinymce/js/tinymce/tinymce.min.js" />
/// <reference path="../customassets/js/custom.js" />
/// <reference path="../customassets/js/infragistics.core.js" />
/// <reference path="../customassets/js/infragistics.dv.js" />
/// <reference path="../customassets/js/infragistics.lob.js" />
/// <reference path="../customassets/js/infragistics.ui.combo.knockout-extensions.js" />
/// <reference path="../customassets/js/infragistics.ui.datachart.knockout-extensions.js" />
/// <reference path="../customassets/js/infragistics.ui.editors.knockout-extensions.js" />
/// <reference path="../customassets/js/jquery.animate-colors-min.js" />
/// <reference path="../customassets/js/jssor.slider.min.js" />
/// <reference path="../customassets/js/jssor.slider.mini.js" />
/// <reference path="../customassets/plugins/icheck.js" />
/// <reference path="app/util.js" />
/// <reference path="app/validate.js" />
/// <reference path="appscripts/validate.js" />
/// <reference path="bootstrap.js" />
/// <reference path="bootstrap-confirm.js" />
/// <reference path="bootstrap-datetimepicker.js" />
/// <reference path="bootstrap-multiselect.js" />
/// <reference path="bootstrap-table.js" />
/// <reference path="bootstrap-tree.js" />
/// <reference path="bootstrapvalidator.min.js" />
/// <reference path="cryptojs/aes.js" />
/// <reference path="cryptojs/cipher-core.js" />
/// <reference path="cryptojs/core.js" />
/// <reference path="cryptojs/enc-base64.js" />
/// <reference path="cryptojs/evpkdf.js" />
/// <reference path="cryptojs/md5.js" />
/// <reference path="cryptojs/sha256.js" />
/// <reference path="highcharts.js" />
/// <reference path="jqgrid/grid.locale-en.js" />
/// <reference path="jqgrid/jquery.contextmenu.js" />
/// <reference path="jqgrid/jquery.jqGrid.js" />
/// <reference path="jqgrid/jquery.tablednd.js" />
/// <reference path="jqgrid/ui.multiselect.js" />
/// <reference path="jquery.layout.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="jquery-1.9.1.js" />
/// <reference path="jquery-2.2.3.js" />
/// <reference path="jquery-input-file-text.js" />
/// <reference path="jquery-ui-1.11.4.js" />
/// <reference path="jquery-ui-i18n.js" />
/// <reference path="jquery-ui-timepicker-addon.js" />
/// <reference path="jstree3/jstree.js" />
/// <reference path="knockout.mapping-latest.js" />
/// <reference path="knockout.validation.js" />
/// <reference path="knockout-3.4.0.js" />
/// <reference path="knockout-date-bindings.js" />
/// <reference path="knockout-jqautocomplete.js" />
/// <reference path="kocustomdatetimepicker.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="moment.js" />
/// <reference path="moment-with-locales.js" />
/// <reference path="numeral.min.js" />
/// <reference path="respond.js" />
/// <reference path="tether/tether.js" />
/// <reference path="tinymce/jquery.tinymce.min.js" />
/// <reference path="tinymce/plugins/advlist/plugin.js" />
/// <reference path="tinymce/plugins/anchor/plugin.js" />
/// <reference path="tinymce/plugins/autolink/plugin.js" />
/// <reference path="tinymce/plugins/autoresize/plugin.js" />
/// <reference path="tinymce/plugins/autosave/plugin.js" />
/// <reference path="tinymce/plugins/bbcode/plugin.js" />
/// <reference path="tinymce/plugins/charmap/plugin.js" />
/// <reference path="tinymce/plugins/code/plugin.js" />
/// <reference path="tinymce/plugins/compat3x/editable_selects.js" />
/// <reference path="tinymce/plugins/compat3x/form_utils.js" />
/// <reference path="tinymce/plugins/compat3x/mctabs.js" />
/// <reference path="tinymce/plugins/compat3x/tiny_mce_popup.js" />
/// <reference path="tinymce/plugins/compat3x/validate.js" />
/// <reference path="tinymce/plugins/contextmenu/plugin.js" />
/// <reference path="tinymce/plugins/directionality/plugin.js" />
/// <reference path="tinymce/plugins/emoticons/plugin.js" />
/// <reference path="tinymce/plugins/example/plugin.js" />
/// <reference path="tinymce/plugins/example_dependency/plugin.js" />
/// <reference path="tinymce/plugins/fullpage/plugin.js" />
/// <reference path="tinymce/plugins/fullscreen/plugin.js" />
/// <reference path="tinymce/plugins/hr/plugin.js" />
/// <reference path="tinymce/plugins/image/plugin.js" />
/// <reference path="tinymce/plugins/insertdatetime/plugin.js" />
/// <reference path="tinymce/plugins/layer/plugin.js" />
/// <reference path="tinymce/plugins/legacyoutput/plugin.js" />
/// <reference path="tinymce/plugins/link/plugin.js" />
/// <reference path="tinymce/plugins/lists/plugin.js" />
/// <reference path="tinymce/plugins/media/plugin.js" />
/// <reference path="tinymce/plugins/nonbreaking/plugin.js" />
/// <reference path="tinymce/plugins/noneditable/plugin.js" />
/// <reference path="tinymce/plugins/pagebreak/plugin.js" />
/// <reference path="tinymce/plugins/paste/classes/Clipboard.js" />
/// <reference path="tinymce/plugins/paste/classes/Plugin.js" />
/// <reference path="tinymce/plugins/paste/classes/Quirks.js" />
/// <reference path="tinymce/plugins/paste/classes/WordFilter.js" />
/// <reference path="tinymce/plugins/paste/plugin.dev.js" />
/// <reference path="tinymce/plugins/paste/plugin.js" />
/// <reference path="tinymce/plugins/preview/plugin.js" />
/// <reference path="tinymce/plugins/print/plugin.js" />
/// <reference path="tinymce/plugins/save/plugin.js" />
/// <reference path="tinymce/plugins/searchreplace/plugin.js" />
/// <reference path="tinymce/plugins/spellchecker/classes/DomTextMatcher.js" />
/// <reference path="tinymce/plugins/spellchecker/classes/Plugin.js" />
/// <reference path="tinymce/plugins/spellchecker/plugin.dev.js" />
/// <reference path="tinymce/plugins/spellchecker/plugin.js" />
/// <reference path="tinymce/plugins/tabfocus/plugin.js" />
/// <reference path="tinymce/plugins/table/classes/CellSelection.js" />
/// <reference path="tinymce/plugins/table/classes/Plugin.js" />
/// <reference path="tinymce/plugins/table/classes/Quirks.js" />
/// <reference path="tinymce/plugins/table/classes/TableGrid.js" />
/// <reference path="tinymce/plugins/table/plugin.dev.js" />
/// <reference path="tinymce/plugins/table/plugin.js" />
/// <reference path="tinymce/plugins/template/plugin.js" />
/// <reference path="tinymce/plugins/textcolor/plugin.js" />
/// <reference path="tinymce/plugins/visualblocks/plugin.js" />
/// <reference path="tinymce/plugins/visualchars/plugin.js" />
/// <reference path="tinymce/plugins/wordcount/plugin.js" />
/// <reference path="tinymce/themes/modern/theme.js" />
/// <reference path="tinymce/tinymce.js" />
/// <reference path="toastr.js" />
/// <reference path="wysiwyg.js" />

