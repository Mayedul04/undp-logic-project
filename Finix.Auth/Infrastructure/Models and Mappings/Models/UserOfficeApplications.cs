﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Finix.Auth.Infrastructure
{
    [Table("UserOfficeApplication")]
    public class UserOfficeApplication : Entity
    {
        public long UserId { get; set; }
        public long OfficeId { get; set; }
        public long ApplicationId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        [ForeignKey("OfficeId")]
        public virtual OfficeProfile CompanyProfile { get; set; }
        [ForeignKey("ApplicationId")]
        public virtual Application Application { get; set; }
    }
}